# Checking out a tag
To checkout a tag, we uses `checkout` with new branch `-b` flag.

```
git checkout tags/v1.2.1 -b rel/v1.2.1
```

> NOTE:
>
> Actual command is:
> ```
> $ git checkout tags/<tag_name> -b <branch_name>
> ```


<br><br><hr>
## References
1. http://stackoverflow.com/questions/791959/download-a-specific-tag-with-git