def fib(max):
    a, b = 0, 1
    while a < max:
        yield a
        a, b = b, a + b

# use generator for lazy loading
for number in fib(1000):
    print number


# call manually
fib_number = fib(1000)
print next(fib_number)
print next(fib_number)
print next(fib_number)
print next(fib_number)
print next(fib_number)
print next(fib_number)

# speed up to the last value
i = 6
while i < 17:
    next(fib_number)
    i += 1

# raise error if generator can no longer generate value
try:
    print next(fib_number)
except Exception, e:
    print "Generator ran out of value. %r" % e
