
array_list = ["string", 1, True, "extra end", "very extra end"]


for item in array_list:
    print "This is the data: %r" % item


i = 0
while i < 6:
    i += 1
    print "the while loop now is: %r" % i


# breaking the loop or skip the current loop
for item in array_list:
    if item == 1:
        continue        # 1 is skipped
    print "This is the data %r" % item
    if item == "extra end":
        break           # loop is broken. Anything else won't be printed