from name_of_the_module import functionA   # import a particular function
import name_of_the_module                  # import all the functions


if __name__ == '__main__':
    print functionA                             # check for function
    print name_of_the_module.functionB          # check for function

    print functionA()                           # execute function
    print name_of_the_module.functionB()        # execute function

# Module setbacks:
# 1) You can import only once (class is object-oriented)