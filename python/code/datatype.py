# boolean
True
False
bool("")                # nothing evaluates as False, regardless of value
bool("anything")        # anything evaluates as True, regardless of values
"This is True" == "This is True" # best practice for comparison


# Number
2342352352332
1 + 1                   # add
1 - 1                   # subtract
4 / 2                   # divide for quotient => 2
4 // 2.222              # divide for floored quotient => 1.0 (actual is 1.8)
4 % 3                   # divide for remainder => 1
2 ** 4                  # power, 2 to the power of 4 => 16
abs(-5)                 # absolute value => 5
int(2342.2342342)       # convert to integer => 2342
float("12312.12312")    # convert to float
bin(222)                # convert to binary
0b0010 | 0b0001         # bit mask using OR => 0b0011
0b0010 ^ 0b0111         # bit mask using EXCLUSIVE OR => 0b0101
0b0010 & 0b0011         # bit mask using AND => 0b0010
0b0010 << 1             # bit shift to left by 1 bit => 0b0100
0b0010 >> 1             # bit shift to right by 1 bit => 0b0001
~0b0010                 # inverted bits => 0b1101


# None
None


# strings
"strings strings strings"
"%d" % 23423    # signed digit decimal
"%i" % 23423    # signed digit decimal
"%o" % 0100     # signed octal value. E.g: oct(64)
"%u" % 23423    # obsolete type. AVOID.
"%x" % 0x2ab2   # hexadecimal in lowercase
"%X" % 0x2AB2   # hexadecimal in uppercase
"%e" % 10e2     # float in exponential format (lowercase)
"%E" % 10E2     # float in exponential format (uppercase)
"%f" % 10.2342  # float in decimal format
"%F" % 10.2342  # float in decimal format
"%g" % 10.22342 # float in auto-exponential/decimal presentation (lowercase)
"%G" % 10.22342 # float in auto-exponential/decimal presentation (uppercase)
"%c" % "a"      # single character
"%r" % "aeg"    # convert anything to string using repr()
"%s" % "sbse"   # convert anything to string usering str()
str(34534543)   # convert anything to string
repr([23,2342]) # convert anything to string


# list
array = ["This", "is", "an", "array"]
array[0] # => "This"
["John Doe", 12123123, True]        # You can mix things up


# tuples (immutable list)
tuples = ("core", "a")


# set (immutable data pack)
sets = {"This", "is", "a", "set"}


# dictionary
dictionary = { "key1" : "value1", "key2": 2, "key3": True}
dictionary["key1"] # => "value1"
