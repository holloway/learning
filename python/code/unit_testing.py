import unittest


# Actual code
class Target(object):
    def __init__(self, core, number=0):
        self.core = core
        self.number = 0

    def add(self, number_a, number_b):
        return number_a + number_b


# Test code
class TargetTest(unittest.TestCase):
    def test_to_add_numbers(self):
        """ add should add 2 numbers """
        target = Target("Voila", 5)
        result = target.add(2, 3)
        self.assertEqual(5, result)

if __name__ == '__main__':
    unittest.main()

