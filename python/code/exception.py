try:
    import whatever
except ImportError:
    print "Something is wrong with import: %s" % ImportError

try:
    raise ValueError("This is not the error")
except ValueError:
    print ValueError

print "Hey, program is still working fine"