# Variable parameters
def variableParameterFunction(*argv):
	arg1, arg2 = argv
	return "arg1=%r, arg2=%r" % (arg1, arg2)
print variableParameterFunction("core", True)


# No parameters
def noParameterFunction():
	return "I don't need parameters"
print noParameterFunction()


# Default Parameters - non-default goes first
def defaultParameters(core, key=0, name="hello"):
	return "key=%d name=%s core=%r" % (key, name, core)
print defaultParameters(True, 5, "Jane")
print defaultParameters(True)