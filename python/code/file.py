
filename = 'samefile.txt'
# Write a File
file = open(filename, 'w')
file.write("Text Line 1 \n")
file.write("Text Line 2 \n")
file.close()

# Append a File
file = open(filename, 'a')
file.write("New Line 1 \n")
file.write("New Line 2 \n")
file.close()

# Read a file
file = open(filename, 'r')
data = file.read()
file.close()
print data

    # preferred way
with open(filename, encoding='utf-8') as file:
    for data in file:
        print data



# Stream a file
a_string = unicode('Paperwhip is a new black.')
import io
a_file = io.StringIO(a_string)
print a_file.read()
print a_file.read() # nothing left to read
print a_file.seek(0)
print a_file.read(5) # stream first 5 characters
print a_file.read(5) # stream next 5 characters
print a_file.tell()  # speak where you are now
print a_file.seek(18) # jump to 19 characters
print a_file.read()