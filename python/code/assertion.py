
# assert True statement
assert 1 + 1 == 2

try:
    assert 1 + 1 == 3
except Exception, e:
    print e

try:
    assert 2 + 2 == 5, "Only for very large values of 2"
except Exception, e:
    print e


# evaluate statement - DON'T USE it on your normal code. You can wipe the entire
# server
print eval(1 + 1 == 2)
print eval(1 + 1 == 3)