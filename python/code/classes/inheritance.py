class Parent(object):
    def __init__(self):
        print "Parent initialized"

    def who_am_i(self):
        print "I'm a parent"


class Child(Parent):
    def __init__(self, name=""):
        super(Child, self).__init__()               # runs parent's method
        print("I'm now initialized as child")


    def who_am_i(self):                             # polymorph parent function
        print "who_am_i function polymoprhed."
        print "I'm a child now"

if __name__ == '__main__':
    dad = Parent()
    son = Child()

    dad.who_am_i()
    son.who_am_i()