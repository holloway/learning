class Football(object):
    def __init__(self, name="", core=""):
        if name != "":
            print "always initialize automatically by %r" % name
        else:
            print "always initialize automatically"

        self.class_variable = "5"                         # instance variables

    def got_kicked(self, force):
        print "football got kicked by %d kN" % force
        print "current score is %r "  % self.class_variable  # applying instance variables


if __name__ == '__main__':
    football = Football("Hoo Rah")
    football.got_kicked(10)
