class Parent(object):
    def __init__(self):
        print "Parent initialized"

    def who_am_i(self):
        print "I'm a parent"


class Child(object):
    def __init__(self, name=""):
        self.parent = Parent()          # create parent as an object instead

    def who_am_i(self):
        print "he is my parent"
        print self.parent.who_am_i()    # use it across each class functions



if __name__ == '__main__':
    dad = Parent()
    son = Child()

    dad.who_am_i()
    son.who_am_i()