
# This is a generator class. Notice that the next is generating the new value.

class Fibo:
    def __init__(self, max):
        self.max = max

    def __iter__(self):
        self.a = 0
        self.b = 1
        return self # must return self to indicate it is an iterator class

    def __next__(self):         # support for Python 3
        fib = self.a
        if fib > self.max:
            raise StopIteration
        self.a, self.b = self.b, self.a + self.b
        return fib # Do not use 'next'. The class itself is a generator already

    def next(self):             # support for Python 2
        return self.__next__()


for number in Fibo(2000):
    print number