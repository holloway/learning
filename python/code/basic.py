# Comments
# This is a comment with a '#' sign


# Number and Maths
print 25 + 30 / 6
print 100 - 25 * 3 / 4
print 3 + 2 + 1 - 5 + 4 % 2 - 1 / 4 + 6
print 3 + 2 < 5 - 7


# Variables
this_is_a_very_long_variable_name = "Yes it is long"


# Arrays
this_is_an_array = ["Bear", 4, "Mammals"]
print "Bear has %i limbs" % this_is_an_array[1] # Accessing item in array
for item in this_is_an_array:                   # looping through array
    print "Array item is: %r" % item


# Access dictionary
this_is_a_dictionary =  { "CA":"San Francisco",
                          "MI":"Detroit",
                          "FL":"Jacksonville"
                        }
print this_is_a_dictionary.items()              # access all dictionary items
print this_is_a_dictionary["CA"]                # access item in dictionary
print this_is_a_dictionary.get("TX", None)      # access item safely
print
for city, key in this_is_a_dictionary.items():  # loop through dictionary
    print "%r has a city called %r" % (key, city)



# String
print 'This is also a string'
print "This is also a string man"

# String Concatenation
my_age = 45
print "This is my age %d" % my_age
print "My Name Is " + "HollowayKeanHo"


# String Placeholder
print "The placeholder for number:%d string:%s boolean:%r" % (5, "cool", True)


# String Escapes
print " \ escape slash. You can escape anything you want"
print " \\ Escape backslash"
print " \" Escape double quote"
print ' \' Escape single quote'
print ' \a ASCII Bell'
print ' a\b ASCII backspace'
print ' \f ASCII formfeed'
print ' \n ASCII newline'
print ' \u2233 Unicode HEX 16-bit'
print ' \Uaf23aabb Unicode HEX 32-bit'


# Ask Input
print "How old are you?"
age = raw_input()
print age

street = raw_input("Which street do you stay in? ")