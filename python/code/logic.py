# logics
# =====
a = 50
b = 30
c = -20
if a < b:
    print "collosal error if a < b is True"
elif a >= b and c < b:
    print "Yes it is true. a > b and c < b"
else:
    print "You better throw away your machine man"

# conditions
# ==========
# '>'   : more than but not equal
# '<'   : less than but not equal
# '>='  : more than and equal
# '<='  : less than and equal
# '=='  : equal
# '!='  : not equal


# loops
# =====
array = ["Alpha", "Bravo", "Charlie"]

# for loop
for item in array:
    print "Hello World %s" % item

# while loop
i = 0
limit = 10
while i <= limit:
    print "Iteration %d" % i
    if i == 5:
        break
    else:
        continue
        print "This will never gets printed"


## break = stop and exit the loop
## continue = skip to next iteration