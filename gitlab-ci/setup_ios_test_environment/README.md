# Setup iOS Environment
This guide is to setup your `.gitlab-ci.yml` file for iOS development. Here,
the guide uses available physical devices instead of using simulators.

<br/><br/>
## Steps
### 1) install and setup gitlab-ci-runner
If you haven't setup a working gitlab-ci-runner, you should read this
[guide](https://gitlab.com/hollowaykeanho/learning/tree/master/gitlab-ci) first.


<br/><br/>
### 2) add `.gitlab-ci.yml` to the project root repository
Gitlab-ci-runner uses a file called ```.gitlab-ci.yml``` to execute the
test-run. Since you're developing on Mac OSX, you should use `shell` mode. The
contents for the file is:

```
stages:
    - build

build_project:
    stage: build
    script:
        - cp ~/code_development/Secrets/Secret_Files.h Secrets.h
        - sh runtest.sh
    tags:
        - ios
        - xcode
```

> NOTE:
>
> The secret file line is optional. I practice safe key code from adding
> sensitive information into git history.


<br/><br/>
### 3) add `runtest.sh` shell script to the root repository
Instead of stuffing all the complicated codes into `.gitlab-ci.yml` file,
we execute `runtest.sh` script instead. This simplifies a lot of execution
guidances. The content for this file is:

```
PROJECT_NAME="Your Project Name"
TEST_PLATFORM="iOS"

for line in $(system_profiler SPUSBDataType | sed -n -e '/iPad/,/Serial/p' -e '/iPhone/,/Serial/p' | grep "Serial Number:" | awk -F ": " '{print $2}'); do
    UDID=${line}
    UDID_ARRAY[i]=${line}
done
DEVICE_COUNT=${#UDID_ARRAY[@]}
if [ "$DEVICE_COUNT" == "0" ]; then
    echo "============================================"
    echo "BUILD FAILED - there is not device attached!"
    echo "============================================"
    exit -255
fi


for ((i=0;i<DEVICE_COUNT;i++)); do
    xcodebuild clean -project $PROJECT_NAME.xcodeproj -scheme $PROJECT_NAME
    if [ "$?" != "0" ]; then
        COUNT=${#RUNNER_RESULT[*]}
        echo "======================================================================================="
        STATEMENT="BUILD FAILED - failed to clean project for: ${UDID_ARRAY[i]}"
        echo "======================================================================================="
        RUNNER_RESULT[$COUNT]=$STATEMENT
        echo $STATEMENT
    fi

    xcodebuild test -workspace $PROJECT_NAME.xcworkspace -scheme $PROJECT_NAME -destination "platform=$TEST_PLATFORM,id=${UDID_ARRAY[i]}"
    if [ "$?" != "0" ]; then
        COUNT=${#RUNNER_RESULT[*]}
        echo "======================================================================================="
        STATEMENT="TEST FAILED - Device: platform=$TEST_PLATFORM,id=${UDID_ARRAY[i]}"
        echo "======================================================================================="
        RUNNER_RESULT[$COUNT]=$STATEMENT
        echo $STATEMENT
    fi
done

COUNT=${#RUNNER_RESULT[*]}
if [ "$COUNT" != "0" ]; then
    exit -55
fi
```

> NOTE:
>
> I'm assuming you're using the same PROJECT_NAME across your project and test
> targets. Otherwise, this script will not work.


<br/><br/>
### 4) Provide executable permissiong to `runtest.sh`
With the script in placed, you now make the script executable by:

```
$ chmod +x runtest.sh
```


<br/><br/>
### 5) Add these test artifacts into your `.gitignore` file
Once you're done, you can add the following artifacts into the `.gitignore`
file.

```
## Your files
Secrets.h
builds
```

> NOTE:
>
> if you don't have your `.gitignore` file, create one.


<br/><br/>
### 6) Make your test schemes available for sharing
Now head to your Xcode menu bar, `Product` > `Scheme` > `Manage Scheme`,
find your `Project Name` and check the `shared` checkbox. `Close` it once it's
done.


<br/><br/>
### 7) Commit and push your code
Once done, commit your code and push it to your server. GitLAB CI Server will
handle the rest.


<br/><br/>
### 3) that's it
Yeap. You can now push it to your branch and have fun auto-testing!


<br/><br/>
## References
1. https://about.gitlab.com/2016/03/10/setting-up-gitlab-ci-for-ios-projects/
2. http://www.tldp.org/LDP/abs/html/exit-status.html
3. http://stackoverflow.com/questions/7363459/how-to-get-the-return-value-of-xcodebuild
4. http://www.tldp.org/LDP/abs/html/exit-status.html
5. https://developer.apple.com/library/content/documentation/DeveloperTools/Conceptual/testing_with_xcode/chapters/08-automation.html
6. http://stackoverflow.com/questions/13086109/bash-scripting-check-if-bash-variable-equals-0
7. https://github.com/fastlane/fastlane/issues/3339
8. https://github.com/bitrise-io/bitrise.io/issues/5
9. https://gitlab.com/ci/lint
10. http://www.mokacoding.com/blog/xcodebuild-destination-options/
11. http://stackoverflow.com/questions/17237354/how-can-i-find-the-device-uuids-of-all-connected-devices-through-a-command-line
12. http://www.stewgleadow.com/blog/2011/11/05/installing-ios-apps-on-the-device-from-the-command-line/
13. http://stackoverflow.com/questions/13086109/bash-scripting-check-if-bash-variable-equals-0