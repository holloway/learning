# Setup GitLAB-CI 
GitLAB-CI provided a very powerful continuous integration (CI) concept for
codes development. It uses a remote runner to run the test, thus, making
the testing seemlessly easy.


<br/><br/>
## Setting Up GitLAB-CI
Here is the steps to setup the GitLAB-CI runner.


1. Go to their [official website]([here](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner#install-gitlab-runner))
and download the the runner based on your operating system:

2.  Provide permission to execute the runner:

     ```
     $ sudo chmod +x /usr/local/bin/gitlab-ci-multi-runner
     ```

3. Setup the runner accordingly.
    1. Run the runner registrations.

        ```
        $ gitlab-ci-multi-runner register
        ```

    2. When prompted for CI integration server, go for the default.

        ```
        Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/ci):
        https://gitlab.com/ci
        ```

    3. Go to the `project website` > `Settings` (Gear sign under your profile picture at top right) > `Runner`. Look for the project token:

        ```
        Use the following registration token during setup: abcdotnxieaharhw
        ```

    4. On the next prompt, key in the token. In this example, it is `abcdotnxieaharhw` obtained in the previous step:

        ```
        Please enter the gitlab-ci token for this runner:
        abcdotnxieaharhw
        ```

    5. On the next prompt, name your machine:

        ```
        Please enter the gitlab-ci description for this runner:
        Holloway-MacBook-Dev
        ```

        > **NOTE**:
        >
        > Name it as if you're going to use across systems. Don't give some funny one-time usable names. This runner is applicable across different projects in the GitLAB. It will appear globally so make sure you do it professionally.

    6. On the next prompt, enter the runner CI tags:

        ```
        Please enter the gitlab-ci tags for this runner (comma separated):
        ios, xcode, osx, rails, python2, python3
        ```

        > **NOTE**:
        >
        > You can insert some other support, like rails, django etc. Remember, it is setting up your laptop as running environment. For this example, the above is essentials.

    7. The runner will register itself successfully after that.

        ```
        Registering runner... succeeded                     runner=s8Bgtktb
        ```

    8. When lastly prompted to run any modes, use `shell` instead of any others:

        ```
        Please enter the executor: virtualbox, ssh, shell, parallels, docker, docker-ssh:
        shell
        ```

        > **NOTE**:
        >
        > This guide is setting up the system as a developer's MacBook. Hence, `shell` is making more sense than any other options. If you want to setup a better version (say a server), you should consult GitLAB-CI team.

    9. When everything is ready, runner should be able to register successfully.

        ```
        Runner registered successfully. Feel free to start it, but if it's running 
        already the config should be automatically reloaded!  
        ```

    10. Perform a clear installation again:

        ```
        $ gitlab-ci-multi-runner stop
        $ gitlab-ci-multi-runner uninstall
        $ gitlab-ci-multi-runner install
        $ gitlab-ci-multi-runner start
        ```

    11. Verify on GitLAB site that it is working.

4. Setup your `.gitlab-ci.yml` according to your project. You can refer to the
guide above to setup your .gitlab-ci.yml accordingly.


<br/><br/>
## References
1. https://about.gitlab.com/2016/03/10/setting-up-gitlab-ci-for-ios-projects/
2. https://danielbitwizardward.gitbooks.io/rails_guide/content/5-1-gitlab_ci_yml.html
3. https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/osx.md
4. https://github.com/gitlabhq/gitlabhq/blob/master/doc/install/installation.md#2-ruby
5. https://about.gitlab.com/2015/06/08/implementing-gitlab-ci-dot-yml/
6. https://gitlab.com/gitlab-org/gitlab-ci-multi-runner#install-gitlab-runner
7. https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/osx.md
