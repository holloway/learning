# Gitlab CI Rails Integration
Gitlab provides a very powerful continuous integration tool for testing project
regressively. Unlike any other products in the internet, the solution they
provided can operate remotely even with your own machine.

Hence, to support rspec-rails Gitlab CI integration, you'll need to do 2 main
steps.


<br><br><hr>
## Steps
### 1) install and setup gitlab-ci-runner
If you haven't setup a working gitlab-ci-runner, you should read this
[guide](https://gitlab.com/hollowaykeanho/learning/tree/master/gitlab-ci).


<br/><br/>
### 2) setup runner to run the project
Gitlab-ci-runner uses a file called ```.gitlab-ci.yml``` to execute the
test-run. After a decent research for Mac, my proposed solution for Mac OSX
runner is as follows:

#### Mac OSX & Ubuntu:
```
before_script:
  - export RAILS_ENV=test

spec:
  script:
    - gem install bundler --no-ri --no-rdoc
    - bundle install --without production
    - bundle exec rake db:setup
    - bundle exec rake db:migrate
    - RAILS_ENV=test bundle exec rspec
    - bundle exec rake db:drop

```

> NOTE:
>
> I'm entirely not sure why the forum made the script quite complicated and
> very specific to Ubuntu Linux. Hence, that said, this README.md will be
> updated another round after gaining some insights from the main team.


<br><br>
### 3) that's it
Yeap. You can now push it to your branch and have fun auto-testing!


<br><br><hr>
## References
1. https://danielbitwizardward.gitbooks.io/rails_guide/content/5-1-gitlab_ci_yml.html
2. https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/osx.md
3. https://github.com/gitlabhq/gitlabhq/blob/master/doc/install/installation.md#2-ruby
4. https://about.gitlab.com/2015/06/08/implementing-gitlab-ci-dot-yml/
5. https://gitlab.com/gitlab-org/gitlab-ci-multi-runner#install-gitlab-runner
6. https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/osx.md