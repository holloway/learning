# Photo Gallery
Want to deploy Photos album? Photos framework is the right place. In this
note, we are going to look into setting up Photos for selection.


<br><br><hr>
## Steps
### 1) include the AVFoundation Framework
We'll need this framework to check and manage user permission for Photos
access.

#### Objective-c
```
#import <Photos/Photos.h>
```


<br><br>
### 2) setup your camera
Now, we'll implement the Photos setup code. 

#### Objective-c
```

@implementation viewController

...

- (IBAction)didSelectAlbum:(id)sender
{
    PHAuthorizationStatus authorizationStatus = [PHPhotoLibrary authorizationStatus];
    if (authorizationStatus == PHAuthorizationStatusDenied) {
        [self albumDenied];
    } else if (authorizationStatus == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                UIImagePickerController *picker = [self selectPhotos];
                [self presentViewController:picker animated:YES completion:nil];
            }
        }];
    } else {
        UIImagePickerController *picker = [self selectPhotos];
        [self presentViewController:picker animated:YES completion:nil];
    }

}


...


- (UIImagePickerController  * _Nonnull )selectPhotos
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = (id) self;
    imagePicker.allowsEditing = NO;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    return imagePicker;
}


- (void)albumDenied
{
    [self featureDenied:NSLocalizedString(@"Photos", @"photo feature label for permission denied alert notice")];
}


- (void)featureDenied:(NSString *)feature
{
    NSString *alertTitle = NSLocalizedString(@"Uh Oh!",
                                             @"feature denied alert title in scan view controller");
    NSString *alertText = [NSString stringWithFormat:NSLocalizedString(@"It looks like your privacy settings are preventing us from accessing your %@. "
                                                                       @"You can fix this by doing the following:\n\n"
                                                                       @"1. Close this app.\n\n"
                                                                       @"2. Open the Settings app.\n\n"
                                                                       @"3. Scroll to the bottom and select this app in the list.\n\n"
                                                                       @"4. Turn the %@ ON for allowing access.\n\n"
                                                                       @"5. Open this app and try again.",
                                                                       @"feature denied alert message in scan view controller"), feature, feature];
    NSString *okActionLabel = NSLocalizedString(@"OK",
                                                @"ok button label for send button in scan view controller");
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertTitle
                                                                   message:alertText
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:okActionLabel
                                                 style:UIAlertActionStyleCancel
                                               handler:nil];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

...

@end
```

> NOTE:
>
> The idea is that before we go for Photos, we first check the permission.
> If the permission is denied, we go forth with the feature alert guide for 
> user.
>
> <br>
> Since the Photos framework handles permission request implicitly, we can go
> straight with presenting the imagePicker to handle the presentation and
> request.
>
> <br>
> Say if user denied the permission during request (not granted), by user
> experience, it's best leave it to do nothing.


<br><br>
### 3) implement delegates
Since we're done with triggering the Photos, it's time to implement the
delegate.

#### Objective-c
```

@implementation viewController

...

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *pickerImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    // process image
    ...
}

...

@end
```

> NOTE:
>
> It's quite straight forward. Basically, all you have to do is implement the
> ```didFinishPickingMediaWithInfo:``` method. Inside, you first extract
> the image via the the key ```UIImagePickerControllerOriginalImage```.
>
> <br>
> Then, you can perform your image processing.


<br><br><hr>
## That's it
That's all about Photos initialization. Have fun with image processing.


<br><br><hr>
## References
1. http://stackoverflow.com/questions/15004114/how-may-i-check-if-my-app-has-access-to-phone-gallery
2. https://developer.apple.com/library/ios/documentation/Photos/Reference/PHPhotoLibrary_Class/#//apple_ref/occ/clm/PHPhotoLibrary/authorizationStatus
3. http://stackoverflow.com/questions/26595343/determine-if-the-access-to-photo-library-is-set-or-not-ios-8