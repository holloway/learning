# Camera (Photos)
Want to deploy camera? AVFoundation framework is the right place. In this note,
we are going to look into setting up native camera for photo captures.


<br><br><hr>
## Steps
### 1) include the AVFoundation Framework
We'll need this framework to check and manage user permission for camera
access.

#### Objective-c
```
#import <AVFoundation/AVFoundation.h>
```


<br><br>
### 2) setup your camera
Now, we'll implement the camera setup code. 

#### Objective-c
```

@implementation viewController

...

- (IBAction)didSelectCamera:(id)sender
{
  // read current camera access permission status
  AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];

  if(authStatus == AVAuthorizationStatusDenied) {
      [self cameraDenied];
  } else if(authStatus == AVAuthorizationStatusNotDetermined) {
      [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
          if(granted) {
              UIImagePickerController *picker = [self takePhoto];
              [self presentViewController:picker animated:YES completion:nil];
          }
      }];
  } else {
      UIImagePickerController *picker = [self takePhoto];
      [self presentViewController:picker animated:YES completion:nil];
  }
}


...


- (UIImagePickerController  * _Nonnull )takePhoto
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = (id) self;
    imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
    imagePicker.allowsEditing = NO;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    return imagePicker;
}


- (void)cameraDenied
{
    [self featureDenied:NSLocalizedString(@"Camera", @"camera feature label for permission denied alert notice")];
}


- (void)featureDenied:(NSString *)feature
{
    NSString *alertTitle = NSLocalizedString(@"Uh Oh!",
                                             @"feature denied alert title in scan view controller");
    NSString *alertText = [NSString stringWithFormat:NSLocalizedString(@"It looks like your privacy settings are preventing us from accessing your %@. "
                                                                       @"You can fix this by doing the following:\n\n"
                                                                       @"1. Close this app.\n\n"
                                                                       @"2. Open the Settings app.\n\n"
                                                                       @"3. Scroll to the bottom and select this app in the list.\n\n"
                                                                       @"4. Turn the %@ ON for allowing access.\n\n"
                                                                       @"5. Open this app and try again.",
                                                                       @"feature denied alert message in scan view controller"), feature, feature];
    NSString *okActionLabel = NSLocalizedString(@"OK",
                                                @"ok button label for send button in scan view controller");
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertTitle
                                                                   message:alertText
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:okActionLabel
                                                 style:UIAlertActionStyleCancel
                                               handler:nil];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

...

@end
```

> NOTE:
>
> The idea is that before we go for camera, we first check the permission.
> If the permission is denied, we go forth with the feature alert guide for 
> user.
>
> <br>
> Since the AVFoundation handles permission request implicitly, we can go
> straight with presenting the imagePicker to handle the presentation and
> request.
>
> <br>
> Say if user denied the permission during request (not granted), by user
> experience, it's best leave it to do nothing.


<br><br>
### 3) implement delegates
Since we're done with triggering the camera, it's time to implement the
delegate.

#### Objective-c
```

@implementation viewController

...

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *pickerImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    // process image
    ...
}

...

@end
```

> NOTE:
>
> It's quite straight forward. Basically, all you have to do is implement the
> ```didFinishPickingMediaWithInfo:``` method. Inside, you first extract
> the image via the the key ```UIImagePickerControllerOriginalImage```.
>
> <br>
> Then, you can perform your image processing.


<br><br><hr>
## That's it
That's all about camera initialization. Have fun with image capture.


<br><br><hr>
## References
1. https://developer.apple.com/library/ios/documentation/AVFoundation/Reference/AVFoundation_Constants/index.html#//apple_ref/doc/constant_group/Media_Types
2. http://stackoverflow.com/questions/32311887/ios-request-access-to-camera
3. http://stackoverflow.com/questions/20464631/detect-permission-of-camera-in-ios
4. http://stackoverflow.com/questions/26070393/is-there-a-way-to-ask-user-for-camera-access-after-they-have-already-denied-it-o