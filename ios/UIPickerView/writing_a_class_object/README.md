# UIPickerView - Writing a class object
So, it's easy to use UIPickerView but how do we write a class for it? In this
note, we're going to go through how to create a UIPickerView class object for
customization.


<br><br><hr>
## Steps
### 1) create a new class files
It's pretty straight forward, just go ahead and create a new class file. Please
ensure you're sub-classing UIPickerView. Also, make sure that you're importing
UIKit Framework.

#### objective-c
**myCustomPickerView.h**:
```
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface myCustomPickerView : UIPickerView

@end
```

**myCustomPickerView.m**:
```
#import <UIKit/UIKit.h>

@implementation myCustomPickerView

@end
```


<br><br>
### 2) add in delegate protocol to ease user delegate handling
Just like any other classes, it's a good thing to include delegate protocol
for user to handles delagate easily when using your class.

#### objective-c
**myCustomPickerView.h**:
```
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@class myCustomPickerView;
@protocol myCustomPickerViewDelegate <UIPickerViewDelegate>

@optional
- (void)someDelegateMethod;

@required

@end


@interface myCustomPickerView : UIPickerView

@property (weak, nonatomic) id <myCustomPickerViewDelegate> delegate;

@end
```

**myCustomPickerView.m**:
```
#import <UIKit/UIKit.h>

@implementation myCustomPickerView

@synthesize delegate;

@end
```


<br><br>
### 4) add in initialization
There are 3 types of initializations, ```init```, ```initWithFrame:``` and
```initWithCoder:```. Normally direct storyboard UIPickerView class assignment
calls ```initWithCoder:``` while the other 2 are often programmatically. We
can use a custom ```setup``` function to unify our UIPickerView setup.

<br>
Meantime, we'll assign the interface ```UIPickerViewDelegate:``` and 
```UIPickerViewDataSource``` for internal class delegate handling.

#### Objective-c
**myCustomPickerView.m**:
```
#import <UIKit/UIKit.h>
#import "Data.h" // dummy data for demo

@interface myCustomPickerView() <UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) Data *data;
@end


@implementation myCustomPickerView

@synthesize delegate;

- (void)setup
{
    //setup your data here
    self.data = [[Data alloc] init];

    super.delegate = (id)self;
    super.dataSource = (id)self;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

@end
```

> NOTE:
>
> Take **extra note** on the ```setup``` method. It is using **super**, not 
> **self** while assigning its own delegate and dataSource.


<br><br>
### 5) structure your item row
With the initialization done, we can now structure the row presentation.
```numberOfComponentsInPickerView:``` indicate the column structure of the row
while ```numberOfRowsInComponent``` structures the total row in a column.

<br>
A simple example shows as follows:

#### Objective
**myCustomPickerView.m**:
```
#import <UIKit/UIKit.h>
#import "Data.h" // dummy data for demo

@interface myCustomPickerView() <UIPickerViewDelegate, UIPickerViewDataSource>

...

@end


@implementation myCustomPickerView

...

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.data.list.count;
}

@end
```

> NOTE:
>
> This is a simple 1 component multiple values structure. For multiple columns,
> you should always check the component number to identify the data type before
> returning the data quantity.


<br><br>
### 6) setup row presentation
There are 2 ways to do it: ```titleForRow:``` or ```viewForRow:```. In this
example, we're using ```viewForRow:``` since it's much complicated and require
more guidance.

### Objective-c
**myCustomPickerView.m**:
```
#import <UIKit/UIKit.h>
#import "Data.h" // dummy data for demo

@interface myCustomPickerView() <UIPickerViewDelegate, UIPickerViewDataSource>

...

@end


@implementation myCustomPickerView

...

- (UIView *)pickerView:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row
          forComponent:(NSInteger)component
           reusingView:(UIView *)view
{
    if (view == nil) {

        // no view is reusable so we'll need to setup one
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 30)];

        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(35, 3, 245, 24)];
        label.textAlignment = NSTextAlignmentLeft;
        label.backgroundColor = [UIColor clearColor];
        label.tag = 1001;
        [view addSubview:label];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, 24, 24)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.tag = 1002;
        [view addSubview:imageView];
    }

    // process your data using row inside your datalist
    NSString *data = self.data.list[row];
    
    // process the row view
    UILabel *label = [view viewWithTag:1001];
    label.text = data;

    // find flag
    UIImageView *flagView = [view viewWithTag:1002];
    flagView.image = [UIImage imageNamed:self.data.imagelist[row]];
    
    // return the row view
    return view;
}

@end

```

> NOTE:
>
> The very beginning we should check the reusable view is availble or otherwise,
> create one (this is for consistency across all the rows). Notice that each
> elements are being tagged with a number for future setup.
>
> <br>
> Then, we take the **row** and **component** to get our data for display.
> Once you got it, fill them into the correct elements.
>
> <br>
> Once you're done, return view for the row.


<br><br>
### 7) setup the delegates when user is selecting the data
There are various approach when it comes to handling selection. Since we're
designing a class, we'll handle both internal and external handling.

#### Objective-c
**myCustomPickerView.m**:
```
#import <UIKit/UIKit.h>
#import "Data.h" // dummy data for demo

@interface myCustomPickerView() <UIPickerViewDelegate, UIPickerViewDataSource>

...

@end


@implementation myCustomPickerView

...

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component
{
    // handles data selection internally
    self.selectedData = self.data.list[row];

    // handles data via external delegates
    if ([delegate conformsToProtocol:@protocol(myCustomPickerViewDelegate)] &&
        [delegate respondsToSelector:@selector(someDelegateMethod)]) {
        [self.delegate someDelegateMethod];
    }
}

@end

```

> TIPS:
> 
> Be creative with delegate methods for external handling.


<br><br><hr>
## That's it
There you go. You've just completed a simple UIPickerView class creation.
Have fun designing one.


<br><br><hr>
## References
1. http://stackoverflow.com/questions/24708428/uipickerview-not-calling-titleforrow
2. http://stackoverflow.com/questions/25537635/uipickerview-does-not-display-data-in-ios-7
3. http://stackoverflow.com/questions/27353514/uipickerview-not-displaying-data-or-calling-viewforrow
4. http://stackoverflow.com/questions/27353514/uipickerview-not-displaying-data-or-calling-viewforrow
5. https://github.com/nicklockwood/CountryPicker/blob/master/CountryPicker/CountryPicker.h
6. https://github.com/nicklockwood/CountryPicker/blob/master/CountryPicker/CountryPicker.m
7. https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIPickerView_Class/index.html
8. http://stackoverflow.com/questions/12251315/how-insert-an-images-and-text-in-specific-rows-of-an-uipickerview/12251834#12251834
9. http://stackoverflow.com/questions/15727332/add-image-icon-to-a-uipickerview-row
10. http://stackoverflow.com/questions/13756591/how-would-i-set-up-a-uipickerview