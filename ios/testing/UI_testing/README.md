# iOS UI Testing
iOS testing framework is a fairly new framework, launch in 2015. There are
some glitches but it's definately worth the efforts. Why? It runs the same
test across multiple devices! That saves a lot of time!


<br><br><hr>
## Steps to setup the testing framework in the profile
This section covers how to setup the testing framework for a project.


<br><br>
### 1) Prepare the UI testing bundle
If you're creating the project at start, make sure you include the UI testing
bundle option before creation.

Otherwise, you can head to
***project > targets > "add (+ sign)" > iOS UITesting > fill in the details***.
Basically, it's adding a new target called UITesting.

<div align="center">
  <img src="https://lh3.googleusercontent.com/6bL3xpTY5zOg-w1snlNXRXSVDzdvP2ZN6r3e0lL0lMxwa0ihaqBgsdMdwREbcSitIoZ29KWAZbfSgYULM8TphN56VVkD_alKhucYi1RnTkbmIO_kPJraYupfwIbMmOQWZOM9bGglrU8SPrr6kmLIqDOn96HSpbA8tEzQdPHbQq6aDV5k--hBB8jd1B7rQpCBecghTXZ-e0h4I5YGNeZ4S6SY0e19NyhUVd31S6V8EZduqG6Z89O3RCVb-kDZk_GPQOMyAtL5GyrGMe-F5LoZcfC8uKfiexXVnq0kfqMmKnLRv3POCrnCHbYKaWU65ilGQA8kxNz_X9MMXLpUcWi1T3xng530PaWehzVPyafr_DpG0bcyHRl1MHWuJdUEINGokqYDA18sCQNkBR6AiJUGZsHddF6mdN5KkfmgkhHR4-0ubpmBKlRV33rOwk43uow_TL5lniRNDJZ2Vbpo3NhUW58uRUF0V5KFoMd8WwIwuqK7jaQ8_226J4IlGA8m0I9gYB8XFuSRYCy_QFgG9_ZVJI_AEjeoYRpRA3DSy3ip9KTA6WrlZ4lZ1kbIfyx-rqkYsIGcWsB0K-YKX48HQFxHCjD7PPLamjoA=w2594-h1280-no" />
</div>

Once you're done, we'll proceed with podfile preparation (if you're using it).


<br><br>
### 2) Update your podfile to facilitate 3rd party framework to test framework
The point here is that the test framework is different from default framework
(2 different targets). Hence, to allow your test framework works, you'll need
to adjust your Podfile to have something as below:

```
# Uncomment this line to define a global platform for your project
# platform :ios, '9.0'
def shared_pods
    pod 'AFNetworking', '~> 3.0'
    pod 'SDWebImage'
    ...
end

target 'Rheinfabrik' do
  # Uncomment this line if you're using Swift or would like to use dynamic frameworks
  # use_frameworks!

  # Pods for Rheinfabrik
  shared_pods

  target 'RheinfabrikTests' do
    inherit! :search_paths
    # Pods for testing
    shared_pods
  end

  target 'RheinfabrikUITests' do
    inherit! :search_paths
    # Pods for testing
    shared_pods
  end

end

```

> NOTE:
>
> Instead of duplicating all the pods to individual target, we uses a
> subroutine (called 'shared_pods') and group them. So next time, we just
> need to call shared_pods in any new target.

Once you're done, we're ready to proceed to create test cases.


<br><br><hr>
## Steps to create a new UI test case

### 1) create a new test case file
To create test case, head to the test section on the file column.
It looks as below:

<div align="center">
  <img src="https://lh3.googleusercontent.com/ktfEyh1rwwgjXHwEj6_hBsiiy4hG3G7i9yoiNhx1UMYuVO-gPvP642Z_d22zdcDACnelKFHLiiVD2qwhaw6e0PRecTwzfiom2fqYqozHjWDjWGgrUS-SIzwj0kM8fAGmUrcH8WAb88ZncdTYVL9HOlTdKZKj-99tr9Fby-VS4vrK5EisbTFjzairIIqBkZGStFlh65bs5b11sDBsXPnjy3IoBzAX1SYyC66quxKyB8IhhBzzLUiOquEY-kHIFbJOg7uq0KV_aYBeDWJ0GRcezNCud50kSuHnnbzM32TkZGW77Kdyc067xf-Htuw7TQvwiNdyH-q9QB536pzhx8L4s69vRk7ykcIPTWrpkHeNZucHgWEzADiEtWPOSgwT29ps1wY_tfhUrHjnF6W1yX4dFAShrmTZ3c62GPW3az96Bi_LnJGtCXCvtBRxQQoHtVxqhi3H87zHlp2KkK0Zcv1jO7K5qNxyXYevqDeisAjBVRty4SqZUQxLm1XV_Du9SkuMmjaMZk01b36xQJDtPyihx8vwFRGi_LFCcZGRR8FVaH41m9SW9DcsmZtcl0may8zi6cXtfSZsvUzLnAcBNkO3DcBej8Nz3A6R=w692-h862-no" />
</div>

### 2) name your test case as the filename
Now name the test case as filename.

<div align="center">
  <img src="https://lh3.googleusercontent.com/060h0a-fJAOPT3njKiJcPrxDN9xVxk1ODfo4iBj8HfR2wrzQTf3nPLt5yNt0Gv2bef004YeuWi5NqA6UUHZC3_McpWKEyKL7yGnZ1EyfU13b9Ht51ml9yMHb4exbNFGuGaNqUjsZmD-1dvdw16vY0NcHYhZfo4J9nPdJr0EMekJnGByPzFMm-Sh_i-EyM7PbKvLCJgEAFLp2p5WutZUhOJ7M2wsCqG46T5yrAcWgLSKnoeW70ClAOjsXp51cKmeziS9Uv8SPjR2C26Q-YSk4ya_JDms7PnW4z8JDktSUEYApUn5U-AkWISwJ3Zc0b-KlinED4ImQQo820gfsTC8aSRE_PszcsURZ4UgHmiN8zL7kB-DKWcfcKKE_tZo0R6A5fhAzkCIDXcTQ7tB5Epqp4x9JfgI7XtupuhVzElcP-EOZHPKOkEerhr73GuKZEpebeTymJ_JCJDsQwvMyr4sDlpD3C8DvtXESnzbdjd6Qee71IHr9IM9JppaOdzNAXd2p1cxpsuiyX_pGKMeeqWKfQ0tDHt8y72BmqToaT4YAGND8uNf3J2vKkUsy9kWr4djrhLo8NTrLL4BGKuppUr9ZfB27nf1Jgm7C=w1462-h1040-no" />
</div>

### 3) save the test case into target file
Save the target file into UITest bundle. **DO NOT save into your main project**.
Otherwise you might have a hard time figuring out why your main code fails.

It should looks something like this:

<div align="center">
  <img src="https://lh3.googleusercontent.com/Hnrhmz_qGQAiL65xfBz_LNVpMvFZhUj8koqhhXgttivKp-OWxXszxXqJvA0tqIL1hQs_ozVVpli7UFDY9nQBNTyzqkUlNWo_JTby0Ex8QHS9PZB3xkIOjQCYDNnhQsN69e4hUVkt9LCAm6vwhmsHcWdHXIJKmJk8xf1NM6RYVYi8zjfOqNBlNxGRLuWMZf6zpZ0Sjll9U2xBFzCmVGhzhajumdcAX4Jr-z0QCPElk-uIBOSnirbNAHRArQLeuSNjeAfZuEa43wkj53d5NXyfYRUSY4CfR4-sLbnAI4F9mHFzdrpJ4HQFUbblixIgjosCEK0HQJSm-YfmfK1hOCnwyMavXRCuI6kOU2Njam6nBgvsLBkxN4InZwrkDxNSH5IKyjqVHJQQ_afCU3jDAvz7dF1xL-9AQIVEF0tFFUxig4ON9GYebFQFepH9ZHc9mkf2T2E8MBmZDTVwu2ajAS1qd18vFHkiBiJtpAIJp6TByUEIT77cJ0mHfIdPBUcf2SqBdSztWNdcEFlYWs4NbUDHfcA-wP7PqsbiRbymV168rdRtPm44QLmVtf_F8ID1Uw4-WEq6mK4JZbkoKIhLO79sJaG7pfiPeBpP=w1728-h604-no" />
</div>

Once you're done, you should see a UI test class inside the UITest bundle.


<br><br><hr>
## Steps to record the test step
### 1) change the default structure if needed
The original file is quite straight forward: you have a ```setUp```,
```tearDown``` and ```testExample```. Basically, ```setUp``` is the method for
Xcode to setup the test environment while ```tearDown``` is the opposite.
```testExample``` is the main execution for a particular test cases.
```
#import <XCTest/XCTest.h>

@interface mostPopularListLoadByDefault : XCTestCase
@end

@implementation mostPopularListLoadByDefault
{
    XCUIApplication *app;
}

- (void)setUp
{
    [super setUp];

    self.continueAfterFailure = NO;
    app = [[XCUIApplication alloc] init];
    [app launch];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testExample
{
    XCUIElement *navigationBars = app.navigationBars[@"Most Popular"];
    XCTAssert(navigationBars.exists);
}


@end

```


<br>
Personally, since this is a UI test, I'll normally want to keep a single test
case for both devices. So, I'll rewrite the code structure as follows:
```
{
    XCUIApplication *app;
}

- (void)setUp
{
    [super setUp];

    self.continueAfterFailure = NO;
    app = [[XCUIApplication alloc] init];
    [app launch];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testExample
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        [self iPad];
        return;
    }
    [self iPhone];
}


- (void)iPhone
{
    // iphone test case 
}


- (void)iPad
{
    // ipad test case
}


- (void)commonSubroutineBothIPhoneAndIPadAreUsing
{
  // ...
}
```


<br><br>
### 2) Once done, you can use the Xcode recorder to record a test case
Personally it's not as good but it helps by coding all the necessary code
skeleton up fast compared to traditional figure-and-code. So let's tap on
it and use it to record the code skeleton.

It's actually the small recorder button in your debugger bar. Press again to
stop recoding.

<div align="center">
  <img src="https://lh3.googleusercontent.com/UvYlBv4YlSQbTHN9Iw5-geqaOvUh4XCdJwvobnQ20nsBLF5nz4lMlVtM-_3U2VV4nDjhzmZZLaWHhCFbAitnwlIYRi-JHtd-_V2dvCnLzg1XReq94Kj1W1-46o41TSSYfRLwerl-GLJFGMydYQWWYAmMvNy0sPcvMexicipdjD_GcBVLk-hDRRW8T_umHJGJiikuN1-OkA0E9cLhngnnEAOfk4fMjZITHnv3rAuTKkgMIEtzPUeb8iuzgmt60rXlQiUgjgx_MSzuHGLDaBcvONYAetmEdG8-thjiOiDe6s2e25Ams9qt3BXKVJcahYtfluxubvj0ZkDZ78YUqnqzcTKMw9rzzQ8wQLSPfbV5DOS4FDUOWMw5duUbZvFU5HqLnw5a2ixsykJszRahxvwqkWdclHFuC8muCREkjK-Wx1HLm09Of8o6GvIJf9siyHDhI4h6qrAIBX36fitPno0jBuvv7-XfgQs37GQR985RxhjNeHPHiqmyLnV2q127lIvojrFNUAvBeGi58vuQPU2cSi2u_VvWpdq6HahztihUoHrZoDvcWBfl2Q-R873iSYL0efzs6wAUTfrSuNMQ75Lq_ZIhNyQn_qv6=w732-h132-no" />
</div>


<br><br><hr>
## Steps to set assertion in test case
Once you're done, you'll realize Xcode is actually coding the test code for
you. All right, truth be told, unless you're planning to do hit-and-run work,
you won't like the code. Around this moment you probably want refactor the
code on top of it.

There are few ways to come arounds it:
1. study the code and modify accordingly
2. use simple assertion.

The easiest assertion is using ```XCTAssert```.
```
XCTAssert(closeButton.exists);
```

Most of the object-oriented coding remains the same. If you want to learn more,
you can view other guide within this folder, like, for example, how to get
the first row from a tableview without hardcoding to label.


<br><br><hr>
## Steps to run the test case 
Once you're happy, let's test it!

<br><br>
### 1) to run the test
run it on your menu bar: ```Product > Test ```.


<br><br>
### 2) to view the test result
See the list in the test inspector. You should see a green tick or red cross
indicating the test case has passed or failed.


<br><br><hr>
## That's it
That's all for UI testing overview!


<br><br><hr>
## References
1. https://github.com/fastlane/fastlane/issues/1686
2. https://forums.developer.apple.com/thread/6503
3. http://stackoverflow.com/questions/31182637/delay-wait-in-a-test-case-of-xcode-ui-testing
4. http://savvyapps.com/blog/ios-expectations-test-async-functions-without-callback-method
5. https://about.gitlab.com/2016/03/10/setting-up-gitlab-ci-for-ios-projects/
6. https://www.raywenderlich.com/61419/ios-ui-testing-with-kif
7. https://www.bignerdranch.com/blog/ui-testing-in-xcode-7-part-1-ui-testing-gotchas/