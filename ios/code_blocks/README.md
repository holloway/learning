# Code Blocks
Miss the ability to transfer blocks of code like procs or lambda in Ruby?
Fear not. Objective-c and swift has it. Code Blocks are a set of codes
stored like a parameters for later execution / repetitive execution. 

<br>
Just like any other variable types, the only difference between code block
and conventional variables is it stores set of execution, instead of data.
Also, that means code blocks are **objects** in nature.

<br>
Using code blocks alongside with delegates is quite a powerful technique,
producing a much cleaner and a very flexible code.


<br><br><hr>
## When to use it
Code blocks are extremely useful when you're performing multi-threading where
you can't handle each of the processes in a synchonous manner. Hence, you use
code blocks to delegate the execution from inputs all the way to output,
completely.


<br><br><hr>
## Declaration
### Basic
Unlike any other declarations, code blocks is the weirdest, following a
customized C function declarations. Code blocks format follows:

<br>
#### Objective-C
```
return_type(^nameOfTheBlock)(NString params1, int params2, ...)
```

> NOTE:
>
> The caret (^) indicates that it is a code block.

<br>
Some examples:
```
NSString *(^ProcessURL)(NSURL url);
int (^ProcessHTTPResponseCode)(NSString *responseCode, NSString *message);
void (^startEngine)(void);
```


<br><br>
### Declare it as a class property
In the case if you need to save it inside your class property, you can use it
similar to the conventional code block manner as above.

<br>
#### Objective-C
```
@interface ViewController ()

@property (nonatomic, strong) int (^ProcessHTTPResponseCode)(NSString *responseCode, NSString *message);
...

@end

```


<br><br>
### Declare it as a parameter for a function
In the case if you need to declare it as a parameter, such as completion
handler, that has a special trick. Instead of the conventional way, you use
this format:
```
nameOfTheInput:(returnType(^)(NString params1, int params2, ...))nameOfTheBlock;
```

> NOTE:
>
> Notice the name of the block is shifted towards the back, instead of new
> declaration.

Below are some examples.

<br>
#### Objective-C
```
+ (void)executeCSRFRequestWithCompletionHandler:(void (^)(NSURLSessionTask *task, NSDictionary *responseObject))successHandler
                               WithErrorHandler:(void (^)(NSURLSessionTask *operation, NSError *error))errorHandler;

```


<br><br><hr>
## Assigning codes into the code block
### for Basic and Class Properties
This is how you assign the code blocks into the variable for basic and class
property types.

<br>
#### Objective-C
```
int (^ProcessHTTPResponseCode)(NSString *responseCode, NSString *message);

ProcessHTTPResponseCode = ^(NSString *responseCode, NSString *message) {
  int httpResponseCode = 0;

  NSLog(@"%@", responseCode);
  NSLog(@"%@", message);
  ...

  return httpResponseCode;
} 

```


<br><br>
### for parameters code blocks
This is how you assign the code blocks into parameter type code blocks.

<br>
#### Objective-C
```
 [self executeCSRFRequestWithCompletionHandler:^(NSURLSessionTask *task, NSDictionary *responseObject) {
                                                NSLog(@"success!!!");
                                                ...
                               }
                               WithErrorHandler:^(NSURLSessionTask *operation, NSError *error) {
                                                NSLog(@"failed!!!");
                                                ...
                               };

```


<br><br><hr>
## References
1. http://www.appcoda.com/objective-c-blocks-tutorial/


