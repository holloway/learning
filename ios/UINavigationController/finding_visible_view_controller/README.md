# To view visibleViewController
Sometimes, especially inside ```AppDelegate.m``` file, you want to look for
a current visible view controller. To do that, we uses:
```navigationViewController.visibleViewController``` to do the job.

<br>
Example:
## Objective-c:
```
  if ([rootViewController.frontViewController isKindOfClass:[UINavigationController class]])
      frontNavigationViewController = (UINavigationController *)rootViewController.frontViewController;
  
  if ([frontNavigationViewController.visibleViewController isKindOfClass:[ScanViewController class]]) {
      ScanViewController *viewController = (ScanViewController *)frontNavigationViewController.visibleViewController;
      
      # ... do something 
  }
```

> NOTE:
>
> I first check for the navigation controller and call it as
```frontNavigationViewController``` once identified.
>
> Next, I check the if the current visible view controller is my custom
> ```ScanViewController```, execute some of my internal operations.
>
> Of course, if identified, I re-cast the current visible view controller
> into ```viewController``` for easier operations.


<br><br><hr>
## References
1. http://stackoverflow.com/questions/9009646/current-view-controller-from-appdelegate
2. http://stackoverflow.com/questions/4067692/getting-the-top-most-uiviewcontroller