# Animate View Layer Borders
Sometimes you may want to animate the apperance for the ```UIView``` borders
or ```UIButton```. To do that, we will be using ```CABasicAnimation```.

<br><br><hr>
## Changing border color
This section is to animate color changes alone. All you have to do is to set
the current color (fromValue) to the desired color.
Also, **do ensure that you set the final color on the object itself too**.


### Objective-c
```
CABasicAnimation *color = [CABasicAnimation animationWithKeyPath:@"borderColor"];

// set the current color
color.fromValue = (id)[UIColor redColor].CGColor;

// set the ending color
color.toValue = (id)[UIColor blueColor].CGColor;
view.layer.borderColor = [[UIColor blueColor] CGColor];

// set the duration and timing
color.duration = [CATransaction animationDuration];
color.timingFunction = [CATransaction animationTimingFunction];

// add it to your targeted view object
[view.layer addAnimation:color forKey:@"myAnimation"];
```


<br><br><hr>
## Changing border width
Similar to border color, only this round, we change set the final width.


### Objective-c
```
CABasicAnimation *width = [CABasicAnimation animationWithKeyPath:@"borderWidth"];

// set the current width
width.fromValue = @2;

// set the ending width
width.toValue = @4;
view.layer.borderWidth = 4;

// set the duration and timing
width.duration = [CATransaction animationDuration];
width.timingFunction = [CATransaction animationTimingFunction];

// add it to your targeted view object
[view.layer addAnimation:width forKey:@"myAnimation"];
```


<br><br><hr>
## Combining both border and width
Now we know how to change border and width, it's time to do combination. To do
so, we use the CAAnimationGroup to group both of them. Instead of adding each
animations individually to layer, you only add the grouped animation once.


### Objective-c
```
/*
 * Create color change animation object
 */
CABasicAnimation *color = [CABasicAnimation animationWithKeyPath:@"borderColor"];

// set the current color
color.fromValue = (id)[UIColor redColor].CGColor;

// set the ending color
color.toValue = (id)[UIColor blueColor].CGColor;
view.layer.borderColor = [[UIColor blueColor] CGColor];


/*
 * Create border change animation object
 */
CABasicAnimation *width = [CABasicAnimation animationWithKeyPath:@"borderWidth"];

// set the current width
width.fromValue = @2;

// set the ending width
width.toValue = @4;
view.layer.borderWidth = 4;


/*
 * Create group animation object
 */
// animate both as 1 group
CAAnimationGroup *both = [CAAnimationGroup animation]; 

// set total timing
both.duration   = 0.5;

// combine the animations
both.animations = @[color, width];

// optionally add other configuration (that applies to both animations)
both.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];

[self.layer addAnimation:both forKey:@"color and width"];
```

## That's it
Give it a try!

<br><br><hr>
## References
1. http://stackoverflow.com/questions/21927994/animate-calayer-border-change