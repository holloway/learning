# Converting PDF to images
The idea of handling PDF is similar to handling any other image format.
However, this is a multi-page and requires redraw version. Apple prepares an
in-house framework just to do this work.


<br><br><hr>
## Converting steps for single target pages
This function is to convert a single PDF page into UIImage. You'll need to set
page the page number into the function.

### Objecitive-c
```
#define PDF_DEFAULT_RESOLUTION      72.0

- (NSMutableArray *)convertPDFPageToUIImage:(NSString *)filePath    page:(NSInteger)pageNumber
    // Declare all the resources and lazy-loading them later
    CFStringRef path;
    CGPDFPageRef page;
    CGContextRef context;
    CFURLRef url;
    CGRect cropBox;
    CGSize pageVisibleSize;
    UIImage *resultingImage = nil;
    CGPDFDocumentRef document;
    size_t totalPages, pageWidth, pageHeight;
    int pageRotation;
    float dpi;


    // process the file path and get the document
    path = CFStringCreateWithCString(NULL,
                                    [filePath cStringUsingEncoding:NSUTF8StringEncoding],
                                    kCFStringEncodingUTF8);
    url = CFURLCreateWithFileSystemPath(NULL, path, kCFURLPOSIXPathStyle, 0);
    document = CGPDFDocumentCreateWithURL(url);
    CFRelease(path);
    CFRelease(url);
    if (document == NULL)
        goto document_release_path;


    // get the total pages
    totalPages = CGPDFDocumentGetNumberOfPages(document);
    if (totalPages == 0)
        goto document_release_path;

    // convert your desired DPI output
    dpi = 300.0 / PDF_DEFAULT_RESOLUTION;

    // convert the page
    resultingImage = nil;
    if (pageNumber < 1)
        pageNumber = 1;
    page = CGPDFDocumentGetPage(document, pageNumber);

    // get page size
    cropBox = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
    pageWidth = cropBox.size.width;
    pageHeight = cropBox.size.height;
    pageRotation = CGPDFPageGetRotationAngle(page);
    pageVisibleSize = CGSizeMake(cropBox.size.width, cropBox.size.height);
    if (pageRotation == 90 ||  (pageRotation == 270) || (pageRotation == -90))
        pageVisibleSize =  CGSizeMake(cropBox.size.height, cropBox.size.width);

    // set dpi settings
    pageWidth = pageWidth * dpi;
    pageHeight = pageHeight * dpi;
        
    // get current image context
    context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGContextTranslateCTM(context, 0.0, pageVisibleSize.height);
    CGContextScaleCTM(context, dpi, -dpi);
    CGContextSaveGState(context);
    CGContextDrawPDFPage(context, page);
    CGContextRestoreGState(context);
        
    // extract image
    resultingImage = UIGraphicsGetImageFromCurrentImageContext();
        
    UIGraphicsEndImageContext();

    
document_release_path:
    CGPDFDocumentRelease(document);
    return resultingImage;
}
```


<br><br><hr>
## Converting steps for full document
This guide currently written into a single function extraction. This function
extracts all the pages instead of one. 


Word of caution:
**avoid import bulkly pdf or you'll face crashing due to memory starvation** 

### Objecitive-c
```
#define PDF_DEFAULT_RESOLUTION      72.0

- (NSMutableArray *)convertPDFtoUIImages:(NSString *)filePath
    // Declare all the resources and lazy-loading them later
    NSMutableArray *images = nil;
    CFStringRef path;
    CGPDFPageRef page;
    CGContextRef context;
    CFURLRef url;
    CGRect cropBox;
    CGSize pageVisibleSize;
    UIImage *resultingImage;
    CGPDFDocumentRef document;
    size_t totalPages, i, pageWidth, pageHeight;
    int pageRotation;
    float dpi;


    // process the file path and get the document
    path = CFStringCreateWithCString(NULL,
                                    [filePath cStringUsingEncoding:NSUTF8StringEncoding],
                                    kCFStringEncodingUTF8);
    url = CFURLCreateWithFileSystemPath(NULL, path, kCFURLPOSIXPathStyle, 0);
    document = CGPDFDocumentCreateWithURL(url);
    CFRelease(path);
    CFRelease(url);
    if (document == NULL)
        goto document_release_path;


    // get the total pages
    totalPages = CGPDFDocumentGetNumberOfPages(document);
    if (totalPages == 0)
        goto document_release_path;


    // create the array and standby for storing the output
    images = [[NSMutableArray alloc] initWithCapacity:5];

    // convert your desired DPI output
    dpi = 300.0/ PDF_DEFAULT_RESOLUTION;

    // loop through each page and process them
    for (i=1; i<=totalPages; i++) {
        // reset variables
        resultingImage = nil;
        page = CGPDFDocumentGetPage(document, i);
        
        // get page size
        cropBox = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
        pageWidth = cropBox.size.width;
        pageHeight = cropBox.size.height;
        pageRotation = CGPDFPageGetRotationAngle(page);
        pageVisibleSize = CGSizeMake(cropBox.size.width, cropBox.size.height);
        if (pageRotation == 90 ||  (pageRotation == 270) || (pageRotation == -90))
            pageVisibleSize =  CGSizeMake(cropBox.size.height, cropBox.size.width);
        
        // set dpi settings
        pageWidth = pageWidth * dpi;
        pageHeight = pageHeight * dpi;
        
        // get current image context
        context = UIGraphicsGetCurrentContext();
        CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
        CGContextTranslateCTM(context, 0.0, pageVisibleSize.height);
        CGContextScaleCTM(context, dpi, -dpi);
        CGContextSaveGState(context);
        CGContextDrawPDFPage(context, page);
        CGContextRestoreGState(context);
        
        // extract image
        resultingImage = UIGraphicsGetImageFromCurrentImageContext();
        [images addObject:resultingImage];
        
        
        UIGraphicsEndImageContext();
    }


    
document_release_path:
    CGPDFDocumentRelease(document);
    return images;
}
```


<br><br><hr>
## That's it
Yeap, that's all for PDF module.


<br><br><hr>
## References
1. https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIKitFunctionReference/#//apple_ref/c/func/UIGraphicsGetCurrentContext
2. https://developer.apple.com/library/mac/documentation/GraphicsImaging/Reference/CGPDFPage/#//apple_ref/c/tdef/CGPDFBox
3. https://developer.apple.com/library/mac/documentation/GraphicsImaging/Reference/CGPDFDocument/index.html#//apple_ref/c/func/CGPDFDocumentIsEncrypted
4. https://developer.apple.com/library/ios/documentation/GraphicsImaging/Conceptual/drawingwithquartz2d/dq_pdf/dq_pdf.html
5. http://stackoverflow.com/questions/13279719/pdf-size-in-a-uiwebview
6. http://stackoverflow.com/questions/14152477/high-quality-uiimage-from-pdf
7. http://superuser.com/questions/633698/convert-pdf-to-jpg-images-with-imagemagick-how-to-0-pad-file-names