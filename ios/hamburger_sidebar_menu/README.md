# SWRevealViewController - Hamburger sidebar menu
If you're a fans of hamburger sidebar menu, [SWRevealViewController](https://github.com/John-Lluch/SWRevealViewController) is a nice plugins.


<br><br><hr><br>
## To install and use
I'll be adding the note soon. For now, you can always refer to AppCoda note
as recommended by the README. Link is [here](http://www.appcoda.com/ios-programming-sidebar-navigation-menu/).


<br><br><hr><br>
## Transferring data between 'CoverView' with 'FrontView' / 'RearView'
To transfer data between the coverview (view before the SWRevealView) to
either FrontView / RearView, your 'prepareForSegue' is a little bit special.
You'll need to execute 'loadView' to tell SWRevealViewController to load all
the views before extracting the actual destination view.

### Objective - C
```
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"sw_front"]) {
        // Destination is always the SWRevealViewController
        SWRevealViewController *revealViewController = segue.destinationViewController;

        // Request SWRevealViewController to load all both the views
        [revealViewController loadView];

        // Front View
        /*
         * THIS IS THE NOTE FOR VIEW WITH NAVIGATION CONTROLLER
         */
        UINavigationController *frontViewNavigationController = (UINavigationController *)revealViewController.frontViewController;

        // Extract the view file from the navigation controller
        MyViewController *frontViewController = (MyViewController *)frontViewNavigationController.viewControllers[0];

        // Transfer data 
        frontViewController.user = self.user;

        // Rear View
        /*
         * THIS IS THE NOTE FOR VIEW WITHOUT NAVIGATION CONTROLLER
         */
        SideBarController *sideBarController = (SideBarController *)revealViewController.rearViewController;

        // Transfer data 
        sideBarController.user = self.user;

    }
}
```


<br><br><hr><br>
## References
1. http://stackoverflow.com/questions/24701633/swrevealviewcontroller-passing-parameters
2. https://github.com/John-Lluch/SWRevealViewController
3. http://www.appcoda.com/ios-programming-sidebar-navigation-menu/