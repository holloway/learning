# NSURL
There are differences in implementing file URL path vs. web URL path. The code
belows demonstrate the differences in creating NSURL with path.

<br><br><hr>
## Web URL
This is for web communications.

### Objective-C
```
NSString *path = "http://www.google.com"
NSURL *invalidFileURL = [NSURL URLWithString:path];

```

<br><br><hr>
## File URL
This is for locating a file in the phone.

### Objective-C
```
NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
NSString *documentsDirectory = [paths objectAtIndex:0]; 
NSString *filepath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, "file.txt"]

/* Specify the path is either a directory or a full filepath */
NSURL *validFileURL = [NSURL fileURLWithPath:filepath isDirectory:NO];
```

<br><br><hr>
## References
1. http://stackoverflow.com/questions/7111248/error-cgdataconsumerurl-close-write-failed-15
