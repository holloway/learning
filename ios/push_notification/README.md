# Apple Push Notification Service (APNS)
Finally, the holy grail: Apple Push Notification Service on iOS app side. In
this guide, we're focusing on setting up the APNS service inside an app project,
configuring iTunes Connects for APNS and handling APNS.

<br>
Before proceeding, you'll need to sign-up for Apple Developer Program. Also,
you'll need an existing APNS backend server that posts a notification. If
you know Rails, you probably can follow this [rpush](https://gitlab.com/hollowaykeanho/learning/tree/master/rails/gems/rpush) gem.

<br>
By the way, push notification **won't** work on simulator. You'll need an
actual device.

<br>
Currently, this guide is only support **remote notification**.
**silent notification** will be supported in the near future while
**local notification** will be supported in a different folder.


<br><br><hr>
## Setting your project into specific bundle identifier
The first thing to do is to create an app specific bundle indentifier. APNS
won't work on wildcard (*) identifier.


<br><br>
### 1) go to your developer.apple.com and login
The link is here: https://developer.apple.com/


<br><br>
### 2) go into "Certificates, Identifiers & Profiles"
Here, you'll see all your certificates, identifiers and profiles for all the
apps.


<br><br>
### 3) select "App IDs" and check for your app
Select the "App IDs" on the left sidebar, check if your app has a unique
identifier.

<br>
<img align="center" src="https://lh3.googleusercontent.com/GOTT3FDbNTnZrQy8wD1sFBZB9P64sNYNeUiRYz3oIqeDJ5WB9lXSDqNGacjLJxKUMmhceFO64Sp-yP5OCp6gEO1PsyuOccMTUb0FedI5MqYtEU43B82Bt-yjO8cuhHlkSLapyNw9a6R86V_rbv4qyqu0Gnbgw_YWTjbr0BWey-E4h8BwqwI8DH6dc1BKW6qiRCeipUc6AlmFi_bgnqDDGioHcNGOsjfeGCdKcDRlIXro3kciZ-Zh0zyXm7-ttjkDkai0yR2x32zTffiPoRUOXYENs_ZxF_Ha06woutGvoyp1NQQl4H3RqnypLKCFdmo-HnA_pHq_HVlj_DME0aVFMNc-MbBV7i1ItMzvFm3vG38lz8YvcHmpBo3YzSYLrUQjgodm5dyeCD-NzY45S6iV2PdqyJZEuAXmMJRoJF-daIJe95arYLy56SYCpCXtzB1vrFzHfgZuhBNADHMFMlOuv-CIPW-5DAB_NGaB5Ot1VlFPX9JMjagrPSwqp5sW9ZCinqHvZTJomTrS1TOeAnB-sv73T11oM1s_Y8QIAcuG_x6fIIhJP8KeRGuN2u9_eaO0iImAxpqVGBzJFhMKz0PxePy2yKMkTvO9=w1980-h1862-no" />

<br>
**from xcode**:
<img align="center" src="https://lh3.googleusercontent.com/JgzLqFHEPW_YqaIElec85oPvDDSqj0iUy72V8TUATJYinIk1WqxBM2qzdkgsB7g3K27zycW5So_G8CgRuiYGt9M0n9d7Ai7RI_HWITj1tf2yiXyWSuqfNuDldyNxmkuD8B5A8apTsBcBFiY9RcnRIUhm2Yd-mBaGA5c54Xn2Uwx7-Fap_dAoJnDvIZh_veOo84fRo4F-Wnosf1bwnGgOVLjqh19AiAunNsA62Xvx1rjc18l_I_3zjHNuGGB2sqc_JXCWltyXw2L3h5boeh3J1j1pTxbuc8A_mTRV-sbun1hpbALeRuszxHh6BV7rE-9WQAf5gcM4c0vMM5HnNQ1Mk4zHb1G5y79S8JeEjybpzmX367EMXXFBY0Mof23R88nMElPh3F_2aYxudQ9D_32t_cYO0HTQ5iMwYfVybMv_MlSj_i6F3Yh-dmzAlCApL164ci2xGhDxV82FO26cpT6t4U_QCGam-D8OXIT1YQ2b0uMVPhIOgakwSbW24WTHWX_6ETV3SUqoYI_KBd4QtqSKk-dOBsVUXkURvfchHTB7Nvtst2vhRcWaV_Po2Vg1A78w4UlorGsUW8yL_jpLIC1p45Z4BXnRIMNa=w2174-h464-no" /> 

> NOTE:
>
> APNS **only works** on app with unique identifier project. If you don't see
> any identifier that is identifical to your XCode app bundle indentifier, then
> you should create a new one.


<br><br>
### 4) create new "App ID" if you don't have existing app specific id.
Skip this step if you identified your new app id from step #3. Create a
new app identifier. Make sure you produces the result from step #3.

> NOTE:
>
> The values below are for demo purpose. You should replace them with the
> correct values.

<br>
<img align="center" src="https://lh3.googleusercontent.com/KVh7ysMsj9u4UvInhVqbAx0i_wuDkFM1rC08l1HfA4MdqW66gtuQjI-PHDiFGpxUIaEEBvTgDrLr798CshQlZk9rJVnUMqIhprKXbTCDKetgrGwsiqUwI2qCwVwe3-FxxfuuvFDz_3S7SVZR02XSxAnNy009Ri-PpIAhQcwoz2l8big2wPVH-HkLxJ9gZgI4ULAvR3Lgckbp8j5LdP8W6R55ibHuwnI20-yEuwq4VtC1NRzYsUDsfgeLyJqkewmCwG1ezlxVze-xu2C3mEckz3ec2BUNurZ601YvVq1ZkFHi0wTMr5eyRlDsLUSQtmyLD8YW3Vsb6ScMlwNDvnmkZjVdMLg6wkbvXnEkxr-Ltn0iLfyCt5W7Yn36RXr7Y4CkdrzXA0XH2gZm18J11sohu1o_qVzdcn6X9bL0jQmn8Zeq-qN-1fRaHMAWeZS_11IxzXMg3HqkF-zri9mE240aXqj9mEkuBsEFAC3oo-hsdfvUuDEkDZkGKyhAVyrEgrVd1I7l_CDxpwYqMmwsUA71Cy5BOsSGdmOWzbU-Sai2RkIkBB7FTVsZDi16DmZiPcNKu4WdhDE3CRAS1wBjsCHqXtJxzguzl01j=w826-h1600-no" />

<br>
After created, do ensure you update your xcode app bundle identifier is the
same as the identifier you've created.

<br>
**recap from xcode**:
<img align="center" src="https://lh3.googleusercontent.com/JgzLqFHEPW_YqaIElec85oPvDDSqj0iUy72V8TUATJYinIk1WqxBM2qzdkgsB7g3K27zycW5So_G8CgRuiYGt9M0n9d7Ai7RI_HWITj1tf2yiXyWSuqfNuDldyNxmkuD8B5A8apTsBcBFiY9RcnRIUhm2Yd-mBaGA5c54Xn2Uwx7-Fap_dAoJnDvIZh_veOo84fRo4F-Wnosf1bwnGgOVLjqh19AiAunNsA62Xvx1rjc18l_I_3zjHNuGGB2sqc_JXCWltyXw2L3h5boeh3J1j1pTxbuc8A_mTRV-sbun1hpbALeRuszxHh6BV7rE-9WQAf5gcM4c0vMM5HnNQ1Mk4zHb1G5y79S8JeEjybpzmX367EMXXFBY0Mof23R88nMElPh3F_2aYxudQ9D_32t_cYO0HTQ5iMwYfVybMv_MlSj_i6F3Yh-dmzAlCApL164ci2xGhDxV82FO26cpT6t4U_QCGam-D8OXIT1YQ2b0uMVPhIOgakwSbW24WTHWX_6ETV3SUqoYI_KBd4QtqSKk-dOBsVUXkURvfchHTB7Nvtst2vhRcWaV_Po2Vg1A78w4UlorGsUW8yL_jpLIC1p45Z4BXnRIMNa=w2174-h464-no" /> 

<br><br>
### 5) compile your Xcode successfully with the specific bundle identifier
You should compile your project once to ensure the project is working with
the new bundle identifier. 


<br><br><hr>
## Delete and refresh your existing provision profiles and certs
Once setting up bundle specific indentifer is done, you'll need to setup
your APNS in your app settings. Then, you'll need to delete the provision
profile created by XCode (or manual) to update the entitlement.


<br><br>
### 1) set your XCode entitlement for APNS
To enable APNS entitlement, you'll start off in your XCode project
capabilities. Enable the APNS.

> NOTE:
>
> For those who did not register for Apple Developer Program, you won't be
> able to proceed from here.

<img align="center" src="https://lh3.googleusercontent.com/AjsUlKCcTVpaXWVNmFHX4kKd_YhPOCI8Epuhk7RJGAtZH9k_47f3vqGGINPKIxZz70XA_jmc3VdXxzBY5caMqHFdooD-ZRQLgLNPT4VmV4kxzOvD_ef4t_fWwotR7KNFAlV-cCjQggELUsQ7xZNGjqTXJzU9es3HXz6G9_usuoz-CfMHPudA_qHUjxI5rE3Car592EkBH4bcBhx68SwAQ62wU5Ak_VS17WBIL6kkfac300vWTQFVNTpN4ZLPr4kNtz-UHPXUD5eLdkUXHeKZaY5FZQiDPgbNcngPOtlLQcjFi_RPv2KbpoC-BqAA2vkg5jrt9hti8s5MY-zb4pSpHL_mo04lRnmfBRmliaRCQnFtMCCGF-5XVw6lBMKpvvyo8c0mbFjJ0GqkE9yOojeRSgI_mUi3QnWznoOvM7nhnach3elppoMN90I-_SF4-9dxvLqxvutyuE0gKA8u9XU23yowWhEtssjN0AlVubwmi78_hgooOAlvou2FGqhinyIIAoKKMI-ZQDXtAeV37CBldV6t8M4ZH_7rkOVjJGL7O2jw77S7oyZ5rpLw_jYe0TFVWjz2qFChqY7g006RFQ-OriBQoaL-kIsA=w1210-h438-no" />


<br><br>
### 2) go to your app developer center, refresh and check for apns.
Once enabled, you should see your app has a APNS configurable entitlement.

<img align="center" src="https://lh3.googleusercontent.com/AKVas4cAPkV2Y-OuBcMkb7LTFyVjRUZKKFsTET-DKaIlCH-Kdo-BB6RAZNzsRrC-35XKaKEYdCJEs8Gi01yaweeoPGIxPfXUSjmrAjUzWY3Qn387wgsTgK4dozUovHB88A_xo48b8tuN5URNMMSXG8mYnuKmafq-lJwIrslKYuCe3WvzzhXEg2KkXFDKEqeAD46C9zaFtegkx_Aq4ZdiAvFqXM9Hg3MNcDucxWjZUwIp0RdY0XRawFExPnfXF8uOr19xGypttUt9duAGhwQu3pxBOXQh93mfuRmRSe1M1Qj1ikf1XP8u9g1RAI19PCbDENx-zV4FhVpRebMArNDm-Srtw_d_s1nREUahteA6Q5r8qmjF6xAPJszpRrqo1sXPGoLgSeYNCNZig2qzwJXPxOH5HCLWnhYB__BM1uCwzr5dZdSGXtKl_XHlAd1Nj2vA7Tphb9gs5NhKaJV9TWPWNwYLDVPMh4LEvrksURFwLV2xGymULBLXKgn9dTm--SviKCp76QYwSNT9XYRXKRKHXdnJFLFAerJoDnNHCovMHqFJEBIIhT-O6F40MISHTCRNU0e328R0TRoMI9gr77TMnAacu6c-X65Y=w2076-h1836-no" />


<br><br>
### 3) select "Edit", then go to APNS section
Let's edit the entitlement and generate the certificate we needs.

<img align="center" src="https://lh3.googleusercontent.com/yAaJlicWKuc5JHqWRckPvVeXxoBI5X4shW4-caA_C8Pe4YD2EOX6767FWEMfxXqYXWZCkyjr9iq_XNwqHcuasxqpSl0BhQfucwnmRzrt4myI9_CdkQjg_wdXrW61qcKzjVhUf3SuHEoCsCvxzcVgl3RrB3pa50oQq_WS0X2MGyJxiKnaieagJPwv6qbj4hiZgjnboy539O3H1pm0zLGq07iG2L-jsQYQv_3snNyyWavQ3JzIjiDzRP4inipy4lALAEWUmCKUZpKt_d-TA_pHfQ4Wh9U2HEw8PUptlgVEc5bk1If67V-GMu4aBmQ2ejsfMDar0U0KFlB6Iz4_BBCnHxDuvWdRTpToNHUlYBf36l7LfbjpeE-hiUCfHkFuIxLa2jkfCgOf9N3xMODdSB47PoD53BaFomAX-vYDAMD2ID0h9kciGJl-8wLLLfkCt9d343-THR4F20c-vIpsWY3h5J3RZLEKjalNfztKLMf7JpSeKbTcmMc2bfT9i2DF9titmPiAQPbA-A8c0GWYjnulX4jpaeOhjeHfcpbunf0IQ6fWbcuWXNC1z4G0wblqUiFP9x8KODo8NCjmn_VKkf8Dj_DWD_F3VAGi=w2076-h1836-no" />

<br>
Then to APNS section:

<img align="center" src="https://lh3.googleusercontent.com/ONZUTvE73Fjpq-8vMAK_DwxJsmpOd9Irx1nLpmExN0zQ7SXDYd0ozgZTZ18OEczSI-utBdqH43xGsdae2lE2EDNrtxShcVx1ybl08XQgfCas_dhyiliNvc2rrxLXgbHl1Fz0YhMl3jE9PxuRxw5zm68AJMloDIIxdGQ7f2MzN2gtAWdzY_FvQm7dwY6qNKEVxAcRnVhbi3yLWbDq6Zz9vDPk41qZ9mLFUkFyPyy3flL9G8Qg8BGt9SnJ_NzA3zeFqZPMPxfqnj8G5d2WyTZpjIWgcDav6ZRjDVWBh7vL0sv1CXJb9BMEF-Xi1fkA_XVk3D-Jvng67NYXLODqEGtqukH0ZC6ojj6MeIZ6i0_SIa4p3Wm7DVfXSVg_uQUZ1prEvlRsOhv49eyCZNnz14Q5UNrZuV1lkoY0ZHDrUUynq6naOKbExDXaroDqDL-voFiH_HYVksCgwPDiGRpjXov8PF8f8LykodOoQm-tVwZMbFQnpQn5I7eX7w22Y5h-mwACkKXZ0sxSBGo8p3UDQnI8qGmOqaTENIByzgbK_JBDGO148-76uq2efH75glVbpm9X4gDFoG2uIvzQpmPatVvfKlfvZVzFioJZ=w664-h404-no" />


<br><br>
### 4) generate the certificates (both developement and local)
Select the create certificates and follows the instruction to create the certs.

> NOTE:
>
> Only team admin / owner can perfrom a successful certificate requests.

When you're done and certs are available, you should your APNS is no longer
"configurable" but instead: "enabled".

<img align="center" src="https://lh3.googleusercontent.com/NheI3PIInJD0NU3x16spCq2c-tvLb_Qnlz9ISJfPC7p9VGxUYS9gUHa2cXpV66aEl9tp8HgDaLnic6DnDY8NN4phidPsJSAEJlXaVTn9nLXqFqdi5gSUGu4_HLnzzBFIeuRUlOlE4h2rs8tTuutXvnbYhGHouO6TXZjnqobzPszuQZLhpdZ-SnJpqqjqM4q9gFzgZY0xvlXD7lriOyfixPtH4MgQpdwbOgtMuzjFTYE2qumkNgyPfxaGLp8Wmg1krp-3XVi82r2A6s75h9QsRfitbCz9-XQaDUP2L5llxW1f-fBt4wsLbfTQSQ1sviPlIQtBLG7FwTArNH152YSh3vJEHCXItkk62qvsPz4x-4G425dnA2KcPdZ2JSD1eJ67zAD1WQ2oKX3X344ZB0SpD5XH5O1id29H7kYz6vCzsrPn5hWJeqmyVra0wTtF__kOibWVPhEaNLz-iP6oVXxI8AndnZopo1jagzhWEESVlkddkuvlTNV1bAFFa4MwZnpQUZi-m_wyZf3DvUrh2BpcaRMdFdc1khsoEPFlSqCXqT4Fl8ccF18BsUJ-SC76OhjSp5Ag4JVqV7OZp-E9LxvnfDWhULVaOdVr=w2062-h1822-no" />


<br><br>
### 5) go to profiles and find the your project's provision profiles
We'll now need to refresh the provision profile. Go to the provision profiles
and check your listings.

<img align="center" src="https://lh3.googleusercontent.com/9HlfR8qkr16Q6Q8ZSs8-wb6uvOi0xnanevhh7lruBEEzjPXRKeF5qTJA6NoP6S4cUv0KV9XP8oWxk780ptoGhCzptzaAcidgs5XdzRfDpHpBbjTblRkcPPMu5aXKvH39BrLsPuDMbsXaJxg4_8EmIwW0EH4vXrviA6Y4Lw9Z-Ep0iDF0wqCUZi7HiMyk-1DMTGP-fYim5VMicf3CZIAw7LkHOZTefcWjaU1aCTgF53bMAAj0nPyj17_YioJs0nFPkW-SPHRxToOusfzghe_Sn28GDzzjVWCGViwgWL_9fROiZiGk_LTpv4kVs8G1SOjiAFbQZivggy9sIcJwzBESiNXUsKxYYiYjT-tuuGkyRnWtOpoWPM1oO6O_wcjJy5rXyh_GfUcWvzcs42Gs7UpuRt954jiuGLW3xCHMhh5THAsdasXaZEtDEj5Xbb07EC4fDbq07_LMvIcPvNOLvkH26dya3OcHg5_zhjzJfn4nDd2wJgzul4WTi936XOTtias8mg5Fc5tssKeyWs1B-YnuXOaF6XgTi4H5nNq6OpTLKe-rBOJECCvoUTwnUuRxXeWNa2eSfzEh1uh1FdvRXMwEx8O6lr31i2Q6=w2062-h1882-no" />


<br><br>
### 6) check the current active profile for APNS
Now check your current working provision profile has APNS enabled, it should
look something as such:

<img align="center" src="https://lh3.googleusercontent.com/MF5mvbllmmg44vMD55c6vatLvm7DpDPg2j3NOu7vl_onn6f_zu-mBYTdokrsqDc8Rl2L5gregpRyO91xoGtiSjDxSLjutNzc_tawWXWiqF6zMcK52-S8B60Ok-JMYlWkZndXrLXh7Wvb8N8kvau8xYjSG_RP_Z8kKS8g52mEik27GNilJ10r_VKhqcmYszf1lC3t8u84c3lz8O0hOKKEAl9fQVWGhkiW8kkng8016NGGNxBqPVreXf7npkh7fWF_FAQNo6OLP3Lq3yjLKzskjTjbqkcSBl8tteAlf-c2rP011USlRfwPdAG_N3YJ043sSJh4ckfFyxIUj2dRNKMwAa4qmjqzD9mM4TUktMY6gLS1UTX8vJKIUdlnFCXQKXhOFj_PEDIslyQRqcUPjhy4l7TMODO2xyALfUGxw6eEw-VEWc87_JSbKnQGlndn1YozZC9krvaLec8mUbTUzSb2hGYp_9kXwld2oSF7hp69CDj05jPTvjupIoQ2iZ5ORI3j0fB55RuEcTxwd85p1z4GhENoHhD-MogexzgOQAFkBPp5fE0wqsXYnU43b6YWDasuIlgiVOp2PCjNktH5uMKWdI0QQWifnDyV=w2092-h1910-no" />

<br>
If you have the APNS enabled, you're ready for the next section. Otherwise,
proceed to next step.


<br><br>
### 7) delete the profile and recreate a new provision profile
Skip this step if your provision profile in Step #6 has APNS enabled. If your
provision profile does not contain APNS enabled, click the delete to
erase it (you would not want that outdated profile). An example of a profile
without push notification:

<img align="center" src="https://lh3.googleusercontent.com/vkI7Vagbyyx9Q0t6bosps_H4Kdt54m6K7p-pkocbBTaSerjuaSFLWj8xwLvNs_RLSLM6ijs1JSAfEou6xkpsLr-YVE4XGEv12f_kGUfZOZ5DS9kueW0qSKV_j9Mvnr3wr-TvH-qX9Be_Oo4MBsRD7fQYsEFVb1cA69MkjHG-qU2mayKuGqmQVEBcUIW7NjCUH0Zl6a9vAB9A0H15Td31mMmYig4fHLMgWFpbJEqoVgm_WaGa405YFetAYyxkT4sVJa9Z_yu8cz_PtSRVsuVGuM2PDSEdMAa_XtHzih6nJbV6vw6SfOizgs1NlJPQX2ovknwEnc8dAw3YgFqwInY9_C9QAM-fkqviXqyYrczWnzn_9Y0ev9wh6diMCRpGdqBd0dyRc0bs0FrTavCT1HKoy0lU1icZPe-zkAz7nD495_K9LoroNS7FPfvcidyZo7Uug2y28Uj_t-iw39XTlm7YrVOGlUW8HbCrvrngPu1DsoR_5xAv6--27lgpXhjQ-WVs58lOKgY9F5FJXOoOIelXb-c60aSe1juQVAyXs3aCvhXZMGZvconG4WuvTUDH87cC--DKFvBqwacBkMuKLN6DKk_nIrK7Wfmy=w2092-h1910-no" />

<br>
Go to your XCode, head for your profiles and select view details.

> NOTE:
>
> in your menu bar:
>
> 1. "XCode" > "Preferences" > "Accounts" > "Your Apple ID" > "view details"

<img align="center" src="https://lh3.googleusercontent.com/MdvONDu22znRAqCz-bEWJPTCVzP59-mERKHqUnZBDrER699O7tWFBeoFT3V4U7nQ58zSobVm7krTKHcvuEq1qC6382AeqbVES5ysEJvNMKBek01SjLyGhGDecPEr6mloJ8Lp2sBTiNAkAeKFyvJJiRxw9GPfTFEodgEjXdTUj2D6cIHsjxqR0WrcSwQcI3IkeDLrGj6k8_Wk3EmlVjCenuX_oKU_eYP_1rbdp_sddvOgH7e9l9jyu7s72_WdxQP_3bG3yaAiz0XkC7RAEnUfeaW05D1cgH1SrpwYnFYK1kt_Xo62KuFzQ_8ZD2OT76KZ7W7ylGPleLZCvMO_Tq7Z7dtG2XsAIzHyrEQUUs2UBZN_-wUgFMInSDX1XZl--9NrQBBWfhZftrILJjXNyYwv5PAXhBjCRV6Av4K2O_0TSOk7vFibE55_ByQj1oWxmALMTQWPv5X6rc074l4cNKrkAMB9MEJWPnjmxWZ_akxlFZ6lAXKb4c2aHaAkX6QZfvbMHoa_PxVi-Tg-9_vgYSADlu3pr1cswexhXQ5JvX2IWGRum4YJ6zd8OcUV6SClt4V9y3SmsIyLBv_HPGUJlRU6Y3P872h8JTeV=w2088-h1310-no" />


<br>
Delete the old existing app provision profile. 

> NOTE:
>
> 1. Select the provision profile.
> 2. press "delete" key on your keyboard.

<img align="center" src="https://lh3.googleusercontent.com/FyOhO8CFOsrV8hMnOwAPnRlUTPd3hN8MSwprppvucWKqzOmpxLMVRNLsVP740f6edU4ZP1qmKYzLJa4iKS7OXr-AGI-JMhWd26oUX8Wbe7Ulg03MFJgT3mZ7HDiSvNYP52iJ0IbYIH-gQdcXmsY3l7dzUXcwpKb0Mj5px8ELtnybXrRwnb-wX9Dvjdt7qCa0-Dt9aUBFOomkNjYjrrgfbZ7eM-F0oyPDAuQspYc9p-GzlBs0nSfLG8urP0rvK8Qo0kRthRoXhMfwCbiBexcW3uwPrVYC_GXb7Is1tPcAfaw39WQXL5D2QIhqAmOdS5V-L1uCov9PV4LsY46YViz-unQa23V24XyobZN5VLGGXOFp3R_Ik-HhPYr7qidX2jLs0zCWsuAzc40ynp9D8VscHmbYOu8zjFikJxCQVedeeM_kYh28PcGOdTAc00udtE52Lz3azmwcTlPnrFU-64ta6475aLJmDZUP6_TTgHxQKO2c0Wo4W5REGdptRQFC2dGYvqR5XhQxoT2aYeyohvA32byGj9cdukxewc3qbUIj0Z0LjwofjKBzeKpLnChZfLgoJOVxsPZagSSHX05v08EBIW29k7cesHVy=w1024-h1106-no" />


<br>
Once your profile is deleted, go to your XCode and perform a "clean compile".

> NOTE:
>
> in your menu bar:
>
> 1. "Product" > "Clean"
> 2. "Product" > "Build" 

<br>
You should get a prompt that profile is missing and XCode will handles it
automatically. This is the step where XCode creates a new provision profile for
you.

<br>
Once completed, refresh your developer page and re-check the new profile in
step #6.


<br><br><hr>
## Generate and install APNS certificates for backend server
All right, now we are done setting up XCode for push notification, it's time
to setup the backend server for pushing the remote notification. By
[Apple documentation](https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/ApplePushService.html#//apple_ref/doc/uid/TP40008194-CH100-SW19),
all notifications are routed to the Apple APNS server. Hence, conceptually, the
backend server is isolated from the mobile device by Apple APNS server.

<br>
For backend server to communicate with APNS server, you'll need the app's 
certificates (different certificates for development mode and production mode).
Hence, this section covers how to generate the certificates for the backend
server.


<br><br>
### 1) go to your "Keychain Access" > "Certificates" in your local mac
Launch your "Keychain Access" and go to "Certificates". Find your apple
certificate for push notification.

<img align="center" src="https://lh3.googleusercontent.com/qcKU3uQoSVlP-haSYuX5CMEHPddHdOv_mlZtmgQyBGTBWNcZ5WMORWE5AckDvYr6X-NCbsxxsV4f9najh853DzvnDH2ff088O7L-PYQ5MBng42nFJj580gC4hSRlWdEePdrS_IDZSIkRmxB8UXT6e-bCDE6oPwF5Db3XaHigskLDP27YyA4RkKnfr2ANdDmTcOYZpqGM1ckFRTnTn8aiajOpnwe6aCUsoJbOzv6mN6RQq2WtyauE-gA8lzamlrrPnhfWNeTShkpKelXLAL88kYiwmGBAQ8lyUY8EHD-j1-t9EDMWdroGoh0K7iPoF7R5HIYpf5r7xPtn9fHTqSFSdVFC7v5aTc4vklELv-YxUXZjZ-Q2NXo3Su0ap8vs8d1OcQTAU7lT3O0NnRdO7WRp-r7QSHPKHqy-EapgAMPkxfbDy-QmTaus8uCgXC9x3rujj9YCK8MQw1a4d5-_lS79JtLJhZvEU4j_Vu62sOZZlrZfDbsMGkPSZukeaIAfOuHgpzoHQkRMP6AbifR6dcRPYX13LuYJZKC7ggBMc9yTQjk4EI2SRZQwNG8NwD5ucCD7z6M9uzdVmHALIuFVMM18-FkTbasceWK0=w2336-h1172-no" />

> NOTE:
>
> In this picture, the blacked value is the app name.


<br><br>
### 2) right click on the cert and "export" it to a .p12 file
Right click on your app APNS cert and export it to .p12 file.

<img align="center" src="https://lh3.googleusercontent.com/ItkQieCEGxFMAsU-iCIvMCDKfJcxvQmSmZ8LWhlMnB0i4gU897cjMYtDlATqLttzpTdkLiz3tg7BOU06MRz-7-BO2uXGpk8_cRiVMOv33GnSLRqeYvPaUrT0fw5vehwLsjiO5-Rxrs7oRHTGP_uKZScOqqSJViVrmX_FdYtYf5i_KsJDd_HE0ygEp4kDYM6mXtIdD4UgXxT1myAvr9QAaFur4IWFrLfMgc8T1RoIxhNv8iBIOVU4_XOQ8rWCb4XftiWG1J4C4gV8E3LaFE2sOgIYQcerZGw0cAfuzmzhhwabpvjfJP0LPKUtD8fQ8xEZkuPSB0NiY566Kwr9uT_lDJ0epvBmcyFDNZVdjvpfWLJhLnfUzdGdkp72eXoysR1xd_cPj0ccfG9TDW4Pmty--VcP7ZwuNOQip4n9Z4yLYN2ExtYmoS1fDikw2nkBr9uAXWlf2-oBJ-P2bO8PK10MBIrMMbcXjRGrVSQMD886JQ4h6Ur76sAi47QXhGh4jD8MLHFXPrV4b2WUwcDmsuBg_VWo-1xqtuqQlkz53tGYBkmCo3Mgh5r2zu4ctaO2bQ5kfiRr-5i_KLrGoA4zPClLTdlny6Y7uIAw=w2310-h1114-no" />

<br>
Then, export it to .p12 file:

<img align="center" src="https://lh3.googleusercontent.com/ZkHOTWgyU4Sn2YHne5Zqn41njJna49ZFq7fVgbWJmmCRua9MN7Dn_UVeiqHQGbzkiDvraRH-KSZltAnDkpuhwL_1-VRY0fbsJjoz4mse9IdEUO5RkAMEXpCb7lS3u5yRI9U5FlOntOWXMehR8LQ8gnunU_Mww5T7rAOO6neq1VbwF9Jd_YThBcpbVcdufXXM9yXZ1oelpjlLInfVyNklmVxE4HGehZYlgkQVc8TxU3UnREf3tE71MOe8LWel3ya24CDcjEcX0pTTD3X-dtt9gLcihsamZk8hwiOC0dGZk3fo3wmK7qp-cw-NvQViGJa2WlwzqbvpDcbTs0bD_EPUOq3Lcj7Trv4-WfJijYRT9eGr3gc_u6ge33VBH-4L391GxfkmebxoGrr6RBbrVLErcqJxfUPyt3-_kOvBmc5y_gDe_p3Yk8vbxXi2rNmtg-9n2l89CCy0BsJUklj-_pW93hzm_Vf2xC9iJ_uiHkmptqW_qzvNK1GaEmKuy41dgbYDgTn5gy85M1QJOaos08sMV6yFUR4VBZ3QYYmdUSziUDa0LOe_4Djh-Ot-fIuxYN36rxmGeXLGsMdnmjizM5ZSxLZSx8ab1UcE=w848-h452-no" />

> NOTE:
> 
> 1) You can name the certificate as you want. I personally make it
> "app_name_sandbox_apns" for development mode or
> "app_name_production_apns".
>
> <br>
> 2) As you click "save" into .p12, you'll be prompted for securing the cert
> with a password. You can proceed with either blank one or with a password
> of your own. You got to ensure that your backend server do knows about
> this password as well in order to use it.
>
> <br>
> 3) Once you secured the certificate, you'll be prompted for your account
> password to export a cert, proceed with your account password.


<br><br>
### 3) go to the .p12 folder and convert it to a .pem file
Open up a terminal, go to the folder where it stores the .p12 file. Execute
the 2 following commands to generate the cert.

```
$ cd ~/Desktop  # or the folder name
$ openssl pkcs12 -in app_name_sandbox_apns.p12 -out app_name_sandbox_apns.pem -nodes -clcerts
```

> NOTE:
>
> The actual command for generating the pem file is the ```openssl``` line:
> ```
> $ openssl pkcs12 -in <.p12 file> -out <your .pem cert name> -nodes -clcerts
> ```

<br>
If your cert is secured with a password in the earlier step, you'll need to
unlock it when openssl prompts you to do so.

<br>
Upon successful conversion, you can now send the .pem cert to your backend
server. Again, if your cert is protected by a password, your backend server
do need the same password in order to use it.


<br><br><hr>
## Setup iOS apns permission request
With everything up, it's coding time! APNS is relatively quite simple in iOS.
There are 2 general steps:
1. initialization
2. delagate handling

<br>
No worries. All codes are only inside ```AppDelegate```.


<br><br>
### 1) initialize your push notification
To initialize APNS, you'll add the following codes inside your
```AppDelegate.m``` file:

```
@interface AppDelegate ()

...

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    ... 

    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationType types = (UIUserNotificationType) (UIUserNotificationTypeBadge |
                                                                 UIUserNotificationTypeSound | UIUserNotificationTypeAlert);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];

    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                                               UIUserNotificationTypeSound |
                                                                               UIUserNotificationTypeAlert)];
    }

    ...

    return YES;
}


...


#pragma mark - delegates
// push notifications
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    if (notificationSettings.types != UIUserNotificationTypeNone) {
      NSlog(@"Permission granted by user");
      [application registerForRemoteNotifications];
      
      if ((notificationSettings.types & UIUserNotificationTypeSound) != 0) {
        NSLog(@"[Permission granted but not sound");
        // ...  /* setup your notification without sound */
      }

    } else {
      NSlog(@"Permission not granted by user");
      // ...

    }
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
  // received device token from Apple APNS server
  NSLog(@"%@", [self convertDeviceTokenToNSString:deviceToken]);
  
  ... // save the token somewhere and post the token to your backend server
}


- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    // handles fails registration for push notification
    NSLog(@"deviceToken: %@", [error localizedDescription]);
}


...


#pragma mark - private methods
// push notifications
- (NSString *)convertDeviceTokenToNSString:(NSData *)deviceToken
{
    const char *data = [deviceToken bytes];
    NSMutableString *token = [NSMutableString string];
    
    for (NSUInteger i = 0; i < [deviceToken length]; i++) {
        [token appendFormat:@"%02.2hhX", data[i]];
    }
    
    return [token copy];
}
```

> NOTE:
>
> Let's go from top to bottom. At the very start, the app always request
> permission for registering notification in the
> ```didFinishLaunchingWithOptions:``` function. Before registering, the app
> will cross check backwards compatibility between iOS8 before and after.
>
> <br>
> Upon successful registration, the function
> ```didRegisterUserNotificationSettings:``` is called asynchonously. This
> method contains the permission user permitted you to use. In this case,
> you'll need to check through the permission type and respond accordingly. 
> In the example above, if the user permittted the app to receive notification,
> the app will proceed to register remote push notification with Apple APNS
> server.
>
> <br>
> With the remote registration completed, either
> ```didRegisterForRemoteNotificationsWithDeviceToken:``` or
> ```didFailToRegisterForRemoteNotificationsWithError:``` is called.
> A successful call returns an APNS device token locked to the app
> (not the device). Otherwise, check the error and fix it accordingly.
>
> <br>
> True story:
>
> If neither is called, you might want to check with your device's mobile data.
> APNS uses mobile data as primary internet source instead of wifi. Just in
> case if you call into this senario, this [document](https://developer.apple.com/library/ios/technotes/tn2265/_index.html)
> is the only note for you to perform troubleshooting. Good luck with black-box
> debugging!
> 
> <br>
> Once you get the apns_device_token, you'll need to POST it back to your
> backend server some time after. Your backend server can only use this
> "apns_device_token" to push notification to that app on that particular
> device. At ```AppDelegate``` point, the token doesn't mean any relation or
> association to user or anything else. It is until user uses the app
> authentication (sign-up / login), then this token establishes a relationship
> with the user.


<br><br>
### 2) setup apns handling
Once you can see your apns device token on your phone, it's time to implement
the delegates. Push notification won't behave like the ordinary "ding" and
banner type when your app is in foreground. You'll need to implement your
own handling if it's that the case.

<br>
For me, reproducing the "banner" effect is quite easy using 3rd party
frameworks like [TWMessageBarManager](https://github.com/terryworona/TWMessageBarManager).
Let's implement it.

<br>
For every remote notification arrives, ```didReceiveRemoteNotification:``` is
called. Hence, we can go ahead and add the very basic delegate model:

```
- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo
{
    if([application applicationState] == UIApplicationStateActive) {

        // extract message. Bail if there is no messages
        NSString *message = [NSString stringWithFormat:@"%@", userInfo[@"aps"][@"alert"][@"body"]];
        if (message.length == 0)
            return;
        
        // extract title. Set to 'Update' as default
        NSString *title = [NSString stringWithFormat:@"%@", userInfo[@"aps"][@"alert"][@"title"]];
        if (title.length == 0)
            title = @"Update";
        
        // show messages
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:title
                                                       description:message
                                                              type:TWMessageBarMessageTypeInfo
                                                          duration:3.0];
    }
 }
```

> NOTE:
>
> This style assumes your "alert" has "title" and "body" entities instead of
> "message" itself. Then, we pass those information into TWMessageBarManager
> to present notification.


<br><br>
### 3) test it out
All right, now everything is up, give it a try. Post a notification from
the backend server to the app. You should receive a standard notification if
your app is in background while the blue TWMessageBar if your app is in
foreground.


<br><br>
### 4) customize your own remote notification functionalities
Now that you have your entire push notification system, you can further
customize your ```didReceiveRemoteNotification:``` to your liking. For me,
I customize it to have my remote send any kind of notification at my will,
something like:

```
// Notification Types
#define APP_APNS_INFO_TYPE                  0
#define APP_APNS_SUCCESS_TPYE               1
#define APP_APNS_FAILED_TYPE                2

#define APP_APNS_DO_UPDATE_USER_CREDITS     1


- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo
{
    if([application applicationState] == UIApplicationStateActive) {
        NSInteger performKey, type, requestType;
        
        // check request type. Look for positive value. Anything 0 is default
        // or assume it to Info type.
        requestType = [userInfo[@"aps"][@"alert"][@"type"] integerValue];
        switch (requestType) {
        case APP_APNS_FAILED_TYPE:
                type = TWMessageBarMessageTypeError;
                break;
        case APP_APNS_SUCCESS_TPYE:
                type = TWMessageBarMessageTypeSuccess;
                break;
        case APP_APNS_INFO_TYPE:
        default:
                type = TWMessageBarMessageTypeInfo;
        }

        // extract message. Bail if there is no messages
        NSString *message = [NSString stringWithFormat:@"%@", userInfo[@"aps"][@"alert"][@"body"]];
        if (message.length == 0)
            return;
        
        // extract title. Set to 'Update' as default
        NSString *title = [NSString stringWithFormat:@"%@", userInfo[@"aps"][@"alert"][@"title"]];
        if (title.length == 0)
            title = @"Update";
        
        // extract custom 'do work' method to perform some custom work
        // during foreground running
        performKey = [userInfo[@"aps"][@"alert"][@"do"] integerValue];
        switch (performKey) {
        case APP_APNS_DO_UPDATE_USER_CREDITS:
            [self performUserCreditsUpdate];
            break;
        default:
            break;
        }
        
        // show messages
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:title
                                                       description:message
                                                              type:type
                                                          duration:3.0];
    }
 }
```


<br><br><hr>
## That's it
That's all for remote notification. Hope you enjoy playing it. Word of advice,
please do spam your co-worker's phone with push notification. It's fun.

> NOTE:
> 
> Just kidding. Please don't do that to your customer. 


<br><br><hr>
## References
1. http://stackoverflow.com/questions/9372815/how-can-i-convert-my-device-token-nsdata-into-an-nsstring/16411517#16411517
2. http://stackoverflow.com/questions/25933825/how-to-interrogate-uiusernotificationsettings-types
3. https://developer.apple.com/library/ios/technotes/tn2265/_index.html
4. https://developer.apple.com/library/mac/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/ApplePushService.html
5. https://developer.apple.com/library/mac/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/IPhoneOSClientImp.html#//apple_ref/doc/uid/TP40008194-CH103-SW2
6. https://developer.apple.com/library/ios/documentation/Miscellaneous/Reference/EntitlementKeyReference/Chapters/EnablingLocalAndPushNotifications.html#//apple_ref/doc/uid/TP40011195-CH3-SW1
7. https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/ProvisioningDevelopment.html#//apple_ref/doc/uid/TP40008194-CH104-SW5
8. https://medium.com/@diegoy_kuri/mobile-push-notifications-on-rails-2ef1ea4eff67#.33cwpky4h
9. http://stackoverflow.com/questions/24664224/rails-rpush-gem-does-not-sending-push-with-no-error-checks-options
10. https://github.com/rpush/rpush
11. http://stackoverflow.com/questions/6856767/local-notification-in-foreground-in-iphone-sdk
12. http://stackoverflow.com/questions/24986123/ios-didreceiveremotenotification-for-specific-view-controller-class
13. https://github.com/terryworona/TWMessageBarManager
14. http://stackoverflow.com/questions/6856767/local-notification-in-foreground-in-iphone-sdk
15. https://www.raywenderlich.com/123862/push-notifications-tutorial
