# Date Time
Dealing with date and time in iOS app? This note is mainly to show how to
parse and generate date and time in a particular format.

<br><br><hr>
## 1) Generate a date time in a specific format
To generate a date time, we use:

### objective-c
```
NSString *datetimeFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init]; 
[dateFormatter setDateFormat:datetimeFormat];
[dateFormatter stringFromDate:[NSDate date]]; // => date in the format type
```


<br><br><hr>
## 2) Parsing a date time in a specific format
to parse a date time, we use:

### objective-c
```
NSString *givenDate = @"2015-09-03T12:56:37.922Z";


NSString *datetimeFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init]; 
[dateFormatter setDateFormat:datetimeFormat];
NSDate* date = [dateFormatter dateFromString: givenDate];
```


<br><br><hr>
## 3) Get current timezone
to get specific timezone in certain language, we use:

### objective-c
```
// english in standard localized name
NSString *timeZone = [[NSTimeZone localTimeZone] localizedName:NSTimeZoneNameStyleGeneric
                                                        locale:[NSLocale localeWithLocaleIdentifier:@"en_US"]
                     ];


// current locale language
NSString *timeZone = [[NSTimeZone localTimeZone] localizedName:NSTimeZoneNameStyleGeneric
                                                        locale:[NSLocale currentLocale]
                     ];

```

> NOTE:
>
> For localizedName constants, you can read this [link](https://developer.apple.com/library/ios/documentation/Cocoa/Reference/Foundation/Classes/NSTimeZone_Class/index.html#//apple_ref/c/tdef/NSTimeZoneNameStyle)


<br><br><hr>
## References
1. http://stackoverflow.com/questions/18682862/nsdateformatter-for-rails-4-0-created-at
2. http://stackoverflow.com/questions/10772033/get-current-date-time-with-nsdate-date
3. http://stackoverflow.com/questions/32450104/how-to-parse-datetime-in-objective-c
4. https://developer.apple.com/library/ios/documentation/Cocoa/Reference/Foundation/Classes/NSTimeZone_Class/index.html#//apple_ref/c/tdef/NSTimeZoneNameStyle
5. http://stackoverflow.com/questions/31338724/how-to-get-full-time-zone-name-ios
6. 