# NSString - Message Bodies
`NSString` is the common string messages using `@""` in objective-C. It holds
a series of characters together.


<br><br>
## Generate Random String
To generate random strings, you can use the `char` randomization method.
Example:

<br>
### Objective-c
```
- (NSString *)generateRandomString:(NSInteger)length
{
    int i;
    NSMutableString* string = [NSMutableString stringWithCapacity:length];
    for (i=0; i<length; i++) {
        [string appendFormat:@"%C", (unichar)('a' + arc4random_uniform(25))];
    }
    return string;
}
```


<br><br>
## Refererences
1. http://stackoverflow.com/questions/2633801/generate-a-random-alphanumeric-string-in-cocoa