# AddressBookUI Framework - Selecting Contact Data from Address Book
Want to let your user to get contact details from addressbook / Contacts?
AddressBookUI is for you. AddressBookUI framework requires user permission
before usage. Otherwise, the app will crash.


<br><br><hr>
## Steps
### 1) Include the AddressBookUI header
Firstly, include the header for AddressBookUI framework.

#### Objective-c
```
#import <AddressBookUI/AddressBookUI.h>
```


<br><br>
### 2) Add the AddressBookUI delegates
Add the delegates into your class interface.

#### Objective-c
```
@interface MyViewController () <ABPeoplePickerNavigationControllerDelegate>
```


<br><br>
### 3) Create a property to hold the modal view (faster loading)
This step is optional. I recommend create early mainly due to its UI loading
(somewhat laggy).

#### Objective-C
```
@interface viewController

...

@property (strong, nonatomic) IBOutlet ABPeoplePickerNavigationController *picker;

...

@end


@implementation viewController

- (void)viewDidLoad
{
    ...

    self.personPicker = [[ABPeoplePickerNavigationController alloc] init];
    self.personPicker.peoplePickerDelegate = self;

    ...
}

@end
```

> NOTE: 
>
> You'll need to set it to strong because the controller will lose itself prior
> its creation. It's a modal prompt.


<br><br>
### 4) Create your IBAction to initiate the action
For showing the address book, you just need to present the picker view like
a model. In this case, I recommend warpping it inside the permission checking
to avoid unintended crashes.

#### Objective-C
```
- (IBAction)didPressedContacts:(id)sender
{
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            if (granted) {
                [self presentViewController:self.personPicker animated:YES completion:nil];
                return;
            } else {
                [self contactDenied];
            }
        });
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        [self presentViewController:self.personPicker animated:YES completion:nil];
        return;
    } else {
        [self contactDenied];
    }
}

...


- (void)contactDenied
{
    [self featureDenied:NSLocalizedString(@"Contacts", @"contacts feature label for permission denied alert notice")];
}


- (void)featureDenied:(NSString *)feature
{
    NSString *alertTitle = NSLocalizedString(@"Uh Oh!",
                                             @"feature denied alert title in scan view controller");
    NSString *alertText = [NSString stringWithFormat:NSLocalizedString(@"It looks like your privacy settings are preventing us from accessing your %@. "
                                                                       @"You can fix this by doing the following:\n\n"
                                                                       @"1. Close this app.\n\n"
                                                                       @"2. Open the Settings app.\n\n"
                                                                       @"3. Scroll to the bottom and select this app in the list.\n\n"
                                                                       @"4. Turn the %@ ON for allowing access.\n\n"
                                                                       @"5. Open this app and try again.",
                                                                       @"feature denied alert message in scan view controller"), feature, feature];
    NSString *okActionLabel = NSLocalizedString(@"OK",
                                                @"ok button label for send button in scan view controller");
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertTitle
                                                                   message:alertText
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:okActionLabel
                                                 style:UIAlertActionStyleCancel
                                               handler:nil];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

```

> NOTE:
>
> Basically, the idea is if the authorization is denied by default, you
> shouldn't let user to access the Contacts. It will crash the app. To work
> around it, we prompt the basic user alert to guide user for re-enable
> the permission (```featureDenied:```).


<br><br>
### 5) And lastly, run your delegate methods
There are 2 sections to consider: cancel and selected. For cancel, simply
dimiss the view. For selection, you'll need to process the data you'll use.
Example belows is using phone numbers for a given person.

<br>
#### Objective-C
Cancel:
```
# pragma mark - AddressBook
- (void)peoplePickerNavigationControllerDidCancel:
(ABPeoplePickerNavigationController *)peoplePicker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

```

<br>
Selected:
```

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
                         didSelectPerson:(ABRecordRef)person
{
    NSMutableArray *phone = [[NSMutableArray alloc] initWithCapacity:1];
    NSString *number = @"";
    ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
    long count=0, i=0;
    
    
    // dismiss the compiler in case we need to use action-sheet for multiple numbers contact
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // count the number of contact
    count = ABMultiValueGetCount(phoneNumbers);
    
    // process all number of contacts
    for (i=0; i<count; i++) {
        number = (__bridge_transfer NSString*) ABMultiValueCopyValueAtIndex(phoneNumbers, i);
        [phone addObject:[NSString stringWithFormat:@"%@", number]];
    }
    
    // check for single number contact
    if (phone.count == 1) {
        [self processOutput:phone.lastObject];
        return;
    }
    
    // ask user to select the number for multiple numbers using ActionSheet Model
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (number in phone) {
        UIAlertAction *action = [UIAlertAction  actionWithTitle:number
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                            [self updateFaxNumber:number];
                                                        }];
        [alert addAction:action];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}
```

> Note:
>
> You might want to read this link for other information extraction.
> [link](https://developer.apple.com/library/ios/documentation/ContactData/Conceptual/AddressBookProgrammingGuideforiPhone/Introduction.html)


<br><br><hr>
## That's it! 
That's all about addressBookUI! Feel free to play and load the contact data.


<br><br><hr>
## References
1. https://developer.apple.com/library/ios/documentation/ContactData/Conceptual/AddressBookProgrammingGuideforiPhone/Introduction.html
2. https://developer.apple.com/library/ios/documentation/ContactData/Conceptual/AddressBookProgrammingGuideforiPhone/Chapters/QuickStart.html#//apple_ref/doc/uid/TP40007744-CH2-SW1
3. https://www.raywenderlich.com/63885/address-book-tutorial-in-ios
4. http://stackoverflow.com/questions/26015865/how-to-pick-a-contact-from-address-book-ios8
5. http://stackoverflow.com/questions/29982983/how-to-select-contact-from-address-book-in-ios
6. http://stackoverflow.com/questions/21747066/how-to-ask-user-for-contact-permission-to-access-then-make-and-open-contact
7. https://openradar.appspot.com/23006507
8. https://forums.developer.apple.com/thread/11354

