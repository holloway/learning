# Category (Adding New Stuff to Existing Class)
Sometimes we want to add a pinch of salts (some extra properties) or vinegar
(methods) into an existing methods, without fiddling the existing class's
source code. iOS provides Category.

<br><br>

Category allows you to add more methods. However, as a disciplined developer,
**I will avoid monkey patching additional instance variables and new property
through this feature**. There is always a safer way to transfer data between
objects.

<br><hr><br><br>

## Steps
### 1) Create a file specifically to the language.
#### Objective - c
```
Create Objective-C file
```

<br>
![Screenshot-1](https://lh3.googleusercontent.com/yWQUaxjhkSWZgXjfQDOH7Qu0d_fON4MJd8Vg5A_JCJiVshzVm-XSW3h7myonI0XtV21yi5mibFC9KLoRAyZIb-_O3R-lfSdb892sA7IeVsqSpUS-PQBz-J9H7xaVzgKNofIpbspr93sZe6533pC9BtG2bBa_-hsrChb1cU9puAKmRrTSet3Lnsr2O3JCihg-lir-VCOL9Jwb-DMIAovkiv4ae80a1TjM88Cwe_KZBoVJzpTX1sWzUjNDwHdtwoFmgTDIAqk4yihJoiRVDrrfF6RGuzJWwnY2-aKdjyzfHL1XGatND2gVoiBXsN-vh5myP9vPRUYhGB69Cs6ql3BTLAEAtSakI6xGrwfGXj3XWzRMiyJafskb5KlssKSD1qodox9K2ksP8zqF-poL52BIlj0ndCVYpavvjYn5be48bYLiP7jVQDOwCmM8U5TFlBvsCcmuZWbj00nZI6t8jRMatIO384TmZuwVzxoUwwVmsllWn2iKdyY3RiiHUu90g_HC2fpOGIjjlywAF5NH-xAa1o-JGGRMMGs3_XOWODkpf7I1vpxVm43WLx4r1Ze7Xdf2PrSzaHDOjKhEpQX1lyRrZjG9PPNjnE0i=w1466-h1034-no)

<br><br>

### 2) Ensure the file type is named as 'Category' and specify the class that you wants to add
Example:
#### Objective - c
```
Name: data
File Type: Category
Class: FrontViewController
```

<br>
![Screenshot-2](https://lh3.googleusercontent.com/jrxveLvRxmZfCcE6lpr9zr3NSZd6cOEms-uRL0-lvIABvNMzdm6Dkfk6W7xoafOnXnjhKsPbmg2e3OZS5iFj4RG60AFfFaWPq-5-H-5J9tVMcfQ_7bLhcWi4qanuCjhWFDeSXfm1drXeg-Y9pNsiScnRYxKCOAvvCj4OmFMh0COmepdKuXfr_bVtAPzlzP_g-rs5CHkzBHkDrtOQvX8EbQGAi7ivk-I3VOeg2zDzUrwVpsRsGGAcuJ6LpcaMKt9411lStGbrk94F4yv1kVp5y8j3KJohCVHssOQNOcXWqq2Sbw5zMbBeUpxc-vQSiLkVXdOQlOS9pQSLfTKlL7FlrPR1gdFLw-WkAlpDaMxlolui4O0OYTNZFtPCBtAH5-aiL-gw7KB87vHxhJnnNYGV9cYZiEE0qQ4Y2P5RQHR7hc7F5F4ZQgGY68JIecuLrtyV3jo-W6OQUFnEGu3-dWFatXPZVRbc5iMjVjZMDz_V3TtlMzv75y30wCZIYM07NsTAjszGG2C_52eHk_g7VUx2sPwHdNyN2tk8DVnJn2FvjKTEikUm0rRvS7p_Rai-YwwqOH8CvQl0v87z2ZEM5yCdSSZY1L06Da-G=w1454-h1034-no)

<br>
> NOTE:
>
> XCode will use the name as addition to existing the class. In the example
> above, the output file will be 'FrontViewController+data.h',
> 'FrontViewController+data.m'.

<br><br>

### 3) Continue to expand your class.
Now that your category files are ready, you can proceed to add in more stuff.


<br><hr><br><br>

## References
1. http://www.tutorialspoint.com/objective_c/objective_c_categories.htm
2. http://rypress.com/tutorials/objective-c/categories
3. https://developer.apple.com/library/ios/documentation/Cocoa/Conceptual/ProgrammingWithObjectiveC/CustomizingExistingClasses/CustomizingExistingClasses.html
