# Loading Indicator
Loading indicator on screen is the simpliest view used in almost everywhere.
It can be insides a ViewController or between segue views. This guide is planned
to show both types of implementation.


<br><br><hr>
## Basic Use
If you want to apply loading indicator on your current view controller, this
section answers your needs.


<br><br>
### 1) Install the MBProgressHUD
Follow the [MBProgressHUD](https://github.com/jdg/MBProgressHUD) on installing
the source codes.


<br><br>
### 2) Use the following template for your execution
[MBProgressHUD](https://github.com/jdg/MBProgressHUD) introduces a simple HUD
code template with no mask and is user interactive. Upon some attempts, I would
recommend the following template for some good enhancement below.

<br>
This template sets a background mask color, ecolored the indicator background
to white instead of the default grey,  and disable user interaction by
default.

#### objective-c
```
- (IBAction)doSomething:(id)sender
{
    // start the loading indicator
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    // set labels
    hud.label.text = @"Loading";
    hud.detailsLabel.text = @"setting up";

    // setting background mask color / display
    hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.backgroundView.color = [UIColor colorWithWhite:0.0f alpha:0.1f];

    // setting loading indicator background color
    hud.bezelView.color = [UIColor whiteColor];

    // disable app user interaction
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];

    // dispatch to free up main thread for processing UI
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        // Perform the operations here
        ...
        
        // Done Loading
        // Get the main thread back, dismiss the indicator and continue after
        // loading execution
        dispatch_async(dispatch_get_main_queue(), ^{

            // re-enable app user interaction again
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];

            // hide the loading indicator
            [hud hideAnimated:YES];

            // perform your after loading operations here
            // ...
        });
    });
    
}
```

> NOTE:
>
> The comments are mainly for description purposes, catering for first-timer
> education purposes. Feel free to remove it once you understand how to use
> MBProgressHUD.


<br><br><hr>
## Between segue views
If you want to apply loading indicator between 2 segues, you'll need to use
[no action segue](https://gitlab.com/hollowaykeanho/learning/tree/master/ios/segues/create_segue_with_no_action) 
instead for triggering the segue transition.

<br>
This section covers in detailed on how to perform the segue loading indicator.

<br><br>
### 1) Create a no-action segue
You can read the [no action segue](https://gitlab.com/hollowaykeanho/learning/tree/master/ios/segues/create_segue_with_no_action) details. Just for quick recall, on storyboard,
it is something like this:

<br>
![Create non-action segue](https://lh3.googleusercontent.com/LFm1xMf9XoOZbYO3kUhSwArhh0ilFRHQTxM6INSPcllEjJ5K2f-2GpLUo-Rvqi4_OmzLdzxPu8fllDDhdY4wQ_jxf2v0Z_M-Ll1I-z8EN9nuMSqvSdAMDZiwdPFCPKVmztdluip1emPDNpJ-6SY5ktBZd8vHKwGeBekhIVIbJUJkJMNzjEq59Ef3QDMaQGqi8gzqoPv3dGYu5_MbRX5nv_frya8SJnnv5BXnA57d3aGl_nnYi_OoKH3SrbhBqU6vcwMsCujC3dt9JurS8tai3jk0tbre3oG25pDzpapY0qOLiWPfHhiMT43emX1bxpETNRBAY7yD_Iv75Q-dwEILM3GJ99ZYVNmbLagVM7-3ARWWWg1yGUKnpQdncTxUl0lYDyQnZPSog3VH1-WGigZHHTooeywRg40xUsGMHCCE4tYl4ce0s4942a8o6ErCEByr9LaoK5yFCL017ECYpyC9bn65_5F5U927mcyK6jwwtBJowxFSlFdaFrnZTmXOmsQOX0Fq0jIMQgbTj8nouhc-FUMX5KrTF6ixrsQq3disY1otUTZAuAeq4djnIbTlqqk6XHt-Zw=w480-h312-no)


<br><br>
### 2) Give the segue an identifier
Give it via the attribute inspector.


<br><br>
### 3) Install the MBProgressHUD
Follow the [MBProgressHUD](https://github.com/jdg/MBProgressHUD) on installing
the source codes if you haven't done yet.


<br><br>
### 4) Attach the IBAction and deploy the loading bar with segue indicator
Basically the idea is that the IBAction performs the loading and execution
before transition. After the completion, we'll need the trigger the segue
transition programmatically.

<br>
The following template is similar with the above. The only thing extra is
the segue execution inside the main queue dispatch.

#### Objective-C
```
- (IBAction)didPressedLogin:(id)sender {
    // start the loading indicator
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    // set labels
    hud.label.text = @"Loading";
    hud.detailsLabel.text = @"setting up";

    // setting background mask color / display
    hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.backgroundView.color = [UIColor colorWithWhite:0.0f alpha:0.1f];

    // setting loading indicator background color
    hud.bezelView.color = [UIColor whiteColor];

    // disable app user interaction
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];

    // dispatch to free up main thread for processing UI
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        // Perform the operations here
        ...
        
        // Done Loading
        // Get the main thread back, dismiss the indicator and continue after
        // loading execution
        dispatch_async(dispatch_get_main_queue(), ^{

            // re-enable app user interaction again
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];

            // hide the loading indicator
            [hud hideAnimated:YES];

            // perform your after loading operations here
            // ...

            // execute segue programmatically
            [self performSegueWithIdentifier:@"yourSegueIdentifier" sender:self];
        });
    });
}
```


<br><br><hr>
## References
1. http://stackoverflow.com/questions/11503964/mbprogresshud-block-interface-until-operation-is-done
2. https://github.com/jdg/MBProgressHUD/blob/master/Demo/HudDemo/MBHudDemoViewController.m
3. https://github.com/jdg/MBProgressHUD
4. http://code.tutsplus.com/tutorials/ios-sdk-uiactivityindicatorview-and-mbprogresshud--mobile-10530