# rounded corners
This guide is for creating a rounded corner view. It's straight forward, just
set the layer using ```QuartzCore``` framework.

## Objective-c
```
#import <QuartzCore/QuartzCore.h>

view.layer.cornerRadius = 5;
view.layer.masksToBounds = YES;
```

> NOTE:
>
> This is applicable for other objects such as ```UIButton``` etc.


<br><br><hr>
## References
1. http://stackoverflow.com/questions/1509547/giving-uiview-rounded-corners