# UITableView separator line
Want to modify / remove the separator line? This guide is the right place.
It's pretty simple, just use:

### Objective-c
```
[self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
```

> NOTE:
>
> styles types:
>
> 1. UITableViewCellSeparatorStyleNone
> 2. UITableViewCellSeparatorStyleSingleLine
> 3. UITableViewCellSeparatorStyleSingleLineEtched


<br><br><hr>
## References
1. https://developer.apple.com/library/ios/documentation/UIKit/Reference/UITableViewCell_Class/index.html#//apple_ref/c/tdef/UITableViewCellSeparatorStyle
2. http://stackoverflow.com/questions/4804632/uitableview-separator-line