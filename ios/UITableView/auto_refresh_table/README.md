# Auto-refresh UITableView
Want the drag and refresh effect? UITableView has a built-in function for that.
This section shows you how to setup one and how to implement it.

<br>
<div align="center">
<img src="https://lh3.googleusercontent.com/tGBOFIcE6sjg2DOQJXETjxudn6sqvGClqMuTqKZQ0tvYb6blLoD_vVnLEnj2ynFWO-rHu_zCwDasceUZ2RrqJQTR1Pm7_q3gr15Kyx8tf4frI3HVHTt_e-oT_bWLD--iK8OReW_ZL_wLxfTSYh3T5INpSRiK2sGa74GbPY16JaHaVXSA-4jPbMNxOWNjGs7R_hcGVloFnfeluAw--BMjBMXKwoTxpnAs4-RJEefuoUuxpBXzglzRbp-laEWw5tcL_P0qeryiIkq5TnLo_W-hRCkFxJXVwh33CUdFAlC0TYvrygLNRitePM4Vs7toEoCPnkYpWmlv2OM8Mf-gSUwVE_qBmZRhOOHaGFQPy1FEEV-ZTcjsBJu7w_5ZVTvevufaoMF4iKJBqnnnvPcHnvoAi0S2rY1tVrd1tvAmrZc-VbDVhZOQ-FvhwsNPyE-nvWwFJgB9b5m97qtolyHn3HpHxb6mHHM7GnJrysB0qQ2FBrExfau626aJJYtcVf6kYcJri33qwF-N8SxmzoFtrlnWLFoukFepC6963GnQTSwwA7lyTUaPG3UWtZyBrmvwwfjeiLCskA5xUxnVJ1sxGHz0xahjUQxWcAiJ=w177-h326-no" />
</div>

<br>
However, please bear in mind that this guide works on prototype cell table,
(not the static cell) table.


<br><br><hr>
## Steps
### 1) setup the refresher control
You can setup the refresher control using UIRefreshControl. Within the
UITableViewController, you just need to initialize it and save it to the
controller's refreshControl.

<br>
Along side, you can design the display such as tint color, background color
and attributed texts.

<br>
Lastly, add an action to it. This will perform the refresh operations. Also,
for unknown reason, refreshControl tends to keep the main thread busy. Hence,
for safety reason, it's better to end the refreshing process right after
initialization.

### Objective-c
```
- (void)setupRefreshControl
{
    // setup refresh controller
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor purpleColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                forKey:NSForegroundColorAttributeName];
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"refreshing..."
                                                                          attributes:attrsDictionary];

    [self.refreshControl addTarget:self
                            action:@selector(performRefresh)
                  forControlEvents:UIControlEventValueChanged];

    if (self.refreshControl.isRefreshing)
        [self.refreshControl endRefreshing];
}

- (void)performRefresh
{
  // ... do some action
}
```


<br><br>
### 2) Stop the Refresh Control
There are 2 ways to  stop the refresher: ask humbly (like the above) or
remove it, depending on your needs. For example, say your datasource is
MVVM data addition refreshment, you probably want to end it humbly since
you'll want to facilitate the refresher activity to your user again. For
the case of search results, that you might want to remove the refresh
control to avoid UI conjestion. 

<br>
In the following examples, I'll be showing the graceful asking first, then
removal. Use either one or both depending on your UI.

#### Objective-c
```
- (void)stopRefreshGracefully
{
    if (self.refreshControl.isRefreshing)
        [self.refreshControl endRefreshing];
}


- (void)stopRefresh
{
  self.refreshControl = nil;
}
```


<br><br>
### 3) That's it
That's it. That's all about auto-refresh table.


<br><br><hr>
## References
1. https://www.appcoda.com/pull-to-refresh-uitableview-empty/