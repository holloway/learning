# Keychain - Storing Secure Data
In the case where you need to store private and secure password, data etc,
Apple Keychain feature come to the rescue.

<br>
However, that feature comes in a pure C language. Therefore, it is not
surprising that you'll code your own keychain wrapper, or tap into Apple
provided wrapper.

<br>
You shouldn't use NSDefault to store sensitive data mainly because the plist
file is visible when the iPhone is connected to any Mac machine.

<br>
By the way, keychain only works on signed iPhones/iPad devices.


<br><br>
## Assumptions
1. You know Xcode and Objective-C.


<br><br>
## Steps
### 1) Get your app service name and access group name
Keychain feature uses an 'app service name' to group all your sensitive data
into an app group. Thanks to Keychain, different apps under your
credentials group can cross-access easily using an 'access group name'.

<br>
Both names looks something like this:
```
NSString *service = @"com.yourcompany.appname"
NSString *group = @"SSSETH2A.com.yourcompany.appname"
```

> NOTE:
>
> For group name, it is known as ```<bundle seed>.<bundle identifier>```.
> You can obtain the full address programmatically through the following code
> and then split them using NSString processing:
>
> ```
> [[NSBundle mainBundle] bundleIdentifier]
> ```


<br><br>
### 2) Create a query dictionary method
For any create, edit, update and delete execution, they all need a dictionary
to perfrom respective action. As a good practice for Don't Repeat Yourself,
let's structure it into a single method.

<br>
In my following examples, they're using GenericPassword class field. I selected
that type as default since I want absolute encryption over my data.

#### Objective-c 
```
- (NSMutableDictionary *)prepareDictionary:(NSString *)key
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    
    NSData *encodedKey = [key dataUsingEncoding:NSUTF8StringEncoding];
    [dictionary setObject:encodedKey forKey:(__bridge id)kSecAttrGeneric];
    [dictionary setObject:encodedKey forKey:(__bridge id)kSecAttrAccount];
    [dictionary setObject:self.service forKey:(__bridge id)kSecAttrService];
    [dictionary setObject:(__bridge id)kSecAttrAccessibleAlwaysThisDeviceOnly forKey:(__bridge id)kSecAttrAccessible];
    
    if (self.group != nil)
        [dictionary setObject:self.group forKey:(__bridge id)kSecAttrAccessGroup];
    
    return dictionary;
}
```


<br><br>
### 3) Code the SAVE method.
The first execution is SAVE / WRITE method. For better usability, you can
provide simple key and value parameters. Here is a good example:
```
- (BOOL)add:(NSString *)key value:(NSData *)data
{
    NSMutableDictionary *keychainQuery = [self prepareDictionary:key];
    [keychainQuery setObject:data forKey:(__bridge id)kSecValueData];
    
    OSStatus status = SecItemAdd((__bridge CFDictionaryRef)keychainQuery, nil);
    if (status == errSecSuccess)
        return YES;
    
    return NO;
}
```

> NOTE:
>
> The key method is SecItemAdd. This C-method execute the query into keychain.
> Also, notice that we're using prepareDictionary method with the key for
> generating query dictionary. Then we add the value into it.


<br><br>
### 4) Code the GET method.
We can now code the GET / LOAD / READ method. For better usability, you can
provide simple key parameter. Here is a good example:
```
- (NSData *)get:(NSString *)key
{
    NSMutableDictionary *dictionary = [self prepareDictionary:key];
    
    [dictionary setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    [dictionary setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    
    CFTypeRef result = NULL;
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)dictionary, &result);
    
    if( status == errSecSuccess) {
        return (__bridge NSData *)result;
    }
    return nil;
}
```

> NOTE:
>
> The key method is SecItemCopyMatching. This C-method execute the query into
> keychain. Also, notice that we're using prepareDictionary method with the key
> for generating query dictionary. Then we set the query limit to 1 and return
> data as true.


<br><br>
### 5) Code the UPDATE method.
We can now code the UPDATE / EDIT method. For better usability, you can provide
parameters **similar to SAVE method**. This simplifies the process. Here is 
a good example:
```
- (BOOL)update:(NSString *)key value:(NSData *)data
{
    NSMutableDictionary *dictionary = [self prepareDictionary:key];
    NSMutableDictionary *newDataDictionary = [[NSMutableDictionary alloc] init];
    
    [newDataDictionary setObject:data forKey:(__bridge id)kSecValueData];
    OSStatus status = SecItemUpdate((__bridge CFDictionaryRef)dictionary, (__bridge CFDictionaryRef)newDataDictionary);
    
    if( status == errSecSuccess) {
        return YES;
    }
    return NO;
}
```

> NOTE:
>
> The key method is SecItemUpdate. This C-method execute the query into
> Keychain. Notice that we're creating a new data dictionary to host the new
> data. SecItemUpdate takes both original query and new data dictionary for
> update.


<br><br>
### 6) Code the DELETE method.
We can create DELETE method. For better and simpler method, you can
provide parameters **similar to GET method**. Here is a good example:
```
- (BOOL)remove:(NSString *)key
{
    NSMutableDictionary *keychainQuery = [self prepareDictionary:key];
    OSStatus status = SecItemDelete((__bridge CFDictionaryRef)keychainQuery);
    
    if( status == errSecSuccess)
        return YES;

    return NO;
}
```

> NOTE:
>
> The key method is SecItemDelete. Since there is not value involved, you can
> supply the query dictionary straight to the method.


<br><br>
### 7) Code the CLEAN ALL method.
Optionally, you can code the final method which is CLEAN ALL traces. Notice
that all keychain values stays within the phone even after user removes the
app.

```
- (BOOL)cleanAllTraces
{
    NSArray *secItemClasses = @[(__bridge id)kSecClassGenericPassword,
                           (__bridge id)kSecClassInternetPassword,
                           (__bridge id)kSecClassCertificate,
                           (__bridge id)kSecClassKey,
                           (__bridge id)kSecClassIdentity];
    for (id secItemClass in secItemClasses) {
        NSDictionary *spec = @{(__bridge id)kSecClass: secItemClass};
        SecItemDelete((__bridge CFDictionaryRef)spec);
    }
}
```


<br><br>
## References
1. https://developer.apple.com/library/ios/documentation/Security/Conceptual/keychainServConcepts/02concepts/concepts.html
2. http://www.b2cloud.com.au/tutorial/using-the-keychain-to-store-passwords-on-ios/
3. http://hayageek.com/ios-keychain-tutorial/
4. http://useyourloaf.com/blog/simple-iphone-keychain-access/
5. http://devmonologue.com/ios/ios/storing-sensitive-information-in-the-iphones-keychain/
6. https://www.raywenderlich.com/92667/securing-ios-data-keychain-touch-id-1password
7. https://github.com/kishikawakatsumi/UICKeyChainStore
8. http://blog.speedwell.com.au/the-basics-of-a-shared-keychain/
9. http://stackoverflow.com/questions/8883102/obtain-bundle-identifier-programmatically
10. http://useyourloaf.com/blog/keychain-group-access/
11. http://stackoverflow.com/questions/11614047/what-makes-a-keychain-item-unique-in-ios
12. http://stackoverflow.com/questions/14086085/how-to-delete-all-keychain-items-accessible-to-an-app