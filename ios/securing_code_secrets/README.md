# Securing code secrets
So, you have client_token, secret_tokens, etc. but
[keychain](https://gitlab.com/hollowaykeanho/learning/tree/master/ios/keychain_storage)
is not the right tool. So how? This guide shows how it's done.


<br><br><hr>
## Steps
### 1) create the .gitignore file
Yes, we'll need this file to ingore our secret file. Create a new file inside
your project **root** directory.

<div align="center">
  <img src="https://lh3.googleusercontent.com/D-A8BKiwf_dQaYpj-gQpUGKcRDwkDkI6oU7P-JKTIQRZ0iMFYgofScOMfBxrqETqpYio_8NT4P0cqfWr1c6lYTS-Ox7QDkndOBz6s9EyGbeVrKqH--iqnD8_HiXliO-mFG6y_GRjSMZ9_-49CAMT3agg9wNEO0aP9mthi6GFxIi2UJLaMEs_nVrhpZkwZ-Cv2ajoSa1K2bLQRA_-QGpBZVas9E_rp6-0UGRVOocNbbUt1itKo6tEdOO1FIW5VYN0qrxo34Wm9Ag0TR2vSnb3NuDzgS9KZYSKxEO77ycjiCPE257aQ7X_P4D-TB8I93XjIeHyJca71dHtSuG7mASSNeph3r-nR60JJmO-C7FzqYHljp9HyEDF69Sf7rFHSdlrBSbWel0XGZIZ0-2CqC6BHcJqUE3SRvJFzIxRaI_k9ONSNrSb4ZMZLxFzBPvRZADA1SkmOq52tiIh5bA2gUEbVkQ9uNgWVCqET1xmL4oNGuHNHa9hGSkkcw5ImmZ-Z7spCFqWTHBQPdA3dCYqJu9Zpioh_jUJE6nNeSWHA682ZgiFIzPO1w-tgSOiSee0njGA9SOvb65CLYDMPefeubkmVvyETAwEpTyH=w2408-h1034-no" />
</div>

To port ```.gitignore``` file into project, you can either create an empty file
and named it ```.gitignore``` shown above or add file into the project.

> NOTE:
>
> Make sure the ```.gitignore``` file is inside the root folder.


<br><br>
### 2) inside ```.gitignore``` file, add ```Secrets.h```
We'll consolidate all the secrets inside this header and use preprocessor's
definition to load them.
```
Secrets.h
```


<br><br>
### 3) add ```Secrets.h``` inside your project file.
It's quite straight forward: just add a new header inside your project
directory. Make sure it's the path that you specified inside ```.gitignore```
file.


<br><br>
### 4) use ```git status``` to cross check the validity
By now, you should not see ```Secrets.h``` in your ```git status```. The idea
is simple, we don't want to commit ```Secrets.h``` but the ```.gitignore```
file. Thus, this made the source code secret from your repository visibility.


<br><br>
### 5) put your secrets inside and use it when is needed
Now with the secret header is out, you can save your secret tokens inside it.
Then import it inside your source code that utilize this secret. Now, please
**do not** globalized this header. It's meant to be secret.


<br><br>
### 6) that's it.
Now you can safely store your ```Secrets.h``` else where or share it
separately with your collague with business needs-to-know.

