# Singleton Object
Singleton object is a single, by far most powerful implementation for data
path management. Singleton object ensures a single object is created throughout
the app.

<br>
The main advantage is that it simplfies the data passing across each view
controllers. Imagine you always have to push data via ```prepareForSegue```,
now you don't have to. All you need to do is save the data back to the
singleton object and call it out again in your new view controller loading
(```viewDidLoad```). Also, when app is terminated, it is **much** easier to
backup ```datacenter``` singleton object into CoreData compared to scattering 
all over the places.

<br>
A sharp example that I use is called ```Datacenter``` to hold all my app data.
In objective-C, I called it as ```Datacenter.h``` and ```Datacenter.m```. In
this note, we're going to build the ```Datacenter``` singleton object for
an app.


<br><br><hr>
## To create a Singleton Object
### 1) create a new NSObject class file
Just like create new NSObject class file, proceed to create your singleton
object. In my case, it's ```Datacenter```.

#### Objective-c
**Datacenter.h**:
```
#import <Foundation/Foundation.h>

@interface Datacenter : NSObject

@end

```

<br>
**Datacenter.m**:
```
#import "Datacenter.h"

@interface Datacenter()

@end


@implementation Datacenter

@end

```


<br><br>
### 2) Add in the singleton shared manager 
Now to make the object into singleton object, we'll cater the call known as 
```sharedManager```. There are many ways to cater the method but I personally
uses GCD dispatch. Hence, the example class ```Datacenter``` is expanded as
the following code:

#### Objective-c
**Datacenter.h**:
```
#import <Foundation/Foundation.h>

@interface Datacenter : NSObject

+ (id)sharedManager;

@end

```

<br>
**Datacenter.m**:
```
#import "Datacenter.h"

@interface Datacenter()

@end


@implementation Datacenter


+ (id)sharedManager {
    static MyManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


- (id)init {
  if (self = [super init]) {
    # ...
  }
  return self;
}


@end

```

> NOTE:
>
> We added the ```sharedManager``` method in this step.
>
> Notice that singleton object uses ```sharedManager``` to recall the
> memory location. In this example, we're dispatching once using GCD to handle
> for us.


<br><br>
### 3) Customize your object with other data
Now that your singleton object is ready, you can proceed with customizing the
data structure of your class object. Example, since I'm using it a datacenter
to hold of my temporary data across all view controllers and ```AppDelegate```,
I'll add my custom data.

#### Objective-c
**Datacenter.h**:
```
#import <Foundation/Foundation.h>
#import "User.h"
#import "Scan.h"
#import "Shop.h"

@interface Datacenter : NSObject

@property (strong, nonatomic) User *user;
@property (strong, nonatomic) Scan *scan;
@property (strong, nonatomic) Shop *shop;

+ (id)sharedManager;

@end

```

<br>
**Datacenter.m**:
```
#import "Datacenter.h"
#import "User.h"
#import "Scan.h"
#import "Shop.h"

...

@implementation Datacenter

...

- (id)init {
  if (self = [super init]) {
    self.user = [[User alloc] init];
    self.scan = [[Scan alloc] init];
    self.shop = [[Shop alloc] init];
  }
  return self;
}


@end

```

> NOTE:
>
> In this example, I've added my own ```User``` model, ```Shop``` model and
> ```Scan``` model.
> 
> When the datacenter is initialized for the first time, I will have each of
> the data object initialized together.


<br><br><hr>
## To use a Singleton Object
### Basic use
To use the singleton object, simply call the ```sharedManager``` method. This
will allocate the object during the first attempt and return the same object
pointer in the subsequent call. Example:

#### Objective-c 
```
Datacenter *datacenter = [Datacenter sharedManager];
```


<br><br>
### Initialize the singleton object the first time
With the class defined, let's use it across our app. I'll initialize the
```Datacenter``` inside ```AppDelegate``` itself,
via ```didFinishLaunchingWithOptions:```. This way, I can initialize all my
data models ready before loading the view. Think of asynchonous implementation.
That would make the loading blazingly fast.

#### Objective-c
**AppDelegate.m**:
```
@interface AppDelegate ()

@property (strong, nonatomic) Datacenter *datacenter;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.datacenter = [Datacenter sharedManager];
    
    ...
}
...

@end
```


<br><br><hr>
## Using singleton object across view controllers
Viola! Now we have the singleton object, we can proceed to load them and save
them during each view controller loading and segue.

We need to 're-save' the data mainly because property only create new object,
not pointing to the same data object inside your singleton object. So, a good
example on how to use it would be like:

### Objective-c
**someViewController.m**:
```
#import "SomeViewController.h"
#import "Datacenter.h"
#import "User.h"
#import "Scan.h"
#import "Shop.h"

@interface SomeViewController

@property (strong, nonatomic) User *user;
@property (strong, nonatomic) Scan *scan;

@end

@implementation SomeViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
    Datacenter *datacenter = [Datacenter sharedManager];
    self.user = datacenter.user;  // load it for view controller usage
    self.scan = datacenter.scan;  // load it for view controller usage

}

...


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    Datacenter *datacenter = [Datacenter sharedManager];
    datacenter.user = self.user; // save it to update the data inside singleton
    datacenter.scan = self.scan; // save it to update the data inside singleton

    ...
}

...
@end

```

> NOTE:
>
> You don't have to declare a property to hold the ```datacenter``` object in
> a view controller since it's mainly wasting the memory. Just use it to load
> the data you need or update them.
>
> Using this method, we don't have to go through the fat traditional data
> passing method. Also, when app is terminated, it is **much** easier to backup
> ```datacenter``` singleton object into CoreData compared to scattering
> all over the places.


<br><br><hr>
## That's all!
Hope you learn this new trick and build more awesome iOS app!


<br><br><hr>
## References
1. http://www.galloway.me.uk/tutorials/singleton-classes/