# Multithreading
Multithreading often used in networking situations where the GUI refresh rate
is not able to wait for long download time. Hence, we uses dispatch_async to
perform asynchonous task.

Keep in mind that GUI always occupy the **main** queue. Hence, it is always
a practice to assign background works first in the background then uses
**dispatch_get_main_queue()** to request for main queue operations, such as
updating the UI.

<br><hr><br><br> 

## Simple multi-threading
A simple straight forward dispatch_async code looks as follows:

### Objective-C
```
dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    // Background work

    dispatch_async(dispatch_get_main_queue(), ^{
        // Update UI

    });
});
```

<br><hr><br><br>

## Nested multi-threading (not recommended)
Multi-nested multi-threading looks as follow:

**Keep in mind that you aren't suppose to nest too many code level asynchonously.**

### Objective-C
```
dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    // Background work

    dispatch_async(dispatch_get_main_queue(), ^{
        // Update UI

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            // Background work

            dispatch_async(dispatch_get_main_queue(), ^{
                // Update UI
            });
        });
    });
});
```

<br><hr><br><br>

## References
1. http://stackoverflow.com/questions/17490286/does-dispatch-asyncdispatch-get-main-queue-wait-until-done
2. https://developer.apple.com/library/ios/documentation/General/Conceptual/ConcurrencyProgrammingGuide/OperationQueues/OperationQueues.html
