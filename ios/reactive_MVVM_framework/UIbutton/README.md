# Handling Actions for UIButton
ReactiveCocoa offers a different way to handle events such as button pressed.
ReactiveCocoa prepared the `RAC_signal` for different events. In this note,
we'll demonstrate how to create a signal when `UIButton` is pressed.

<br><br>
## Objective-C Example
```
RACSignal *signal = [self.signUpButton rac_signalForControlEvents:UIControlEventTouchUpInside];
```

<br><br>
Beyond here, you can continue to perform signal analysis the the button.