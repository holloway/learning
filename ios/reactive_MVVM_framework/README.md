# ReactiveCocoa and MVVM Framework
As the source code grows larger, MVC's controller became very fat and
callbacks are all over the places. MVVM framework is here to solve the MVC fat
controller problem. 


<br><br>
## Basic About MVVM
The idea is straight forward: remove the controller's burden and place most
of the view operative codes into a new "controller" called **ViewModel**.
Between `ViewModels` and `ViewControllers`, they exchange data and signaling.

<div align="center">
<img src="https://lh3.googleusercontent.com/3kEj_AfxN_93Deu4LD1-Lw2L4a2VTZ0Sj9TISTKm0p1sq8zfXuB0mAQJubbbs8I3UrY1rNilLq4FEpzB6VMHTUJSDG04TRtFpYpKm1zChJSJ7TeuHQiG9C8rDVeUlZ-dJPGQOqX33_ahR4xbKg2V93YXb7cvA4ROL7tkZzor6kHFNqVzqfLgUaw4QI9Gi7ZA76presv8XwbXX2_xfZ_jel2DJ6pRAGYClBMVcwkw8by7-HjYAHpSY640f_u8vJ3qlI_Zt1FTLdg5ajeoC5O44WFxM52v7wMiwSEZfHKH9vcOnwhEC8E6XxQ1KR435N_XJ0BnLyZ9HwqluEmLy-j77zL-bBkr-K2az1XCYIhzKItBAML__GXzT5ZPhm6Fi7C2wErT2BOlILaKM3JSEpDoS3xoqhgegbJ9ITYwXQ3RMQIq1t5PtBEN3z4SBNNidyv-flUZe2tvxJEPNZ33iMzu47nAYCsCpK-DnA6-gKl-vwpghGRdaKDI-RMezcqEWM1a8tNSkcyHI1yTbjkU9Bw241VydSjhikvvxFY9pzreC9TW9sJRlSCpzMhqhvodrw5fTzoFWSEFIwVXMmnXh5L4UJ8t0KGhhC9zAWYNDqDy1U-EiOVPKw=w900-h235-no" />
</div>

<br>
That being said, the ownership for `ViewController`, `ViewModel` and `Model`
are following a standard rules:
1. `ViewController` has a reference to `ViewModel`, not the other way round.
2. `ViewModel` has a reference to `Model`, not the other way round.
3. `ViewController` is isolated from `Model` via `ViewModel`.

<div align="center">
<img src="https://lh3.googleusercontent.com/nRRNn1vnb8uJgYkbVfTu_WOeK6Bu5YUAHH1sXTQEsmseNT1tfZYRfritE8sBIXEGxoLN3p6L3QBo4Bb4C2oZjK5oP6PSuaU1OW2Dr-QdiV1duJFbETgvRqNlA-ZbLnCJuyCzIMQjXGc5B99xLe12Xd5Ov1FOCCz1ZgMYHe3-5EE6yYJyGiNLjrND0I7I3khmYajE1NSTgbssYwHpm9xS6JPCEfXw5u2BWST5fWhcVhbYSS3L4vmulmSmmsvKDBhlMwxocG3x8n7MMe3-4FSP0FHj4DR9doNXZXewApuybja2TLJ5DpnFfOrl-Vq1CR2o8_w7t_2vnPuyrUslD3FzzwdEqrDLeUmuNXQ_LDRSO59F36ErSuOk55V8O4JvBdf98KLrP-uHCkl63VWk7QL6pCTh-z8bRWgKoANMTKU_TjD6mRkhjepX8sO-lLM4CxDrsztTaf1UQbUYmpElBa7At4XYgFM6jttmeROw4QHWmGX-a7A2sjay3C5n4bATIF8Uw0oCaYa8V8cjU40wJdgiDoJQBoyiMcDcIiC5uEbAt6TMF_k6cGWSd5U70a5gnE8bx9N6y-JeeeQadMqpG6Az-Dd48rHT6c8DmTBSjMgBQcKSrKrqNA=w600-h270-no" />
</div>

<br>
Instead of:
```
- (void)viewDidLoad
{
  ...
    if (person.salutation.length > 0) {
        _nameText = [NSString stringWithFormat:@"%@ %@ %@", self.person.salutation, self.person.firstName, self.person.lastName];
    } else {
        _nameText = [NSString stringWithFormat:@"%@ %@", self.person.firstName, self.person.lastName];
    }
}
```

<br>
Now, with MVVM, we can have the followings in our `ViewController`:
```
- (void)viewDidLoad
{
  ...
  self.nameLabel.text = self.viewModel.nameText;
  self.birthdateLabel.text = self.viewModel.birthdateText;
}

```


<br><br>
## Workspace Structure
Unlike conventional MVC, the workspace structure that I can come out with
looks as such:
```
Project_Workspace
  |
  +--- Project
  |     |
  |     +--- helpers
  |     |     |
  |     |     +--- ...  // any common code blocks / modules
  |     |
  |     +--- models
  |     |     |
  |     |     +--- ...  // data models
  |     |
  |     +--- viewModels
  |     |     |
  |     |     +--- ... // model-view processing
  |     |
  |     +--- views
  |     |     |
  |     |     +--- controllers
  |     |     |     |
  |     |     |     +--- ...  // view controller
  |     |     |
  |     |     +--- views
  |     |           |
  |     |           +--- ...  // storyboards, xib
  |     |
  |     +--- AppDelegate.h
  |     |
  |     +--- AppDelegate.m
  |     |
  |     +--- Assets.xcassets
  |     |
  |     +--- Info.plist
  |     |
  |     +--- Supporting Files
  |             |
  |             +--- main.m
  |
  |
  +--- ProjectTests
  |     |
  |     +--- ...
  |
  +--- ProjectUITests
  |     |
  |     +--- ...
  |
  +--- README.md
  |
  +--- .gitignore
  |
  +--- Secrets.h
  |
  +--- .gitlab-ci.yml
  |
  +--- Podfile
  |
  +--- Podfile.lock
```

> NOTE:
>
> The differences is the `ViewControllers` are now parked under `views` folder
> and the existing folder is now named as `ViewModels`.


<br><br>
## Signals
You can use the traditional `delegates` and `callbacks` to create the signal.
However, as the project grows, these methods becomes cumbersome and a nightmare.
Hence, [ReactiveCocoa](https://github.com/ReactiveCocoa/ReactiveCocoa) pod has
come to the rescue.

<br>
ReactiveCocoa pod uses reactive programming method and functional programming
to get things running. ReactiveCocoa provides APIs that generate and handles
signal subscriptions, making the code more simplified and free from `callbacks`
and `delegates` stacking nightmares.


<br><br>
### Pod Install
The pod name is `ReactiveCocoa`. You can add it into your `Podfile` and
`pod install` it:
```
pod 'ReactiveCocoa'
```


<br><br>
### Create a RACSignal
To create a signal is fairly simple: tap into the ReactiveCocoa signal adds-on.
Example, say we want to track the text field changes (normally we use
`delegates`), we proceed with `rac_textSignal`. Example:
```
// setting the signal observation
RACSignal *usernameSourceSignal = self.usernameTextField.rac_textSignal;
```

> NOTE:
>
> ReactiveCocoa helped us to simplify all the `callbacks` and `delegates`
> so that we don't have to code it all over the places.


<br><br>
### Subscribe to RACSignal - `subscribe` 
To take action when a signal is appearing, we use `subscribe`. For example,
we create a text change signal action for `self.usernameTextField`:
```
-(void)viewDidLoad
{
  // ...

  [self.usernameTextField.rac_textSignal
    subscribeNext:^(id x) {
        NSLog(@"%@", x);
  }];
}
```

<br>
The above creates a RACSignal is for observing the text field changes and
execute the `subscribeNext` code blocks once a signal is given
(user type 1 character). Notice that we did not use `delegates` or `callbacks`
for this case but only mentioning the signal during `viewDidLoad`.

<br>
The next thing is to expand from here, we can also supply error handling code
block and completed code block through their extension, shown below:
```
RACSignal *usernameSourceSignal = self.usernameTextField.rac_textSignal;

[usernameSourceSignal
    subscribeNext:^(id x) {
      NSLog(@"%@", x);
      // ...

    } error:^(NSError *error) {
      NSLog(@"Error: %@", error);
      // ...

    } completed:^{
      NSLog(@"Completed code here");
      // ...

    }
];
```

> NOTE:
>
> You can have multiple different subscribers for any signals. The idea is like
> digital eletronics circuit. When a signal appears, you pass it to a certain
> output connected to that line.


<br><br>
### Filtering a signal - `filter`
Now that we create a signal, we can also apply filtering into it that acts as
a gatekeeper. Example:
```
  [[[self.usernameTextField.rac_textSignal
    filter:^BOOL(NSString *text) {
      return text.length > 3;

    }]catch:^RACSignal *(NSError *error) {

    }]subscribeNext:^(id x) {
      NSLog(@"%@", x);
      // ...

    } error:^(NSError *error) {
      NSLog(@"Error: %@", error);
      // ...

    } completed:^{
      NSLog(@"Completed code here");
      // ...

    }
  ];
```

<br>
For this case, `filter` acts as a signal validation. You can validates your
fields within this code block. Notice that the filter is actually another
`RACSignal`, executes right after the signal is generated. Hence, in this case,
you can chain them up like a functional reactive programming style:
<div class="center">
  <img src="https://lh3.googleusercontent.com/UVlR3QsYX9MBN7bESaBcDmQDwPDKEwmk7UqPiT6KBeYCkfwvvymmSRmQIICquldVI_UKqGhxhRuKGaTlQoNUtti0E0W-pCiPQCRuFIVDTu8EVcx9P-eFZbBSXBMqfVZyTfZbuJDw-8FQmqLn4B3BB079Uldj_eZ3VNipKfgQT0J4U0h1qDqLaxKm4pKhTkfisY_CtOh2LIqY4NYuetCWdqdyTXIZ52OOGi5RRA5jp18wZNVmIYQ3vRSzYGILJ31ztg9aRX-43omJKSd0qepUF0ARp7apRy6HC1wAqhB_a_VG7dPaK73_CTuc78mWvS-0f_htO6efE0YACbJjWI7qJNHRREC0rwFiAYx0e9FFJhKKf_R58ZsmfRdOcK3V6j6hzYqz35v6pZEql64TuZcm0FoQ_iApNFnv_YjRCjOcZ_pTEv6ef-3s57tEPWD2mqa7JBY37hjBpDFxS0JsY4Y9jQJEGU_p3vUZP-JZR8KzEISyv2JkJO1a624LmBVxhdl1jxTQr26w5edYLXqoUiAe7GC0oHEghcVXvnq3smJU72NLZFJqyGP72t3yv_Fb8B6XApCEXlIzyADuDbV9CE7oGPxzmzBoGlMMA34BVabyZeosipXFfA=w1976-h480-no" />
</div>


<br>
If you find the code above is complicated, we can re-write it into multi-line:
```
  // setting the signal observation
  RACSignal *usernameSourceSignal = self.usernameTextField.rac_textSignal;

  // run the filter block
  RACSignal *filteredUsername = [usernameSourceSignal  
    filter:^BOOL(id value) {
      NSString *text = value;
      return text.length > 3;
  }];

  // run the subscribe execution
  [filteredUsername
    subscribeNext:^(id x) {
      NSLog(@"%@", x);
      // ...

    } error:^(NSError *error) {
      NSLog(@"Error: %@", error);
      // ...

    } completed:^{
      NSLog(@"Completed code here");
      // ...

    }
  ];
```

> NOTE:
>
> `filter` drops the signal if the condition fails. If you want to capture both
> PASSING and DROPPING signals, use `map` or `flattenMap` block instead.


<br><br>
### remap signal output - `map`
By looking at the example above, we need a function that converts signal params
into a different signal params. In this case, we use `map`. Let's enhance our
previous filter:
```
  [[[self.usernameTextField.rac_textSignal
    map:^id(NSString *text) {
      return @(text.length);
    }]

    filter:^BOOL(NSNumber *length) {
      return [length integerValue] > 3;
    }]

    subscribeNext:^(id x) {
      NSLog(@"%@", x);
      // ...

    } error:^(NSError *error) {
      NSLog(@"Error: %@", error);
      // ...

    } completed:^{
      NSLog(@"Completed code here");
      // ...

    }
  ];
```

<br>
From the code, we now witnessed that `map` changes the output from string to
number, which filter is taking it as an input. Again, if you find the code is
complicatd, we can re-write it as:
```
// setting the signal observation
RACSignal *usernameSourceSignal = self.usernameTextField.rac_textSignal;

// run the map block
RACSignal *mappedSourceSignal = [usernameSourceSignal
    map:^id(NSString *text) {
      return @(text.length);
}];

// run the filter block
RACSignal *filteredUsername = [mappedSourceSignal  
  filter:^BOOL(NSNumber *length) {
    return text.length > 3;
}];

// run the subscribe execution
[filteredUsername
  subscribeNext:^(id x) {
    NSLog(@"%@", x);
    // ...

  } error:^(NSError *error) {
    NSLog(@"Error: %@", error);
    // ...

  } completed:^{
    NSLog(@"Completed code here");
    // ...

  }
];
```


<br>
To illustrate the functional operation and the data flow:
<div class="center">
  <img src="https://lh3.googleusercontent.com/5vTHb0aPE6IGCb_R9PDY5v1cHYC8Jp2vScj3ZsWrvdGSEbJgQyCSnihuYMCMXujdL7M3kpFAT_CRmuH1Hb8m_tUWfU1TJr9rIKjEs9MP2kwFU0eIy1xVt1PpkjV9qqRSKOEZXh1uuy3xXm6tUttzuJdVUYxzMFDDxXLIkwsN8YOgNeHFemEFqRre-rD_udW5uHVg8w33CAeGzsEWNWkUoHVZYfH4MpCUuaE9P9iLb3xoEen-8PAwhWTlp3Ox3sSmhTPr7807y2ewj_AP6AWd0AnSCTeXjSUEV10-8Y_sAxLC72hWaXEg4osdmHJshd2bPwOWjEOlj-f0O3an0uFQW8m1NG0iWEHw90VqiAjNpy4LSAfpPuSso0DCfx5-e8QUYDV0T_ii3tHXJPMfR8QxgVp7Jqgp1crKfqAcjrcQBsQ9s_ZeDVdgWFLQzqSJIn1wlg2IQ9hadtzwAFCMVuClrhpr7Io3a_YHXrjmpkNX6w7FTCfNu1aZ1soiF69LIOoEbK_YFQzfuGfV0WpV_I4OaVcUL5IfgdpZ-2XJT6_x-kF4J_JERSPLkV2nwwHJbDtMcTUp9WsvhSsMiqygEK6Wb_mwe6uW5XsXMFLn9vq5yFXEH_W0cQ=w1440-h252-no" />
</div>

<br>
> NOTE:
>
> You can chain as many `map` signal for additional execution.


<br><br>
### combine signals using OR - `combinedLatest:reduce:`
We can also combine multiple signals into 1 new signal using `combineLatest`,
just the like `OR` gate in digital electronics. Here is an example:
```
RACSignal *validUsernameSignal = self.usernameTextField.rac_textSignal;
RACSignal *validPasswordSignal = self.passwordTextField.rac_textSignal;

RACSignal *combinedSignal = [RACSignal combineLatest:@[validUsernameSignal, validPasswordSignal]
                                              reduce:^id(NSNumber *usernameValid, NSNumber *passwordValid) {
                                                            return @([usernameValid boolValue] && [passwordValid boolValue]);
                            }];
```

<br>
This creates a 3rd signal from the existing 2 signals. Then, using the
we generates the 3rd signal output using the `reduce:` code block.
<div class="center">
  <img src="https://lh3.googleusercontent.com/mKC-Vgf2d3KiJ8ZLnHxUC3XYU_OS_PTEegSAcMOymsAV0JROxrrYC0VXu51UqNQe5hd28T3GgkeY2hDvdIK8LbqAx36pDh3OV01lNTC_zdbhyucV1qQpLwZMVaHhOG4LMpBeM8EUzYG-wuMBfA9zBsliJgS5WgdUag4YW64q52mtveg9xYXVqmUHhr5o3FvBfLD3v0USJOYSCeRx5tUWHqejSXx9vhPoedq3lC8l0PYTlk3P6i-Gkyc9VGJ9CuZuxUq1VOxrhUoCd9wzbyC6w7UUmC-R-_QMgHBzxKrfl80WaqVyvv8nS51G2tlhq1qZWLUM0ncFn4nj6AT2biI7cP6ukEQwIouxBs0TtYh0Vvkxn_f419cA0MXiXl8RcAyzVGFJSc4SZ978iu-XPRrrMrEeiMfe9LplNU6g2yybSfdt5EzmL96YEBRiWTWFFc_O5N8NhQ7zwTI-uWXoSFGfHZ9YwadJjNf1nvEkoSxeN0M6V1NMeRVX_csoplDZhce4woKE8Ua8Off2yDqyECh6r6veqAQftPBEW1oWOz3l8d8dBBmehxlBmujsTHpHzKHZJY5MW-vMH7lI6cuEtDatE_9DTD0FZGpVraLcQmVpKvupaCoTSQ=w744-h282-no" />
</div>

> NOTE:
>
> OR gate works in a way that if either signal emits, the 3rd signal is
> produced. Their signal pattern looks as such:
>
> ```
> Signal A | -(a0)-----------------------(a1)-----x
>          |
> Signal B | ----(b0)----(b1)----(b2)------------(b3)---->
>          |
> cl/r:A,B | ----(a0,b0)-(a0,b1)-(a0,b2)-(a1,b2)-(a1,b3)->
>          |
>          +----------------------------------------------
>               -- Time -->
> ```


<br><br>
### combine signals using exclusive AND - `zip:reduce:`
We can also combine multiple signals into 1 new signal using `zip`,
just the like exclusive AND `XAND` gate in digital electronics. Here is an
example:
```
RACSignal *validUsernameSignal = self.usernameTextField.rac_textSignal;
RACSignal *validPasswordSignal = self.passwordTextField.rac_textSignal;

RACSignal *combinedSignal = [RACSignal zip:@[validUsernameSignal, validPasswordSignal]
                                    reduce:^id(NSNumber *usernameValid, NSNumber *passwordValid) {
                                                    return @([usernameValid boolValue] && [passwordValid boolValue]);
                            }];
```

<br>
This creates a 3rd signal from the existing 2 signals. Then, using the
we generates the 3rd signal output using the `reduce:` code block.
<div class="center">
  <img src="https://lh3.googleusercontent.com/UoJdD0wI9n4jiIRraAu1Hbc8q4Co3QSPc-VlmyKUKMIODnfp_R8upLlU6SjkOtKWFEhrmrpfe-Jx577BOkZyDi6BgToInYY61KTlHxYCZttZRjnUuG4oeoQA7TBjBvmLht_CPmozhZ0gyt4bWLqQ9nbYg0yMq7z3Wgqm6G6Cye42l4qqpMWi0StRS8PLGTb-FOmwggt_pAcy6aMGGE8mo_iDizXy5Drfln2v9AiG6O-eMs9AYuc-kL2OtjyfQzOL6bVO_0HbBb1jpN_hfYwuz0OzrVfVTEoeFyoXpG3D5Zbjr9ObdsRRucGCevbFiMHJIsYz9gHxiQ9AMAtKx-fiialDCJAKmeEg51yCEGMJNBFEgz7CZsfrZeVaJA7DHz9OafXC_y5kCe2TZJLn8eufHj4TkKhOHJQ_yspUP8wngJqIFsZ5Fa-wuCyxyff-cirtC4NyoBp0mEMc5ori-amaNW5GxJMr6d-mXnHl31ksJ4INj1RMv9kMChkXX6G3VngtP9E2KoFdOJAAuuFb1TZOn05slpPOzX78QGueS94hcRKWWkLrsB1KHQLFH_yTJ3lMv9-891wib5uI8eYfmszMXQQyYZCLrIB2mnHuk2b4z1QbC5tdog=w1440-h546-no" />
</div>

> NOTE:
>
> Exclusive AND gate works in a way that if both signals emits, the 3rd signal
> is then produced. The signal pattern looks as such:
>
> ```
> Signal A | -(a0)-------------(a1)-----x
>          |
> Signal B | ----(b0)-(b1)-(b2)----(b3)--->
>          |
>  zip:A,B | ----(a0,b0)-------(a1,b1)--x
>          |
>          +-------------------------------
>              -- Time -->
>```


<br><br>
### splitting from signals - `multi subscribers`
Apart from combining signal, we can split the signal to multiple subscribers or
combinator. There is no specific methods like `combineLatest` to do splitting.
All you need to do is having different subscribers subscribe to the correct
signal.

Example:
```
RACSignal *validUsernameSignal = self.usernameTextField.rac_textSignal;
RACSignal *validPasswordSignal = self.passwordTextField.rac_textSignal;
RACSignal *combinedSignal = [RACSignal combineLatest:@[validUsernameSignal, validPasswordSignal]
                                              reduce:^id(NSNumber *usernameValid, NSNumber *passwordValid) {
                                                            return @([usernameValid boolValue] && [passwordValid boolValue]);
                            }];

// subscriber to validPasswordSignal
RAC(self.passwordTextField, backgroundColor) =
  [validPasswordSignal
    map:^id(NSNumber *passwordValid) {
      return [passwordValid boolValue] ? [UIColor clearColor] : [UIColor yellowColor];
    }];
 
// subscriber to validUsernameSignal
RAC(self.usernameTextField, backgroundColor) =
  [validUsernameSignal
    map:^id(NSNumber *passwordValid) {
     return [passwordValid boolValue] ? [UIColor clearColor] : [UIColor yellowColor];
    }];

// subscriber to combinedSignal
...
```

Notice that both `validUsernameSignal` and `validPasswordSignal` are splitted
into 2 different subscribers, each:
1. its individual subscriber
2. combinator to provide the 3rd signal.

<br>
Both examples for `combineLatest:reduce:` and `zip:reduce:` are good examples
on splitting signals.


<br><br>
## Creating custom signal
In the case where the API is not reactive, you'll need to convert them into a
signal. 
```
-(RACSignal *)signInSignal
{
  return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
    // ... run your custom API
    return nil;
  }];
}
```

<br>
For cases like AFNetworking, you will use `subscriber sendNext:`,
`subscriber sendCompleted`, `subscriber sendError:` to handle new signal
generations. A code example:
```
- (RACSignal *)loginWithEmail:(NSString *)email
                     password:(NSString *)password
{
    RACSignal *signal = [RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
        [User authenticate:email
                  password:password
         completionHandler:^(NSURLSessionTask *task, NSDictionary *responseObject) {
             ...
             [subscriber sendNext:nil];
             [subscriber sendCompleted];
         } withErrorHandler:^(NSURLSessionTask *operation, NSError *error) {
             ...
             [subscriber sendError:error];
         }];
        return nil;
    }];

    return signal;
}


```

> NOTE:
>
> The variables / value you send is the next block's input value. For this,
> you might want to process the output before sending it to the next subscriber.


<br><br>
## subscribe inner signal from outer signal (Signal of Signals) - `flattenMap`
In the cases where a functional block yields a `RACSignal` or implementing
custom signal, you can use `flattenMap` instead of the traditional `map`.
Consider the example:
<div class="center">
  <img src="https://lh3.googleusercontent.com/H78-Z9B9O6kBh3vATjWj8Ze84-IkZGhkoXQAxAv9tpzkeBk4YoMVtfyYwUiQRCZii6BFOsoSSFPLrbryee0ipGkvV4TBZRVPI7YaieBUQX8x2H7TjfnO_qbDG53m3GCnkUzyS3NG10ETS9AVhsPqLu1_7V5XRKhD6Zm1hTLsiz50-e0mwIVJ-5mIDYkuP6qfaOZZ4hEvpSVQZwV80M_sIyXL2u-ZXv8Nk4gvvuIJl9qT84ZUwnLtLDFwH-U6HrnmrYfHVePpDg9WclELTYOvlGxuDM6aVjeBE4htZILXu-Pj3AKXR3ej05kjdFmQOvkJ_gxwDioPJQkwsvKOHhi0C6_YGvwYdVbgI31qJaeWVdi5ZoML9tUHi5civbRCNJZ6KgDyLKMgUEU-oXtmpmBYHqQH_5k5zxyMPcsKRZTJt4WWUm_QOETn52wfqtt5EELpETB8xO3PxxSGPqDI3AP0fz4Ni8j7g_1ueY0OjTjo8jCqhC9jiySbxaeFKd_4vTB61hclcYNRkKSkIadhZgiDWMu-PcWQ0X4UfuqRtKlx5XN5N8-moaDcQ0revKPhP2Zrpy-7A0ggyJneDQCGevzvzJJkvNgg1jvVQNvwP-tDYVulHVgKRw=w2100-h294-no" />
</div>
```
[[[self.signInButton
   rac_signalForControlEvents:UIControlEventTouchUpInside]
   map:^id(id x) {
     return [self signInSignal];
   }]
   subscribeNext:^(id x) {
     NSLog(@"Sign in result: %@", x);
   }];
```

<br>
We actually wanted to subscribe the signal yield by the return value from
`map`, not the `map` block and we have no way to declare from external. Hence,
This is where we uses `flattenMap`. It will return the subscriber with the last
signal output.

```
[[[self.signInButton
   rac_signalForControlEvents:UIControlEventTouchUpInside]
   flattenMap:^id(id x) {
     return [self signInSignal];
   }]
   subscribeNext:^(id x) {
     NSLog(@"Sign in result: %@", x);
   }];
```

> NOTE:
>
> This is suitable for custom signals where you'll need to wait for asynchonous
> activity synchonously like processing networking activity.
>
> <br>
> Like `map`, you can chain as many `flattenMap` to do many operations.


<br><br>
## THEN signal
Sometimes, we need a positive signal and THEN subscribe to that signal. Example,
we first wanted the user to login successfully before subscribing the search
text field. To do so, we use `then` block.

```
[[[self loginSignal]
  then:^RACSignal *{
    @strongify(self)
    return self.searchText.rac_textSignal;
  }]
  subscribeNext:^(id x) {
    NSLog(@"%@", x);
  } error:^(NSError *error) {
    NSLog(@"An error occurred: %@", error);
  }];
```

<br>
This produces the following signal block:
```
loginSignal --> then --> rac_textSignal --> subscribeNEXT
```


<br><br>
## DELIVER_ON signal - switching threads
All operations are background threaded. In the case for updating the main UI,
we uses `deliverON:`.

```
[[[[[[self requestAccessToTwitterSignal]
  then:^RACSignal *{
    @strongify(self)
    return self.searchText.rac_textSignal;
  }]
  filter:^BOOL(NSString *text) {
    @strongify(self)
    return [self isValidSearchText:text];
  }]
  flattenMap:^RACStream *(NSString *text) {
    @strongify(self)
    return [self signalForSearchWithText:text];
  }]
  deliverOn:[RACScheduler mainThreadScheduler]]
  subscribeNext:^(id x) {
    NSLog(@"%@", x);
  } error:^(NSError *error) {
    NSLog(@"An error occurred: %@", error);
  }];
```


<br><br>
## Throttle signal - wait until the signal is stablized
Sometimes, we want to wait completion or observe over a period of time before
executing the next subscribe block. To handle this, we use `throttle` for
a particular signal block.

```
[[[[[[[self requestAccessToTwitterSignal]
  then:^RACSignal *{
    @strongify(self)
    return self.searchText.rac_textSignal;
  }]
  filter:^BOOL(NSString *text) {
    @strongify(self)
    return [self isValidSearchText:text];
  }]
  throttle:0.5] // <--- this is waiting for no changes as long as 500 miliseconds
  flattenMap:^RACStream *(NSString *text) {
    @strongify(self)
    return [self signalForSearchWithText:text];
  }]
  deliverOn:[RACScheduler mainThreadScheduler]]
  subscribeNext:^(NSDictionary *jsonSearchResult) {
    NSArray *statuses = jsonSearchResult[@"statuses"];
    NSArray *tweets = [statuses linq_select:^id(id tweet) {
      return [RWTweet tweetWithStatus:tweet];
    }];
    [self.resultsViewController displayTweets:tweets];
  } error:^(NSError *error) {
    NSLog(@"An error occurred: %@", error);
  }];
```


<br><br>
## `catch` error handling signal and `retry` signal
This signal capture the error produced during a signal processing. It provides
handling for it. The signal is dropped when there is an error occured. To
resubscribe, we use the optional `retry` to reinitialize the signals. A code
example is as follows:

```
@property (weak, nonatomic) RACSignal *login;


// create RAC Signal
self.login = [self.loginButton rac_signalForControlEvents:UIControlEventTouchUpInside];


@weakify(self);
[[[self.login catch:^RACSignal *(NSError *error) {
        @strongify(self);
        [self.hud hideProgressIndicator];
        [self clearPasswordField];
        [self presentNoticeAlertViewController];
        return [RACSignal error:error];

    }] retry] subscribeNext:^(id x) {
        @strongify(self);
        [self.hud hideProgressIndicator];
        [self clearPasswordField];
        [self performSegueWithIdentifier:userLoginToScanViewSegue sender:self];
    }];
```




<br><br>
### More Signals?
There are more signals available in the pods. You'll need to read through
the sources code. A short list is available 
[here](http://cocoadocs.org/docsets/ReactiveCocoa/2.3.1/Classes/RACSignal.html).


<br><br>
## `RAC` Macro
`RAC` Macro is for us to simplify a generated output for `map` functional block.
For cases say, if you use `map` to get a value for an object instance, you
probably will do the following:
```
[[validPasswordSignal
  map:^id(NSNumber *passwordValid) {
    return [passwordValid boolValue] ? [UIColor clearColor] : [UIColor yellowColor];
  }]
  subscribeNext:^(UIColor *color) {
    self.passwordTextField.backgroundColor = color;
  }];
```


<br>
Hence, instead of doing the assignment in a backward manner, we use `RAC` Macro,
making the code simplified as:
```
RAC(self.passwordTextField, backgroundColor) =
  [validPasswordSignal
    map:^id(NSNumber *passwordValid) {
      return [passwordValid boolValue] ? [UIColor clearColor] : [UIColor yellowColor];
    }];
```


<br><br>
## Side-Effects Implementation
Side-effects implementation simply runs the execution without returning any
value. A good implementation is like the execute the loading indicator before
executing next signal. Consider the example:

```
[[[[self.signInButton
   rac_signalForControlEvents:UIControlEventTouchUpInside]
   doNext:^(id x) {
     self.signInButton.enabled = NO;
     self.signInFailureText.hidden = YES;
   }]
   flattenMap:^id(id x) {
     return [self signInSignal];
   }]
   subscribeNext:^(NSNumber *signedIn) {
     self.signInButton.enabled = YES;
     BOOL success = [signedIn boolValue];
     self.signInFailureText.hidden = success;
     if (success) {
       [self performSegueWithIdentifier:@"signInSuccess" sender:self];
     }
   }];
```

<br>
The `doNext` block is a side-effect implementation. It disables the sign-in
button when the signal is found. Then, it allows the next block to re-enable
the button again. 

<br>
This kind of implementation is best working with loading indicator, temporary
disabling etc.


<br><br>
## Avoiding Retain Cycles
There is one important note on instance variable: use `weak` variables for
subscribing blocks instead of the `strong`. This is mainly due to the
`ReactiveCocoa` working nature and code blocks working nature.

<br>
Common issue is using `self` in the subscribe block. To work with it, we
use `@weakify(strong_variable)` from `RACEXTScope.h` to generate the weak
version while `@strongify(weakified_variable)` to make it strong again for
usage.

<br>
Example, we `@weakify(self)` self first for subscribe code block. Then, inside
the subscribe block, we `@strongify(self)` again for operations and output.

```
#import "RACEXTScope.h"

...
- (void)init
{
  @weakify(self);
}

...

- (void)someAction
{
  ...
  [[[[self.signInButton
    rac_signalForControlEvents:UIControlEventTouchUpInside]
     doNext:^(id x) {
       ...
    }]
    flattenMap:^id(id x) {
       ...
    }]
    subscribeNext:^(NSNumber *signedIn) {
      @strongify(self);
      self.signInButton.enabled = YES;
      ...
    }];

}

```

> NOTE:
>
> If you happen to use RAC macro instead of subscribeNEXT method, you won't
> bump into this retain cycle complexity.


<br><br>
## Full Examples for chaining
```
@property (weak, nonatomic) RACSignal *login;

// create RAC Signal
self.login = [self.loginButton rac_signalForControlEvents:UIControlEventTouchUpInside];
[self loginBlock];


- (void)loginBlock
{
    @weakify(self);

    [[[[[[[self.login filter:^BOOL(UIButton *sender) {
        @strongify(self);
        self.firstPressedLogin = YES;
        if ([self.viewModel validForLoginForEmail:self.emailInputField.text
                                         password:self.passwordInputField.text]) {
            return YES;
        } else {
            [self highlightEmailField:![self.viewModel validateEmailFormat:self.emailInputField.text]];
            [self highlightPasswordField:![self.viewModel validPasswordFormat:self.passwordInputField.text]];
            [self clearPasswordField];
            [self presentNoticeAlertViewController];
            return NO;
        }
    }]doNext:^(id x) {
        @strongify(self);
        [self.view endEditing:YES];
        [self.hud initOntoViewController:self.view
                                 message:self.viewModel.authenticationLoadingMessage];

    }]flattenMap:^RACStream *(id value) {
        @strongify(self);
        return [self.viewModel loginWithEmail:self.emailInputField.text
                                     password:self.passwordInputField.text];

    }]deliverOn:[RACScheduler mainThreadScheduler]]
    catch:^RACSignal *(NSError *error) {
        @strongify(self);
        [self.hud hideProgressIndicator];
        [self clearPasswordField];
        [self presentNoticeAlertViewController];
        return [RACSignal error:error];

    }] retry] subscribeNext:^(id x) {
        @strongify(self);
        [self.hud hideProgressIndicator];
        [self clearPasswordField];
        [self performSegueWithIdentifier:userLoginToScanViewSegue sender:self];
    }];
}
```


<br><br>
## References
1. https://www.raywenderlich.com/62699/reactivecocoa-tutorial-pt1
2. https://yalantis.com/blog/how-i-learned-to-write-custom-signals-in-reactive-cocoa/
3. https://www.binpress.com/tutorial/reactivecocoa/20
4. http://www.teehanlax.com/blog/krush-ios-architecture/
5. https://leanpub.com/iosfrp
6. http://cocoasamurai.blogspot.my/2013/03/basic-mvvm-with-reactivecocoa.html
7. http://www.teehanlax.com/blog/getting-started-with-reactivecocoa/
8. http://www.teehanlax.com/blog/model-view-viewmodel-for-ios/
9. https://www.objc.io/issues/13-architecture/mvvm/
10. https://github.com/futurice/ios-good-practices
11. http://cocoadocs.org/docsets/ReactiveCocoa/2.3.1/Classes/RACSignal.html
12. http://stackoverflow.com/questions/20144752/combine-signals-in-reactivecocoa-to-a-new-one-that-fires-when-all-change
13. https://www.raywenderlich.com/62796/reactivecocoa-tutorial-pt2
14. https://spin.atomicobject.com/2014/04/03/combinelatest-and-zip-in-reactivecocoa/
15. http://stackoverflow.com/questions/22559868/reactivecocoa-combinelatest-and-zip/22562642
16. http://stackoverflow.com/questions/19439636/difference-between-catch-and-subscribeerror
17. http://stackoverflow.com/questions/33091295/how-to-resubscribe-to-racsignal-after-error
18. http://codeblog.shape.dk/blog/2013/11/16/wrapping-afnetworking-with-reactivecocoa/
19. http://www.techsfo.com/blog/2013/08/managing-nested-asynchronous-callbacks-in-objective-c-using-reactive-cocoa/