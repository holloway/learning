# Replacing Delegates
Now that we are using ReactiveCocoa to perform signal exuection, it's time to
replace the delegates implementation. Depending on the types of delegates,
we generally uses the provided API:

## Objective-C
```
RACSignal *signal = [self rac_signalForSelector:@selector(tableView:didSelectRowAtIndexPath:)
                                    fromProtocol:@protocol(UITableViewDelegate)];


// if you're using tableView then you should refresh the delegates
self.tableView.delegate = nil;
self.tableView.delegate = self;


// example of using the signal
[signal map:^id(RACTuple *args) {
    UITableView *tableView = args[0];
    NSIndexPath *indexPath = args[1];

    // ... do your operations

    return [[NSNumber alloc] initWithInteger:indexPath.row]; // return a desired value
}];
```

> NOTE:
>
> You use the `@selector` to select the delegate function and use `@protocol`
> to indicate the delegate source. 
>
> <br>
> The reason to `map` signal exists here is to demonstrate how to utilize
> the delegate signal and remap the return value to a desired version.
>
> <br>
> If you're using TableView, you should always do a delegate refresh by
> re-assigning self again. This is to inform TableView that you're using
> ReactiveCocoa's delegate implementation instead of the conventional version.


<br><br>
## Fast-Pace Delegates
Due to unknown reason, ReactiveCocoa's implementation somewhat slows down
the user experience compared to the conventional delegate implementation. A
good example would be showing the loading indicator for processing an image
from the Camera UI. 

<br>
Appearently, the `doNext` does not show the loading indicator immediately.
Hence, in such senario, you might need to revert back to conventional delegate
implementation and use `RACCommand` instead. Example:

<br>
### Objective-C
```
@interface ...

...
@property (strong, nonatomic) RACCommand *processImageCommand;
@property (strong, nonatomic) RACSignal *processImageSignal;

@end


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.viewModel = [[ScanViewModel alloc] init];
    self.loadingIndicator = [[LoadingIndicator alloc] init];

    // ... other initialization


    self.processImageCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        return [RACSignal return:input];
    }];
    self.processImageSignal = [self.processImageCommand.executionSignals flatten];

    @weakify(self);
    [[[[self.processImageSignal flattenMap:^RACStream *(NSArray *args) {
        @strongify(self);
        NSDictionary *info = args[1];
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        return [self executeProcessingImage:image];

    }] catch:^RACSignal *(NSError *error) {
        @strongify(self);
        [self.loadingIndicator hideProgressIndicator];
        [self.previewTableView reloadData];
        [self dismissViewControllerAnimated:YES completion:nil];
        return [RACSignal error:error];

    }] retry] subscribeNext:^(NSArray *args) {
        @strongify(self);
        [self.loadingIndicator hideProgressIndicator];
        [self.previewTableView reloadData];
        [self dismissViewControllerAnimated:YES completion:nil];

    }];
}



/*
 * ReactiveCocoa doNext: implementation is too slow after user selected
 * 'use photo'. Hence, it's better to fall back to original delegate method to
 * kick-start the loading indicator before the processing begins.
 */
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIView *targetView;
    if (picker.navigationController.view != nil) {
        targetView = picker.navigationController.view;
    } else {
        targetView = picker.view;
    }
    [self.loadingIndicator initOntoViewController:targetView
                                          message:self.viewModel.processingLoadingIndicatorText];

    // continue executing the image processing processing
    [self.processImageCommand execute:[[NSArray alloc] initWithObjects:picker, info, nil]];
}

```


## References
1. https://gist.github.com/bricklife/439e69c97f251b4364db
2. https://github.com/ReactiveCocoa/ReactiveCocoa/issues/1121
3. https://github.com/nikolaykasyanov/SoundCloudStream/blob/master/SoundCloudStream/Views/CRTActivitiesViewController.m#L158
4. https://spin.atomicobject.com/2014/02/03/objective-c-delegate-pattern/
5. http://rcdp.io/Signal.html