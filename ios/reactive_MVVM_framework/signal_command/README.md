# The Very Basic Signal Generation - RACCommand
ReactiveCocoa packed a large number of arsenals for signal generation but what
if you want a custom version? `RACCommand` is the right tool. We use
`RACCommand` to generate its own signal.

<br><br>
## The Objective-C way
```
  // to create a command
  RACCommand *command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
      return [RACSignal return:input];
  }];

  // to subscribe its signal
  RACSignal *signal = [command.executionSignals flatten];


  // to trigger a signal
  [command execute:@"Takes an input"];
```

> NOTE:
>
> 1. Keep the command as simple as possible by passing only the input.
> 2. Always remember to flatten the executionSignals; otherwise, you'll lose the input.
> 3. The commands takes an input and pass through the entire signal pipelines.