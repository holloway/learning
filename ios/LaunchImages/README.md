# Image Sizes
In case you're designing full page launch images for your app, these are the
screen resolution prior SDK and iOS 9.3. 

<br><br><hr>
## iPhone Portrait iOS 8,9
Retina HD 5.5 (iPhone 6 Plus) - 1242px x 2208px - Default-736h@3x.png

Retina HD 4.7 (iPhone 6) - 750px x 1334px - Default-667h@2x.png

<br><br>
## iPhone Landscape iOS 8,9
Retina HD 5.5 (iPhone 6 Plus) - 2208px x 1242px - Default-Landscape-736h@3x.png

<br><br>
## iPhone Portrait iOS 7-9
2x (iPhone 4) - 640px x 960px - Default@2x.png

Retina 4 (iPhone 5) - 640px x 1136px - Default-568h@2x.png

<br><br>
## iPhone Landscape iOS 7-9
1x - 1024px x 768px

2x - 2048px x 1536px

<br><br>
## iPad Portrait iOS 7-9
1x - 768px x 1024px

2x - 1536px x 2048px

<br><br>
## iPad Landscape iOS 7-9
1x - 1024px x 768px

2x - 2048px x 1536px

<br><br>
## iPhone Portrait iOS 5,6
1x (iPhone 3) - 320px x 480px - Default.png

2x (iPhone 4) - 640px x 960px - Default@2x.png

Retina 4 (iPhone 5) - 640px x 1136px - Default-568h@2x.png

<br><br>
## iPad Portrait iOS 5,6
1x - 768px x 1024px

2x - 1536px x 2048px

<br><br>
## iPad Landscape iOS 5,6
1x - 1024px x 768px

2x - 2048px x 1536px

<br><br>
## iPad Portrait Without Status Bar iOS 5,6
1x - 768px x 1004px

2x - 1536px x 2008px

<br><br>
## iPad Landscape Without Status Bar iOS 5,6
1x - 1024px x 748px

2x - 2048px x 1496px

<br><br><hr>
## References
1. http://stackoverflow.com/questions/34112681/ios-launch-images-driving-me-crazy
2. http://stackoverflow.com/questions/30676843/how-where-do-xcode-6-launch-screen-sizes-correspond-to-apple-docs
3. https://developer.apple.com/library/ios/documentation/UserExperience/Conceptual/MobileHIG/IconMatrix.html#//apple_ref/doc/uid/TP40006556-CH27
4. https://developer.apple.com/library/ios/documentation/UserExperience/Conceptual/MobileHIG/LaunchImages.html#//apple_ref/doc/uid/TP40006556-CH22-SW1
