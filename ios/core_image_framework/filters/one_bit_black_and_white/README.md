# 1-Bit Black and White Processing
In the case of image recognition, 1-bit black and white image is more useful
for edge or border detection. To produce such image, I tried the following
filters based on the reference.

<br>
We'll be using ```CIColorControls``` and ```CIExposureAdjust``` for enhancing
the existing imagery color. Next, we switch the image to black and white using
```CIColorMonochrome```. Lastly, we use ```CIColorPosterize``` for thresholding
the intensity to only 2 levels: black and white.

<br><br><hr>
## Objective - C
```
    CIImage *beginIamge = [CIImage imageWithCGImage:image.CGImage];
    CIFilter *filter;
    filter = [CIFilter filterWithName:@"CIColorControls"
                        keysAndValues:kCIInputImageKey, beginIamge,
                                      @"inputBrightness", [NSNumber numberWithFloat:0.0],
                                      @"inputContrast", [NSNumber numberWithFloat:1.1],
                                      @"inputSaturation", [NSNumber numberWithFloat:0.0],
                                      nil
              ];
    filter = [CIFilter filterWithName:@"CIExposureAdjust"
                        keysAndValues:kCIInputImageKey, [filter valueForKey:kCIOutputImageKey],
                                      @"inputEV", [NSNumber numberWithFloat:0.7],
              nil
              ];
    filter = [CIFilter filterWithName:@"CIColorMonochrome"
                        keysAndValues:kCIInputImageKey, [filter valueForKey:kCIOutputImageKey],
                                      @"inputIntensity", [NSNumber numberWithFloat:1.0],
                                      @"inputColor", [[CIColor alloc] initWithColor:[UIColor whiteColor]], nil
              ];
    filter = [CIFilter filterWithName:@"CIColorPosterize"
                        keysAndValues:kCIInputImageKey, [filter valueForKey:kCIOutputImageKey],
                                      @"inputLevels", @2,
              nil
              ];
    CIImage *output = filter.outputImage;
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgiImage = [context createCGImage:output fromRect:output.extent];

    UIImage *newImage = [UIImage imageWithCGImage:cgiImage scale:scaleRatio orientation:image.imageOrientation];
    CGImageRelease(cgiImage);
    return newImage;
```

<br><br><hr>
## References
1. http://stackoverflow.com/questions/10030631/what-is-the-best-core-image-filter-to-produce-black-and-white-effects