# CoreImage Framework
CoreImage framework is like the 'Photoshop' SDK that does image processing
(not to confused with image recognition like OpenCV). To date, CoreImage
uses either CPU or GPU to render the images. Its early competitor, GPUImage has
a comparable performance. The only difference is its source: CoreImage is from
Apple while GPUImage is from open source community.

<br>
However, noted that it uses C library code which hence, requires you to be
very careful with memory management. Also, instead of using ```UIImage```,
CoreImage uses ```CIImage``` object instead.

<br>
This is a simple 4 steps guide on applying available CIFilter for existing
images. For advanced image processing, I will likely create a sub-folder
to host the notes.

<br><br><hr>
## Useful Steps for Applying CIFilter
### 1) Converting UIImage to CIImage object
This is to convert the common UIImage into CIImage. It uses the CGIImage
inherited inside UIImage.

<br>
#### Objective-C
```
CIImage *beginIamge = [CIImage imageWithCGImage:image.CGImage];
```


<br><br>
### 2) Apply CI Filter to image
Next is to create the filtered image, also known as CIFilter object.

#### Objective-C
```
    CIFilter *filter;
    filter = [CIFilter filterWithName:@"CIColorControls"
                        keysAndValues:kCIInputImageKey, beginIamge,
                                      @"inputBrightness", [NSNumber numberWithFloat:0.0],
                                      @"inputContrast", [NSNumber numberWithFloat:1.1],
                                      @"inputSaturation", [NSNumber numberWithFloat:0.0],
                                      nil
              ];
```

> NOTE:
>
> Each filters has its own filter name and its respective keys and values.
> CoreImage employs kernel codes for performance advantages. You may need to
> read the official document for the different CI Filters.


<br><br>
### 3) Chain CI Filter imagery
To perform chain image, just make use of
```[filter valueForKey:kCIOutputImageKey]``` to generate the next filter input.
Example:

<br>
#### Objective-C
```
    filter = [CIFilter filterWithName:@"CIColorControls"
                        keysAndValues:kCIInputImageKey, beginIamge,
                                      @"inputBrightness", [NSNumber numberWithFloat:0.0],
                                      @"inputContrast", [NSNumber numberWithFloat:1.1],
                                      @"inputSaturation", [NSNumber numberWithFloat:0.0],
                                      nil
              ];
    filter = [CIFilter filterWithName:@"CIExposureAdjust"
                        keysAndValues:kCIInputImageKey, [filter valueForKey:kCIOutputImageKey],
                                      @"inputEV", [NSNumber numberWithFloat:0.7],
              nil
              ];
    filter = [CIFilter filterWithName:@"CIColorMonochrome"
                        keysAndValues:kCIInputImageKey, [filter valueForKey:kCIOutputImageKey],
                                      @"inputIntensity", [NSNumber numberWithFloat:1.0],
                                      @"inputColor", [[CIColor alloc] initWithColor:[UIColor whiteColor]], nil
              ];
```


<br><br>
### 4) Convert back to a new UIImage
Once you're done with image processing, you next role is to convert it back
to an useful UIImage. You'll need to use CGIImage as a bridge between CIImage
and UIImage. Example:

<br>
#### Objective-C
```
    CIImage *output = filter.outputImage;
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgiImage = [context createCGImage:output fromRect:output.extent];
    
    UIImage *newImage = [UIImage imageWithCGImage:cgiImage
                                            scale:1.0
                                      orientation:image.imageOrientation];
    CGImageRelease(cgiImage);
    return newImage;
```


<br><br><hr>
## Best Practices
Due to the heavy resources consumption, you got to be careful when you're
performing image processing. This [link](https://developer.apple.com/library/ios/documentation/GraphicsImaging/Conceptual/CoreImaging/ci_performance/ci_performance.html#//apple_ref/doc/uid/TP30001185-CH10-SW1) spells out all the best practices to produce best
performance available for your app.


<br><br><hr>
## References
1. https://developer.apple.com/library/ios/documentation/GraphicsImaging/Conceptual/CoreImaging/ci_intro/ci_intro.html
2. https://developer.apple.com/library/ios/documentation/GraphicsImaging/Conceptual/CoreImaging/ci_performance/ci_performance.html#//apple_ref/doc/uid/TP30001185-CH10-SW1
3. https://developer.apple.com/library/ios/documentation/GraphicsImaging/Conceptual/CoreImaging/ci_tasks/ci_tasks.html#//apple_ref/doc/uid/TP30001185-CH3-TPXREF101
4. https://developer.apple.com/library/ios/documentation/GraphicsImaging/Conceptual/CoreImaging/ci_detect_faces/ci_detect_faces.html#//apple_ref/doc/uid/TP30001185-CH8-SW1
5. https://developer.apple.com/library/ios/documentation/GraphicsImaging/Conceptual/CoreImaging/ci_autoadjustment/ci_autoadjustmentSAVE.html#//apple_ref/doc/uid/TP30001185-CH11-SW1
6. https://developer.apple.com/library/ios/documentation/GraphicsImaging/Conceptual/CoreImaging/ci_filer_recipes/ci_filter_recipes.html#//apple_ref/doc/uid/TP30001185-CH4-SW3
7. https://developer.apple.com/library/ios/documentation/GraphicsImaging/Conceptual/CoreImaging/ci_concepts/ci_concepts.html#//apple_ref/doc/uid/TP30001185-CH2-TPXREF101
8. https://developer.apple.com/library/ios/documentation/GraphicsImaging/Conceptual/CoreImaging/ci_feedback_based/ci_feedback_based.html#//apple_ref/doc/uid/TP30001185-CH5-SW5
9. https://developer.apple.com/library/ios/documentation/GraphicsImaging/Conceptual/CoreImaging/ci_advanced_concepts/ci.advanced_concepts.html#//apple_ref/doc/uid/TP30001185-CH9-SW1
10. https://developer.apple.com/library/ios/documentation/GraphicsImaging/Conceptual/CoreImaging/ci_custom_filters/ci_custom_filters.html#//apple_ref/doc/uid/TP30001185-CH6-TPXREF101
11. https://developer.apple.com/library/ios/documentation/GraphicsImaging/Conceptual/CoreImaging/ci_image_units/ci_image_units.html#//apple_ref/doc/uid/TP30001185-CH7-SW12