# Email Mailer
In case of sending email as attachement? MessageUI framework comes to the
rescue!

<br>
MessageUI framework basically setup an email ready for user to send. However,
if you're thinking of auto-sending, as far as I know of, it's impossible unless
you use a 3rd party library.

<br>
Note that MessageUI framework view controller is a modal view controller.

<br><br><hr>
## Steps
### 1) Include the header
First and foremost, let's include the header for mailer.

<br>
#### Objective-c
```
#import <MessageUI/MessageUI.h>
```


<br><br>
### 2) Include the mailer delegates
Next is to include the delegates. The mailer will consistently calls this
delegate for different state in action.

<br>
#### Objective-c
```
@interface ViewController() <MFMailComposeViewControllerDelegate>
...
@end 

@implementation ViewController

...

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
 
    [self dismissViewControllerAnimated:YES completion:NULL];
}

...

@end
```

> NOTE:
>
> If user clicked 'send' button. The mailer will call the delegate based on
> the status changes. Hence, it's wise to dismiss the mailer ViewController
> here.


<br><br>
### 3) Setup your Mailer
Now you can setup your mailer. It's quite straight forward.

#### Objective-c
```
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];

    // Set the delegate to the correct view controller
    mail.mailComposeDelegate = self;

    // Set the subject
    [mail setSubject:@"Sample Subject"];

    // Set the message body
    [mail setMessageBody:@"Here is some main text in the email!" isHTML:NO];

    // Set receipients
    [mail setToRecipients:@[@"testingEmail@example.com",
                            @"testingEmail2@email.com"
    ]];

    // Attach an image file to the email
    NSData *fileData = [NSData dataWithContentsOfFile:imageFilePath];
    [mail addAttachmentData:fileData mimeType:@"image/tiff" fileName:@"sample.tff"];

    // Present as Modal
    [self presentViewController:mail animated:YES completion:NULL];
```

> NOTE: 
>
> For text body, you can set to HTML and code it in a HTML way.


<br><br>
### 4) That's it.
Yeap, test it out.


<br><br><hr>
## References
1. http://stackoverflow.com/questions/4302403/send-a-file-as-attachment-in-objective-c
2. http://stackoverflow.com/questions/14707645/attaching-txt-to-mfmailcomposeviewcontroller
3. http://www.codingexplorer.com/mfmailcomposeviewcontroller-send-email-in-your-apps/
4. http://www.tutorialspoint.com/ios/ios_sending_email.htm
5. http://www.oimmei.com/blog/using-standard-messageui-send-email/
6. http://www.appcoda.com/ios-programming-101-send-email-iphone-app/