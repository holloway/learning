# Document Picker
Building an app that needs to import stuff from 3rd party storage app like
Google Drive / Dropbox? UIDocumentMenuViewController is the right tool!

<br>
<div align="center">
<img src="https://lh3.googleusercontent.com/PIJkJNSV5x1pX8bIVea93LDkHoFntW2_qpO0Ttf-aG232pwEJu36UyOqrBraOPCl3qZbyt8z7kLJQOhQJKTXWPQRcGP4iNwgVq8vdSI8DJwqwzDACWQ2NxetSYEjsDslaslGz01aD0-j8PDSU1L4wHrXqA2W3Gq7B0CH2NYeDC5KDgB-s8dMQa27xu-4aIr9tV7jd7YYlLdqNZRFHYrIHYpHaaWdBwwlRn8joTM4x1dw7y64Cww86KDTzLuVnNM5G7RG0ueRKnO-jHg30eu97KYYYpdi0sx5fSBSZVrzkrQ4NuU4GxISXtMZhL2A8Mhsb09nw9YjYT_DZZhZvVgDwu2oxynUzHkNO8y3iNs_Kza0IjLp-k4146QGxy53fE6Wi50dZ8xxOp-CHKPGVek85_y17Ap5ZFMEWSqVdS6EhUjxZasqEQzsKwfLvelUPqATqP8yjoU0MfCzfrVmCenAJUtO0fM9wCxC06TbXVkX8CdiLIg7-kb7Hmb1qa3f7GQWkEQqIyHg1T3MWt_MGqY-zcct-wUSWwYBDjH2UhyGwU-NstUEVI1QX3B7lkltYhQTny632fhqEDg6Lp9SdtusYDcBwQaq65bf=w313-h235-no" alt="UIDocumentMenuViewController" />
</div>

<br>
The basics for UIDocumentMenuViewController is to either ```import``` or
```export``` files. There are more advanced options such as ```open``` or
```move``` available. However, it depends on whether 3rd party supports these
options.

<br>
Also, take note that you'll need to enable iCloud capabilities for your app,
which may or may not requires you to sign up for developer program.

<br>
> NOTE:
>
> There is a known auto-layout [issue](http://stackoverflow.com/questions/35457136/auto-layout-error-using-uidocumentmenuviewcontroller)
> from the library. Use with caution.


<br><br><hr>
## Steps
### 1. Import the necessary libraries
You'll need 2 libraries: the UIKit and the Mobile Core Services.

<br>
### Objective-c
```
#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
```


<br><br>
### 2. Implement the delegates for the class
```UIDocumentMenuViewController``` uses delegate to handle selected files.
Let's implement the delegate files inside our implementation file.

<br>
### Objective-c
```
@interface ClassName() <UIDocumentPickerDelegate>
...
@end

@implementation ClassName

...
- (void)documentMenu:(UIDocumentMenuViewController *)documentMenu
didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker
{
    documentPicker.delegate = self;
    [self presentViewController:documentPicker animated:YES completion:nil];
}


- (void)documentPicker:(UIDocumentPickerViewController *)controller
didPickDocumentAtURL:(NSURL *)url
{
    // image path
    NSString *stringPath = [[url absoluteString] stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    UIImage *importedImage = [UIImage imageWithContentsOfFile:stringPath];
    
    NSLog(@"%@", stringPath);
    ...
}

...

@end
```

> NOTE:
>
> Notice that ```UIDocumentMenuViewController``` is a 2 stages executions,
> which hence, requires 2 different delegates.
>
> 1. User selects which 3rd party app to use. (```didPickDocumentPicker:```)
> 2. User selects the file. (```didPickDocumentAtURL:```)
>
> Your document processing codes should be inside ```didPickDocumentAtURL:```.


<br><br>
### 3. Create the UIDocumentMenuViewController and present it
Now that the delegates are ready. It's time to create the view controller and
present them. Unlike image picker, ```UIDocumentMenuViewController``` requires
you to specify the document types you're planning to import. In the following
code examples, I import PNG, JPEG or PDF files only. You can refer the
file constants [here](https://developer.apple.com/library/ios/documentation/MobileCoreServices/Reference/UTTypeRef/index.html#//apple_ref/doc/constant_group/UTI_Image_Content_Types).

<br>
#### Objective-c
```
UIDocumentMenuViewController *viewController = [[UIDocumentMenuViewController alloc]
                                                  initWithDocumentTypes:@[(NSString *)kUTTypePNG,
                                                                          (NSString *)kUTTypeJPEG,
                                                                          (NSString *)kUTTypePDF]
                                                                 inMode:UIDocumentPickerModeImport];
viewController.delegate = viewController;
[viewController.view setNeedsLayout];
[self presentViewController:viewController animated:YES completion:nil];
```


<br><br>
### 4) Done. Test it out.
Most of the app permission etcs. are handled by the native library. Enjoy the
fun.


<br><br><hr>
## References
1. https://developer.apple.com/library/ios/documentation/MobileCoreServices/Reference/UTTypeRef/index.html#//apple_ref/doc/constant_group/UTI_Image_Content_Types
2. https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIDocumentMenuViewController_Class/index.html
3. https://developer.apple.com/library/ios/documentation/FileManagement/Conceptual/DocumentPickerProgrammingGuide/AccessingDocuments/AccessingDocuments.html