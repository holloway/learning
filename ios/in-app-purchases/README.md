# In-App Purchases
Now all your apps are ready, it's time to setup some shop to earn some $$$.
Apple supplies 2 kinds of payment gateway:

1. In-app purchases
2. Apple Pay

<br>
Each of them are meant for different usage. In-app purchases is for digital
app contents like credits, remaining hours, subscriptions etc. Based on
Apple documentations, **it is not allowed for physical goods** like t-shirts,
toaster, lunch pack.

<br>
Apple Pay is for **physical goods**, like those goods selling from e-commerce.
It acts like a credit cards.

<br>
This guide focuses only on in-app purchases. Also, you'll need to be part of
Apple Developer Program in order to proceed further.


<br><br><hr>
## Assumptions
1. You already know ```UITableViewController```.
2. Right now, this guide only supports **consumable** product.


<br><br><hr>
## Steps
### 1) Setup your iTune Connect App Store
It's a straight forward setup on [iTune Connect website](https://itunesconnect.apple.com/itc/static/login).
Apple will prompt you to setup your account for monetary management. Upon
approval, you can proceed to your app and create a product.

<br>
These are the simplified steps for creating a simple product:
1. Go to ```My App```.
2. Select ```Your App```.
3. Select ```Features``` in the header. This leads you to a sidebar for managing
products
4. Go to ```In-App Purchases``` inside the sidebar. 
5. Click the add icon ```+``` besides the title, above the product tables.
6. Select your product based on [Apple Documentation](https://support.apple.com/en-us/HT202023).
7. For References Name, you can fill in anything you want (for your own references).
8. Fill in the ```product identifier```, for best practices,
this ```tld.websitename.appname.referencename``` works pretty well. Example:
```com.quanto.trade.realtimestream```.
9. Choose a price tag from Apple's price matrix.
10. ```add a new language``` to display the product information, such as
**title** and **product description**. These information are accessible by your
users so try to keep them concise and action style.
11. For sandbox, you can leave **review notes** blank for now.
12. For sandbox, you can leave **screenshot for review** blank for now.
13. Click ```save``` and wait Apple to process them.

> NOTE:
>
> Keep the ```product identifier```. We'll use it for querying product
> information later, from our server and iTune server.


<br><br>
### 2) Design your shop storyboard
Start off by designing your shop. For simplification, I'm sticking to current
design using UITableViewController:

![shop-design](https://lh3.googleusercontent.com/cW7JZNETmpO_Ut34cxm4l-w7HCw5dhfoI6_iynQT68KsmV8mZE1Ug4EFPjEDDMPv8BBmACzAgAZ9hREYP60kn62wlxmNHrYJvP0DwAS-swH84GHMyhML0Y5OJZ1vRd7GdbM6MRfa0uqik4XSQE8QFf0nhwUINEFW5wjvaolYxvB0Sr3HNHpwbzf3_5dEZKWyUZECxUYpvean2iaqNu_PyaTYacFKEStC6Ea6g2z1bKEtZ82ZaBnL_c5BmecxwPUxSYNmDzyMm-bX1oJ6QN8afJosJKBsLtdzgr404lO2zesgPhG_JM7lONXucGh-0qTSBlHm4hpl_xd_EL8AgD7NeUvh-hvecKx643dl8-j5mqRurRBCYfsbpMg0_M0xdfTX8zD1ZyVqga8FkYF-ZbfCkrjBCT2qhBvm6X6Vz4P96HprU2iN8EDPXsdOfI2QjsYcw8MaXiKwmU3WtPFWQ1LGNfT4FU4vlqIpycGp6F4q3ImAUXqI1FGjLDZG6O-q6HhYcY9z30MwGRTG9my635ERMkLeRut9igpPu7QN9festR3FKtA9_jrOi1SBPqsm9yvxyb3C_i-9ifeHuoKc6BPVnNjpXu9b2SoU=w640-h1136-no)


<br><br>
### 2) Import StoreKit Library
We'll be using StoreKit framework to host and query data from iTune Connects.

#### Objective-c
```
#import <StoreKit/StoreKit.h>
```


<br><br>
### 2) Set delegates
StoreKit implements several delegates from querying info to purchases. Hence,
we'll need to implement them.

#### Objective-c
```
@interface MyViewController() <SKProductsRequestDelegate,
                               SKPaymentTransactionObserver>
...
@property SKProduct *exampleProduct;

@end


@implementation MyViewController

...

- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response
{
    if (response.products.count <= 0) {
        NSLog(@"No product is available");
        return;
    }
    
    for (SKProduct *iTunesProduct in response.products) {
        // process your product data if you have a lot
        // this example only shows single product saving
        self.exampleProduct = iTunesProduct
    }
}


- (void)paymentQueue:(SKPaymentQueue *)queue
 updatedTransactions:(nonnull NSArray<SKPaymentTransaction *> *)transactions
{
    for(SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"User is purchasing.");
                // Do nothing except data analytics
                break;
            case SKPaymentTransactionStatePurchased:
                NSLog(@"User has purchased!");
                // Perform purchased operations
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"User wants to restore the feature.");
                // Perform restoration operations
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Transaction failed");
                if(transaction.error.code == SKErrorPaymentCancelled) {
                    NSLog(@"Transaction was cancelled.");
                }

                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            default:
                break;
        }
    }
}
...

@end
```

> NOTE:
>
> The first delegate is for StoreKit to handle all queried products from
> iTune store. In this example, I'll save only 1 product to a property.
>
> The second delegate is for transaction handling. Notice that each cases
> has a clear-cut comments and ready for future implementation.


<br><br>
### 3) Implement Product Methods
Next is to implement query product methods. For this example, we'll define
the ```product identifier``` inside the app itself. You are allowed to query
these identifiers from your backend server, as stated by [Apple Guideline](https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/StoreKitGuide/Chapters/ShowUI.html#//apple_ref/doc/uid/TP40008267-CH3-SW5).

#### Objective-c
```
#define  MY_FIRST_PRODUCT       @"com.quanto.trade.realtimestream" // Product Identifier


@interface MyViewController() <SKProductsRequestDelegate,
                               SKPaymentTransactionObserver>
...

@end


@implementation MyViewController

...

- (void)requestAllProductsFromITunes
{
    NSMutableArray *productTags = [[NSMutableArray alloc] initWithCapacity:self.products.count];
    
    // setup product list
    [productTags addObject:MY_FIRST_PRODUCT];   /* if you have other products, continue to add them */
    
    // proceed with iTune query
    self.productRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray:productTags]];
    self.productRequest.delegate = (id)self;
    [self.productRequest start];
}

...

@end
```


<br><br>
### 4) Implement the Purchase Methods
After we get the product request method up, it's time to implement purchase
methods. Purchase methods have 2 stages implementation, mainly for Astral
(parental control). The first stage is to detect whether user is allowed to
purchase product. The second stage is the actual purchases.

<br>
Keep in mind that as a good user experience, you should design each handling
for each users.

#### objective-c
```
@interface MyViewController() <SKProductsRequestDelegate,
                               SKPaymentTransactionObserver>
...

@end


@implementation MyViewController

...

- (void)purchase:(NSString *)ProductIdentifier
{
    SKProduct *product;
    
    // check the product identifer for product
    if ([self.exampleProduct.productIdentifier isEqualToString:ProductIdentifier]) {
        product = self.exampleProduct;
        break;
    } else {
      NSLog(@"FATAL error! Refresh all your product query");
      return;
    }

    // process with parental control checking
    if ([SKPaymentQueue canMakePayments]) {
        NSLog(@"Parental control disabled. User is able to pay.");
        [self startPurchase:product];
    } else {
        NSLog(@"Parental control enabled. User is unable to pay.");
        // Proceed with handling
    }
}


- (void)startPurchase:(SKProduct *)product
{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    self.purchaseProduct = product;
    [[SKPaymentQueue defaultQueue] addTransactionObserver:(id)self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

...

@end
```


<br><br>
### 5) Present Your Products to View Controller
With everything in place, you can now present your product details to the
ViewController. Keep in mind that data query above happens asynchonously so
you'll need to handle the view carefully. I would suggest you query the shop
data right at the app loads before user can use the app.

#### Objective-c
```
@interface MyViewController() <SKProductsRequestDelegate,
                               SKPaymentTransactionObserver>
...

@end


@implementation MyViewController

...

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"productCell"
                                                            forIndexPath:indexPath];
    SKProduct *product = self.exampleProduct;
    
    // get title
    cell.productTitle.text = product.localizedTitle;

    // get description
    cell.productDescription.text = product.localizedDescription;
    
    // get localized pricing
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:product.details.priceLocale];
    NSString *price = [numberFormatter stringFromNumber:product.details.price];
    
    // setup buy button with price label
    cell.buyButton.accessibilityIdentifier = product.details.productIdentifier;
    [cell.buyButton addTarget:self
                       action:@selector(didSelectBuyButton:)
             forControlEvents:UIControlEventTouchUpInside];
    [cell.buyButton setTitle:price forState:UIControlStateNormal];
    return cell;
}


- (IBAction)didSelectBuyButton:(id)sender
{
    UIButton *button = (UIButton *)sender;
    [self purchase:button.accessibilityIdentifier];
}


...

@end
```

> NOTE:
>
> Notice that iTune Connects solved a few complicated items for us:
> localized pricing, localized title and localized description. Hence, it is
> ALWAYS THE BEST PRACTICE to keep these information on the iTunes Connects.
>
> In the case where you need to query icon images, you can do so from your
> backend server. However, keep in mind that your backend server should store
> information other than those inside iTunes Connects.


<br><br>
### 6) That's it.
That's it. This is the simple purchase implementation.


<br><br><hr>
## References
1. https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/StoreKitGuide/Chapters/ShowUI.html
2. https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/StoreKitGuide/Chapters/ShowUI.html
3. https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/StoreKitGuide/Introduction.html
4. http://www.techotopia.com/index.php/An_iOS_7_In-App_Purchase_Tutorial
5. http://stackoverflow.com/questions/19556336/how-do-you-add-an-in-app-purchase-to-an-ios-application
6. https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/StoreKitGuide/Chapters/RequestPayment.html#//apple_ref/doc/uid/TP40008267-CH4-SW2
7. https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/StoreKitGuide/Chapters/ShowUI.html#//apple_ref/doc/uid/TP40008267-CH3-SW5
8. https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/StoreKitGuide/Introduction.html
9. https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/StoreKitGuide/Chapters/DeliverProduct.html#//apple_ref/doc/uid/TP40008267-CH5-SW3
