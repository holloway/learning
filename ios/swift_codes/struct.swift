struct Card {
	var call:Int
	var name:String
	func simpleDescription() -> String {
		return "simple structure"
	}
}

let card = Card(call:2, name:"Hello")
print(card.simpleDescription())


// struct is similar to C's struct and Class. The only difference is struct
// is copied while class is pointer passing.