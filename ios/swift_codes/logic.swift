// If Else If
let currentValue = 0
let targetValue = 5
var difference: Int

if currentValue > targetValue {
	difference = currentValue - targetValue
} else if currentValue < targetValue {
	difference = targetValue - currentValue
} else {
	difference = 0
}

// switch cases
let vegetable = "red pepper"

switch vegetable {
case "celery":
	print("Celery")
case "cucumber", "walnuts":
	print("Health")
case let x where x.hasSuffix("pepper"):
	print("Spicy \(x) huh?")
default:
	print("Simple and nice")
}
// NOTE - all cases are auto-breaking. It won't continue like C does.


// for loop
let individualScores = [75, 43, 103, 87, 12]
for score in individualScores {
	print(score);
}

let interestingNumbers = [
    "Prime": [2, 3, 5, 7, 11, 13],
    "Fibonacci": [1, 1, 2, 3, 5, 8],
    "Square": [1, 4, 9, 16, 25],
]
for (kind, numbers) in interestingNumbers {
	print("operate on it")
}

for i in 0..<4 {
	print("do something from 0 to 3")
}

for i in 0...4 {
	print ("do something from 0 to 4")
}


// while loop
var n = 2
while n < 100 {
	n += 1
}


