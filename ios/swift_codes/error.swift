enum PrinterError: Error {
	case outOfPaper
	case noToner
	case onFire
}

func send(job: Int, toPrinter printerName: String) throws -> String {
	if printerName == "Never Has Toner" {
		throw PrinterError.noToner
	}

	return "Job sent"
}

do {
	let printerResponse = try send(job:1040, toPrinter: "BiSheng")
	print(printerResponse)
} catch PrinterError.noToner {
	print("No toner already")
} catch PrinterError.onFire {
	print("Your document got roasted!")
} catch {
	print(error)
}