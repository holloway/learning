// Swift comments
	// Indent (2-4 spaces)

// Variables
var variables = 5
var targetValue: Int = 0 //Specify types

	// optional values
var optionalString: String? = "Hello"

// Constants
let constants = 3.142


// String
"This is a proper string"
'Not a string'

let message = "The value of the slider is: \(currentValue)"  // Joining String
            + "\nThe target value is: \(targetValue)"

// Boolean
true
false

// Numbers
2, 5, 123123, 5434534

// Array
var emptyArray = []
var shoppingList = ["catfish", "carrot"]
shoppingList[1] = "corn"

// Dictionary
var emptyDictionary = [:]
var occupations = ["a" : 0,
									 "b" : 1,
									]
occupations["a"] = -1


// Number Conversion
// Float to Long
var long_number = lroundf(234.2342342)
// Numbers to Float
var float_number = Float(234234234)
// Number to String
var string = String(23423423)
// String to Array
// String to Dictionary
// Dictionary to String
// Array to String


// Print to Console
print("Hello World")


// Placeholder
let nickName: String? = nil
let fullName: String = "John"
let greeting = "Hi \(nickName ?? fullName)"
