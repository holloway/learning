// Basic class example
class Shape {
	var numberOfSides: Int = 0
	var name: String
	let language = "Swift"	// immutable constant property even by inheritance
	final var version = "2.0"		// make a method or property immutable by inheritance
	var classConstantProperty: String { get { return "Generic Shape" } }	// mutable constant property through inheritance

	init(name: String) {
		self.name = name
	}

	func simpleDescription() -> String {
		return "this shape has \(numberOfSides)."
	}

	deinit {
		// do custom cleanup
	}
}


var shape = Shape(name: "Square")
shape.numberOfSides = 4
print(shape.simpleDescription())

// Inheritance Example: Square inherits Shape on top
class Square: Shape {
	var sideLength: Double
	override var classConstantProperty: String { get { return "square" } }	// override parent's mutable constant property

	// customize variable's getter and setter methods
	var perimeter: Double {
		get {
			return 4.0 * sideLength
		}
		set {
			self.sideLength = newValue / 4
		}
	}

	init(sideLength: Double, name: String) {
		self.sideLength = sideLength
		super.init(name: name)
		numberOfSides = 4
	}

	func area() -> Double {
		return sideLength * sideLength
	}

	override func simpleDescription() -> String {									// override parent method
		return "A square with side of length \(sideLength)"
	}
}
let square = Square(sideLength: 4.0, name: "Query")
print(square.area())
print(square.simpleDescription())
