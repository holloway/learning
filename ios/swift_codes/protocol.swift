protocol ExampleProtocol {
	var simpleDescription: String { get }
	mutating func adjust()
}

class SimpleClass: ExampleProtocol {
	var simpleDescription: String = "A very simple class"
	var anotherProperty: Int = 69105

	func adjust() {
		simpleDescription += "Now 100% justified"
	}
}
var a = SimpleClass()
a.adjust()
print(a.simpleDescription)


struct SimpleStructure: ExampleProtocol {
	var simpleDescription: String = "A very simple structure"

	// in struct, function is immutable. Hence, you'll need the keyword 'mutating' to indicate a function has changed
	mutating func adust() {
		simpleDescription += "ADJUSTED!"
	}
}
var b = SimpleStructure()
b.adjust()
print(b.simpleDescription)


// To extend a protocol
extension Int: ExampleProtocol {
	var simpleDescription: String {
		return "The number \(self)"
	}

	mutating func adjust() {
		self += 42
	}
}
print(7.simpleDescription)
