// NOTE: for a simple objective-c function is:
// (NSString *)greetWithPerson:(NSString *)person day:(NSString *)day
func greet(person: String, day: String) -> String {
	return "Hello \(person). Today is \(day)"
}
let statement = greet(person: "John", day: "Wednesday")
print(statement)


// To remove labelling when using, add "_" in the parameter label
func greet(_ person: String, day: String) -> String {
	return "Hello \(person). Today is \(day)"
}
let statement = greet("John", day: "Wednesday")
print(statement)


// To run a final code block execution before returning, regardless error, use defer
func greet(person: String, day: String) -> String {
	defer {
		print("recycling this method. Cya!")
	}

	return "Hello \(person). Today is \(day)"
}



// Code blocks
function hasAnyMatches(list: [Int], condition: (Int) -> Bool) -> Bool {
	for item in list {
		if condition(item)
			return true
	}

	return false
}

function lessThanTen(number: Int) -> Bool {
	return number < 10
}
hasAnyMatches(list: [20, 19, 7, 12], condition: lessThanTen)
hasAnyMatches(list: [20, 19, 7, 12], condition: {
	number in
	return number * 10 == 100
	})
hasAnyMatches(list: [20, 19, 7, 12], condition: {
	return $0 * 10 == 100
	})