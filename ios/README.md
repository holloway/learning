# iOS
This folder contains all objective C knowledge ready to be shared publicly.

<br>
The folder arrangement is purpose-diven. If you're looking for quick code
referencing, please look into codes folder.


<br><br>
## Setup
If you're looking for MVVM Framework, please proceed to this
[link](https://gitlab.com/hollowaykeanho/learning/tree/master/ios/reactive_MVVM_framework).


<br><br>
## Limitation
Currently I'm digging deep into iOS using Objective-C. Once I mastered necessary
amount of components, I'll do a language switch to Swift.


<br><br>
## References
1. https://medium.com/ios-os-x-development/ios-architecture-patterns-ecba4c38de52#.7hy6cfh56
2. http://stackoverflow.com/questions/35793259/real-scenario-for-implementation-of-mvc-vs-mvvm-vs-viper-for-ios-based-projects
3. https://www.raywenderlich.com/126522/reactivecocoa-vs-rxswift