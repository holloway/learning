# Add Delay Job execution for OpenShift Rails
In the case where you need background job using delayed_job gem on OpenShift
platform, this is the right note. Delayed_job is an excellent gem for
background work at the cost of using database I/O traffic.

<br>
Unlike Sidekiq, you don't need to spend 2 gears (extra horsepower) on Redis and
Sidekiq cartidges respectively.

<br><br><hr>
## Assumptions
1. Ruby 2.0.0
2. [Already Generated OpenShift App](https://gitlab.com/hollowaykeanho/learning/tree/master/rails/openshift/create_new_app)
3. Rails 4.2
4. Experience in setting up Rails into external services like Heroku
5. Familar with Secure SSH
6. Aren't afraid of reading too much documentations
7. Old style hashing
8. [Know your GIT well!](https://gitlab.com/hollowaykeanho/learning/tree/master/git)
9. You're VERY experienced with [BASH Scripting](https://gitlab.com/hollowaykeanho/learning/tree/master/bash)
10. You're using ActiveRecord


<br><br><hr>
## Steps
### 1) Follow the delayed_job instruction to install into Rails.
Add the gemfile with daemon gem
```
gem 'delayed_job_active_record'
gem 'daemon'
```


<br><br>
### 2) Perform a bundle install for the gems.
```
$ bundle install
```


<br><br>
### 3) Configure the activejob to use delayed job.
Add the following configuration into your ***config/application.rb***:
```
config.active_job.queue_adapter = :delayed_job
```


<br><br>
### 4) Create the activejob database table.
```
$ bundle exec rails generate delayed_job:active_record
$ bundle exec rake db:migrate
```


<br><br>
### 5) Add the start sequence.
Add the following into your ***.openshift/override/start***:
```
/usr/bin/bundle exec $OPENSHIFT_REPO_DIR/bin/delayed_job -n 2 start |& /usr/bin/logshifter -tag ruby &
```

> NOTE:
>
> The local command is:
> ```
> $ bundle exec rake .bin/delayed_job -n <number_of_worker> start
> ```
>
> So remember to specify the number of worker for your app.

<br>
An example of output looks something like this:
```
#!/bin/bash -e
source $OPENSHIFT_REPO_DIR/.openshift/action_hooks/override/global_variables
CURRENT_LOCATION=$(pwd)
cd $OPENSHIFT_REPO_DIR

echo "Starting Server"
/usr/bin/bundle exec $OPENSHIFT_REPO_DIR/vendor/bundle/ruby/bin/puma -C config/puma.rb -b tcp://${OPENSHIFT_RUBY_IP}:${OPENSHIFT_RUBY_PORT} --pidfile $PUMA_PID_FILE |& /usr/bin/logshifter -tag ruby &
/usr/bin/bundle exec $OPENSHIFT_REPO_DIR/bin/delayed_job -n 2 start |& /usr/bin/logshifter -tag ruby &

cd $CURRENT_LOCATION
```


<br><br>
### 6) Add the stop sequence.
Now add the following into your ***.openshift/override/stop***:
```
/usr/bin/bundle exec $OPENSHIFT_REPO_DIR/bin/delayed_job stop |& /usr/bin/logshifter -tag ruby &
```

> NOTE:
>
> The local command is:
> ```
> $ bundle exec rake .bin/delayed_job stop
> ```
>
> So remember to specify the number of worker for your app.

<br>
An example of output looks something like this:
```
#!/bin/bash -e
source $OPENSHIFT_REPO_DIR/.openshift/action_hooks/override/global_variables
CURRENT_LOCATION=$(pwd)
cd $OPENSHIFT_REPO_DIR

echo "Stopping Puma"
if [ -s "$PUMA_PID_FILE" ]; then
  pgrep -f puma -u $UID | grep -q $(<"$PUMA_PID_FILE") && kill $(<"$PUMA_PID_FILE") || rm -f "$PUMA_PID_FILE"
  /usr/bin/bundle exec $OPENSHIFT_REPO_DIR/bin/delayed_job stop |& /usr/bin/logshifter -tag ruby &
fi

cd $CURRENT_LOCATION
```

<br><br>
### 7) Run a short test on background work.
You're done. Now run a short test before commiting to git.


<br><br>
### 8) commit and push to your OpenShift repository
Once the test is passing, you can now commit using git and push to your OpenShift production repository.


<br><br>
> NOTE:
>
> For mailer, on the official delayed job README, it needs its conventional delay call rather than
> using the deliver method.
>
> Hence, instead of:
> ```
> # without delayed_job
> Notifier.signup(@user).deliver
>
> # with delayed_job
> Notifier.delay.signup(@user)
> 
> # with delayed_job running at a specific time
> Notifier.delay(run_at: 5.minutes.from_now).signup(@user)
> ```
> as of to date of writing this README.md. Please get up-to-date information from the official documentation.

<br><br><hr>
## References
1. https://github.com/collectiveidea/delayed_job