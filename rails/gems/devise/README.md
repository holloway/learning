# Devise Gem
This is the massive user management control gem for Rails. It has all the
primary features including omni-authentication.

<br><hr><br><br>

## Assumptions
1. You have setup the [action-mailer](https://gitlab.com/hollowaykeanho/learning/tree/master/rails/gems/action-mailer).
2. You know Ruby on Rails inside out.

<br><hr><br><br>

## Steps
1) Add devise gem.
```
gem 'devise'
```

<br><br>

2) Perform Devise installation.
```
$ bundle exec rails generate devise:install
```

<br><br>

3) Generate the model. Example, user.
```
$ rails generate devise user
```

<br><br>

4) Generate the views for the model.
```
$ rails generate devise:views users
```

<br><br>

5) Generate the controllers for the model.
```
$ rails generate devise:controllers users
```

<br><br>

6) Update the routes to use custom controllers.
```
devise_for :users, :controllers => {:registrations => "users/registrations",
                                    :sessions => "users/sessions",
                                    :passwords => "users/passwords"
                                   }
```

<br><br>

7) Pretty much done. In the case if you need to customize Devise, you can
refer to their documentations and customize easily in the controller and views.


<br><hr><br><br>

## References
1. https://github.com/plataformatec/devise