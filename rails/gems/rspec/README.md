# Rspec Basics
Rspec is one of the unit test framework for Ruby and Rails. In this section,
we're going to learn some basic approach and common syntax to get unit test
up and running.


<br><br><hr>
## Overview
The most basic rspec code testing is using the ```expect``` and match your 
method or whatever you're testing expectation. Example:

<br>
cases:

1. say method ```find(integer)``` is expected to raise an error when no result is found.
2. say method ```id(integer)``` returns an integer number when valid, nil when is not valid.

<br>
Both cases are crisp and clear: a method is expected to do something when
given a situation.

<br>
So, let's write in plain english:
```
1. expect find("Abcs") to raise error # knowing that the params yield no result
2. expect id(5) to equal 5 # knowing that it returns 5
3. expect id(nil) to equal nil #knowing that it returns nil
```

<br>
Rpsec made not much of differences to level the test code to be closer to
plain English. Hence, if we translate to rspec code, it is as follows:
```
expect{ find("Abcs") }.to raise_error
expect( id(5) ).to eq 5
expect( id(nil) ).to eq nil
```

<br>
Plain simple? Welcome to the test-driven development for Ruby. We'll walk
through some common snytax and formats before we start coding.


<br><br><hr>
## Expectation format
Notice the above example, case (1) expect is passing in a proc ( {...} ) while
the others are passing in parameters ( (...) ). The difference between them
are plain simple:

<br>
1) If you're expecting a **behavior changes**, pass in a proc:
```
expect{ some_action_here }.to perform_an_action / perform a change
```

<br>
2) If you're expecting a **value changes** like return value, pass in a parameters:
```
expect(return_value_of_an_action).to equal? greater? smaller? etc. expected_value
```


<br><br><hr>
## to / not to
Rspec not only allows you to match positive expectation. It allows you to test
expected fail cases. Notice the above examples, we have been using ```.to```
to match an active pass & fail expectation. You can also use ```.not_to```
to do the opposite expectation.

The differences? not much apart liguistic and managing expectation. In the
following example, they're the same:
```
expect( id(5) ).to eq 5
expect( id(5) ).not_to eq nil
```

> NOTE:
>
> Although you're given a choice to use ```.to``` or ```.not_to```, try not
> not twist the sentence please. Make sure the sentence is direct, crisp and
> clear.
>
> In the following notes, I'll be using ```.to``` to expect a positive
> and negative expectation.


<br><br><hr>
## Equal Matcher
This is suitable for **value changes** expectation. To ensure an expectation
to equal an output, we uses equal matcher. The basic, less strict equal is
`eq()`:


```
expect( id(5) ).to eq 5
expect( id(nil) ).to eq nil
expect( "This is string" ).to eq("This is string")
expect( 5 ).to eq(5.0)    # They are different type, but pass in this case
```

<br>
If you're looking for a more strict equal matcher, you can use ```eql```:
```
expect( id(5) ).to eql 5
expect( id(nil) ).to eql nil
expect( "This is string" ).to eql("This is string")
expect( 5 ).to eql(5.0)    # fail in this case due to type differences
```

<br>
For string, if wish to be more linguistic, you can use ```equal```:
```
expect( "This is string" ).to equal("This is string")
```


<br><br><hr>
## Be Matcher
This is suitable for both **value changes** and **behavioral changes**
expectations, stating the result to be ```true```, ```false``` or ```nil```.
Example for cases below:

We're expecting:
```
expect(true).to be_truthy
expect(nil).to be_nil
expect(false).to be_falsey
```


<br><br><hr>
## Be Within Matcher
This is suitable for both **value changes** and **behavioral changes**
expectations, stating the result to falls within the range close to a number.

Example:
```
# 27.5

it { is_expected.to be_within(0.5).of(27.9) }   # |-0.5     27.9       +0.5|
it { is_expected.to be_within(0.5).of(28.0) }   # |-0.5     28.0       +0.5|
it { is_expected.to be_within(0.5).of(27.1) }   # |-0.5     27.1       +0.5|
it { is_expected.to be_within(0.5).of(27.0) }   # |-0.5     27.0       +0.5|
```


<br><br><hr>
## Change Matcher
This is suitable for **behavioral changes** expectation, stating a behavior
has changed.

Example:
```
expect { Counter.increment }.to change{ Counter.count }.from(0).to(1)
expect { Counter.increment }.to change{ Counter.count }.by(2)
expect { Counter.increment }.not_to change{ Counter.count }
```


<br><br><hr>
## Raise Error Matcher
This is suitable for **behavioral changes** expectation, stating a behavior is
expected to raise an error.

Example:
```
expect { raise "oops" }.to raise_error
expect { raise "oops" }.to raise_error(RuntimeError)          # specific error
expect { raise "oops" }.to raise_error("oops")                # custom error
expect { raise "oops" }.to raise_error(/op/)
expect { raise "oops" }.to raise_error(RuntimeError, "oops")
expect { raise "oops" }.to raise_error(RuntimeError, /op/)
```


<br><br><hr>
## Respond To Matcher
This is suitable for **value changes** expectation, stating a value is
expected to have a method to respond to.

Example:
```
expect(obj).to respond_to(:foo).with(1).argument
expect(obj).to respond_to(:bar).with(2).arguments
expect(obj).to respond_to(:baz).with(1..2).arguments
expect(obj).to respond_to(:xyz).with_unlimited_arguments
```


<br><br><hr>
## Include Matcher
This is suitable for **value changes** expectation, especially for array or
listed items, stating a value is expected to contain a certain items.

Example:
```
expect("a string").to include("a")
expect("a string").to include("str")
expect("a string").to include("str", "g")
expect("a string").not_to include("foo")

expect([1, 2]).to include(1)
expect([1, 2]).to include(1, 2)
expect([1, 2]).to include(a_kind_of(Integer))
expect([1, 2]).to include(be_odd.and be < 10 )
expect([1, 2]).to include(be_odd)
expect([1, 2]).not_to include(17)

expect(:a => 1, :b => 2).to include(:a)
expect(:a => 1, :b => 2).to include(:a, :b)
expect(:a => 1, :b => 2).to include(:a => 1)
expect(:a => 1, :b => 2).to include(:b => 2, :a => 1)
expect(:a => 1, :b => 2).not_to include(:c)
expect(:a => 1, :b => 2).not_to include(:a => 2)
expect(:a => 1, :b => 2).not_to include(:c => 3)
```


<br><br><hr>
## Match Matcher
This is suitable for **value changes** expectation, stating a value is expected
to match a certain pattern.

Example:
```
expect("a string").to match(/str/) # passes
expect("a string").to match(/foo/) # fails
expect(/foo/).to match("food")     # passes
expect(/foo/).to match("drinks")   # fails
```


<br><br><hr>
## ```it``` statement
```it``` statement is a describer describing a test case. When a test is is
running, rspec will use ```it``` descriptor to throw an output to you (when
you have failed cases or you're in verbose mode).

<br>
Each ```it``` runs in its own environment. They aren't inter-related.

Example:
```
it "expect a string to match a string regex" do
  expect("a string").to match(/str/) # passes
end
```


<br><br><hr>
## ```context``` statement
```context``` statement is a container holding a list of test cases describing
a context. Think of it like a chapter in a book. This chapter does this, this,
this test cases.

Example:
```
context "valid input" do
  it "raise an error when no argument is provided" do
    expect { Account.new }.to raise_error
  end
  it "accepts account number without initial balance" do
    expect { Account.new("1234567890") }.not_to raise_error
  end
  it "accepts both account number and initial balance as argument" do
    expect { Account.new("1234567890", 5000) }.not_to raise_error
  end
  it "raise an error when invalid account number (more than 10) is provided" do
    expect { Account.new("12345678901") }.to raise_error(InvalidAccountNumberError)
  end
  it "raise an error when invalid account number (less than 10) is provided" do
    expect { Account.new("123456789") }.to raise_error(InvalidAccountNumberError)
  end
end
```


<br><br><hr>
## ```describe``` statement
```describe``` statement is a master container holding a list of contexts
describing an object unit test. Think of it like a scientific journal. This
journal has this and that.

Example:
```
# Account.new
describe Account do
  let(:acct_number)     { "2342352523" }
  let(:hidden_acct_num) { "******2523" }
  let(:initial_balance) { 5000 }
  let(:account)         { Account.new(acct_number, initial_balance) }

  describe "#initialize" do
    context "valid input" do
      it "raise an error when no argument is provided" do
        expect { Account.new }.to raise_error
      end
      
      ...

    end
  end

  describe "#transactions" do
    context "returns transactions array" do
      ...
    end
  end

  describe "#balance" do
    context "returns balance" do
      ...
    end
  end

  describe "#account_number" do
    it "should return sliced acc number" do
      expect(account.acct_number).to eq hidden_acct_num
    end

    it "should not return full acc number" do
      expect(account.acct_number ).not_to eq acct_number
    end
  end
end
```

> NOTE:
>
> the difference between ```describe``` and ```context``` is its purpose.
>
> Example:
>
> 1. Describe "Class A" in the context of "attributes"
> 2. Describe "Method A" in the context of "given valid inputs"
> 3. Describe "Method A" in the context of "given invalid inputs"


<br><br><hr>
## ```let``` statement
```let``` statement is a variable used in rspec testing. Say you want something
to be consistent across all the test cases within a ```context``` or a
```describe``` hierarchy. 

Example:
```
describe Account do
  # example on how to declare test variables or pre-opereations
  let(:acct_number)     { "2342352523" }
  let(:hidden_acct_num) { "******2523" }
  let(:initial_balance) { 5000 }
  let(:account)         { Account.new(acct_number, initial_balance) }


  describe "#initialize" do
      # this describe statement can see:
      # 1) acct_number

    context "valid input" do
      # this context statement can see:
      # 1) acct_number
      let(:qaunta)      { "quanta"  }

      it "raise an error when no argument is provided" do
        # this test case can see:
        # 1) acct_number
        # 2) quanta
      end
      
      it "some other test cases" do
        # this test case can see:
        # 1) acct_number
        # 2) quanta
      end

      ...

    end
  end


  # This describe cases will not see :quanta since it's private
  describe "#transactions" do
    # this describe statement can see:
    # 1) acct_number

    context "returns transactions array" do
      # this context statement can see:
      # 1) acct_number

      it "some other test cases" do
        # this test case can see:
        # 1) acct_number
      end

      ...

    end
  end


  # Demonstrate how to use the variables
  describe "#account_number" do
    it "should return sliced acc number" do
      expect(account.acct_number).to eq hidden_acct_num
    end

    it "should not return full acc number" do
      expect(account.acct_number ).not_to eq acct_number
    end
  end


  ...
end
```


<br><br><hr>
## That's it
You're ready for Ruby Rspec Test Driven Programming!


<br><br><hr>
## References
1. https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers/equality-matchers
