# Rspec Rails
This section is about Rails with Rspec. In its sub-folders,
it's all about the code gist testing a particular case. This section has been
updated for Rails 5.

The following guide is more about setting up Rails with rspec.

1. CoffeeScript removal
2. Shoulda-matcher inclusion
3. Shoulda-callback-matcher inclusion
4. Factorygirl inclusion
5. Capybara inclusion
6. Selenium inclusion


<br><br><hr>
## Local Setup
If you're on Linux, you'll need to install these packages for Selenium driver.
```
$ sudo apt-get install firefox xvfb libqtwebkit-dev x11-xkb-utils -y
$ sudo apt-get install xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic -y

# go google chromedriver and install it inside your machine
```

<br>
If you're on Mac, you'll need to install these packages, assuming you're using
Brew:
```
$ brew install qt5
$ brew install chromedriver
$ brew link --force qt5
```


<br><br><hr>
## Steps
### 1) Create a rails new app.
```
$ rails new <app_name> --database=postgresql
```


<br><br><hr>
### 2) ***Optional***: Scrub the Gemfile comments.
You can clean up the comments.


<br><br><hr>
### 3) ***Optional***: Remove the CoffeeScript gem if you prefer javascript.
If you are fond to javascript instead of coffee scripts, Rails 5 allows you
to remove the gem so that the generator will generate ```.js``` file instead.


<br><br><hr>
### 4) Add the following gems
Depending on your needs, I've arranged the gems into categories. Here, I uses
rspec as my primary test cases. 
```
group :development do
  gem 'byebug'
end

group :development, :test do
  # must have gems 
  gem 'rspec'
  gem 'rspec-rails'
  gem 'shoulda-matchers'
  gem 'shoulda-callback-matchers'

  # factory girl if you're using mocking
  gem 'factory_girl_rails'

  # view integration test
  gem 'capybara'
  gem 'selenium-webdriver'
  gem 'capybara-webkit'
  gem 'headless'

  # For developer's code improvements on database query
  gem 'bullet'
end
```


<br><br><hr>
### 5) Bundle install to latest gem
```
$ bundle install
```


<br><br><hr>
### 6) If you're using Bullet gem, initialize it by adding the following inside
***config/environments/development.rb***:
```
config.after_initialize do
  Bullet.enable = true
  Bullet.alert = true
end
```


<br><br><hr>
### 7) Initialize Rspec installation:
```
$ rails generate rspec:install
```


<br><br><hr>
### 8) Setup shoulda-matchers
Achieve this by pasting the following codes into the ***spec/rails_helper.rb***,
after the last config 'end' syntax:
```
Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end
```


<br><br><hr>
### 9) Initialize Capybara
If you're using Capybara, initialize it by adding the following codes into
the **spec/rails_helper.rb**, at the top with the group of gems 'requires':
```
require 'capybara/rails'
```


<br><br><hr>
### 10) Setup production app with asset precompile
If you want to precompile your assets, you can achieve it by editing
**config/environments/production.rb** to:
```
 # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = true
  config.assets.precompile = ['*.js', '*.css']
```


<br><br><hr>
### 11) Commit your changes. Your TTD rails framework is ready.



<br><br><hr><hr>
# Using Rspec Rails
## Automatic Creation
Once rspec rails setup is done, each rails generators will create a spec test
script instead of the common unit test script.


<br><br><hr>
## Manual Creation
1) To generate a rspec test script manually:
```
$ rails g rspec:TYPE

e.g:

$ rails g rspec:model
```


<br><br><hr><hr>
# Other optimization gems:
You can install the following gems separately for optimization purposes.
You don't have to include them inside your gemfile.
Example:
```
gem 'colored'
gem 'traceroute'        # trace unused routes
gem 'rails_best_practices'      # scan codes for best practices
gem 'rubocop'         # scan codes for best practices
gem 'rubycritic'        # scan codes for best practices
gem 'brakeman'          # scan app for security
gem 'rack-mini-profiler'      # profile and benchmark app
```


<br><br><hr>
# References
1. https://github.com/jc00ke/guard-puma
2. https://github.com/rspec/rspec-rails
3. https://github.com/guard/guard-rspec
4. https://github.com/thoughtbot/shoulda-matchers#configuration
5. https://github.com/thoughtbot/factory_girl_rails
6. https://github.com/beatrichartz/shoulda-callback-matchers
7. https://gist.github.com/kyletcarlson/6234923
8. https://gist.github.com/eliotsykes/5b71277b0813fbc0df56
9. http://ricostacruz.com/cheatsheets/rspec-rails.html
10. https://github.com/flyerhzm/bullet
11. https://github.com/whitesmith/rubycritic
12. https://github.com/amatsuda/traceroute
13. https://github.com/presidentbeef/brakeman
14. https://github.com/railsbp/rails_best_practices
15. https://github.com/bbatsov/rubocop#cops
16. https://github.com/MiniProfiler/rack-mini-profiler
17. http://joanswork.com/rubocop-rails-getting-started/
18. https://houndci.com/configuration
