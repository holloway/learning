# Rspec for Rails Controller
Testing controller, usually folks would called it *integration* test is a kind
of test strategy focuses on getting input and process output at API level. In
this guide, we'll show each cases one by one.

In this example, we're going to walk through each examples from basic to 
advanced in controllers.


<br><br><hr>
## 1) basic controller testing in a common GET API
Now, for a controller as simple as this the following ```:new``` API, we'll
look into the basic rspec structure.
```
class UsersController < ApplicationController
  def new
    @user = User.new
  end
end
```

<br>
So the method is quite simple, when ```:new``` API is called, we know it will
create a user class instance from ```User``` model and since there is no
specification, we know that it will generate ```:new``` view file. Hence,
we're clear that:

**Input**: create ```@user``` assignment

**Output**: render ```:new``` template

<br>
So, the very basic code looks something like this:
```
require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  let(:user) { User.create(:user) }

  describe 'GET #new' do
    it 'assigns @user to be a new User' do
      get :new
      expect(assigns(:user)).to be_an_instance_of User
    end

    ... # some process test cases

    it 'renders #new template' do
      get :new
      expect(response).to render_template 'new'
    end
  end
end
```

> NOTE:
>
> In the first test case, it's testing the input. In the second case, it is
> testing the output. 
>


<br><br><hr>
## 2) testing assignments
Those 'class' variables ```@user``` in the above, we called it **assignments**.
To test these assignments we uses the following statement inside a test case:
```
expect(assigns(:user)).to be_an_instance_of User    # For @user = User.new
```

> NOTE:
>
> The more you have, the more you'll need to test. Try not to hard-code too
> many items.


<br><br><hr>
## 3) testing output rendering
If you want to test the view type, you can use the following examples
depending on your expectation. For example:
```
expect(response).to render_template 'new'   # if the controller render 'new'
expect(response).to have_http_status 200    # http reply 200
expect(response.headers["Content-Type"]).to eq "application/html; charset=utf-8" # test header attributes
expect(response.headers["Content-Type"]).to eq "application/json; charset=utf-8" # test header attributes
expect(response.body).to eq ""              # no body content http#204
```

<br>
If you're writing json reply, then you'll need a different module to test
each elements:
```
require "rspec/json_expectations"

expect(response.body).to include_json(
      id: 25,
      email: "john.smith@example.com",
      name: "John"
    )
```

> NOTE:
>
> The body is:
>
> {
>
>   "id": 25,
>
>   "email": "john.smith@example.com",
>
>   "name": "John"
>
> }

<br>
> NOTE:
>
> In case you want to know more about [http code](https://httpstatuses.com/).


<br><br><hr>
## 4) test model data changes
Now that you know how to test the input and output, we can now test the
processing in the middle. On controller level, 
**you do not test the contents inside the model**, just the output from Model
will do. Let's consider another case:
```
def create
  @user = User.new(user_params)
  if @user.save
    redirect_to @user
  else
    render :new
  end
end
```

In this POST CREATE API case, we see there is a data change in for user model
(it saves a user data).

Hence, to test this, we expect the data model total count to change. Example
```
expect{ post :create, user: valid_user_params }.to change{ User.count }.by(1)
expect{ post :create, user: invalid_user_params }.not_to change{ User.count }.by(1)
```


<br><br><hr>
## 5) provides params and setting test variables & POST - CREATE API
notice the test cases in the example above, the note simplified the
```user_params``` but never stated how. Hence, we uses ```let``` keyword to
declare test variables inside ```describe``` and/or ```context```. By delaring
test variables, you're making the test data consistent across each test cases
and easier to manipulate. (GREAT! no hardcoding.)

Let's reconsider the test case above again:
```
def create
  @user = User.new(user_params)
  if @user.save
    redirect_to @user
  else
    render :new
  end
end
```

We'll write a test case probably looks something as such:
```
require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  let(:user) { User.new(:user) }
  let(:valid_params) { { email: user.email, username: user.username } }
  let(:invalid_params) { { email: 'wrong@email', username: user.username} }

  describe 'POST #create' do
    context 'with valid input' do
      it 'saves current user' do
        expect{ post :create, user: valid_params }.to change{ User.count }.by(1)
      end

      it 'assigns user with correct input' do
        post :create, user: valid_params
        expect(assigns(:user).email).to eq user.email
        expect(assigns(:user).username).to eq user.username
      end

      it 'redirects to user #show page' do
        post :create, user: valid_params
        expect(response).to redirect_to user_path(user)
      end
    end

    context 'with invalid input' do
      before(:each) { post :create, user: invalid_params }

      it 'renders #new' do
        expect(response).to render_template :new
      end
    end
  end

end
```

> NOTE:
>
> The higher the ```let``` hierarchy, the more exposed to other test cases it
> is. In the example above, all the context and test cases can use the test
> variables.

> NOTE:
>
> Take note how did we pass the params into an API during test mode:
> ```
> post :create, user: valid_params
> post :create, user: invalid_params
> ```

> NOTE:
>
> Also, take note on how we declare a parameter test variables:
> ```
> let(:valid_params) { { email: user.email, username: user.username } }
> let(:invalid_params) { { email: 'wrong@email', username: user.username} }
> ```


<br><br><hr>
## 6) Going Advanced: testing DESTROY
All right, in the example above, we have seen GET and POST API. In the mean
time, we have seen how to test the controller in general along with assigning
parameters. Now let's see DESTROY API. Consider the following case:
```
def destroy
  @user = User.find(params[:user_id])
  @user.destroy
  redirect_to root_path
end
```


Applying what we learnt on top, we have:
```
  describe 'DELETE #destroy' do
    before(:each) do 
      delete :destroy, id: user.id
      user.reload
    end

    it 'should remove user from db' do
      expect{ User.find(user.id) }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'should redirect to root path' do
      expect(response).to redirect_to root_path
    end
  end
```

> NOTE:
>
> notice the API call is now: ```delete :destroy, id: user.id ```. Then the
> rest are all the same.

> NOTE:
>
> we're using ```reload``` to ensure there is a database activity.


<br><br><hr>
## 7) Going Advanced: testing PUT or PATCH
By now, you should get the hang of it right? Let's try PUT, straight to rspec,
it looks something like the following then:
```
describe "PUT update/:id" do
  let(:old_attr) { :title => 'old title', :content => 'old content' }
  let(:new_attr) { :title => 'new title', :content => 'new content' }


  before(:each) do
    @article = Article.create(old_attr)
    put :update, :id => @article.id, :article => attr
    @article.reload
  end

  it { expect(response).to redirect_to(@article) }
  it { expect(@article.title).to eql new_attr[:title] }
  it { expect(@article.content).to eql new_attr[:content] }
end
```



<br><br><hr>
## 8) Going Advanced: flash messages
All right, now we have tested all 7 routes, we can proceed with testing flash
messages. Depending on individual, if you have your own way of flashing
messages, then you should do it your way to test it. This is an example where
folks uses ```flash``` to store the messages. Consider the case:
```
  def destroy
    @user = User.find(params[:user_id])
    @user.destroy
    redirect_to root_path, notice: 'Your account have been deleted.'
  end
```


To test the flash message, you just need to use ```expect(flash[:notice])```:
```
...

  describe 'DELETE #destroy' do

    ...

    it 'should render notice' do
      expect(flash[:notice]).not_to be nil
    end
  end

...
```

> NOTE:
>
> Instead of performing a direct message comparison, I would want to check
> it is not null or the length is more than 0. This is to avoid making the test
> code brittle and causing the development cripple every single time.
>
> Your goal: avoid brittle test, more flexible test code.


<br><br><hr>
## 9) That's it
All right, I've covered most of materials for testing the controller. Remember,
avoid coding **brittle** test codes (things like overly hard coded things).


<br><br><hr>
## References
1. https://gist.github.com/eliotsykes/5b71277b0813fbc0df56
2. https://www.relishapp.com/rspec/rspec-rails/v/2-1/docs/controller-specs/render-views
3. https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
4. http://stackoverflow.com/questions/9223336/how-to-write-an-rspec-test-for-a-simple-put-update
