# View Feature Testing
Instead of performing technical testing for view files, this guide will jump
straight to feature testing using views. We'll be using rspec selenium driver
and Capybara for this implementation. We'll start off with non-headless test
browser.


<br><br><hr>
## Installing Capybara and gems
You can refer to the guide in the main
[rais-rspec](https://gitlab.com/hollowaykeanho/learning/tree/master/rails/gems/rspec-rails)
article. The main gems components are:
```
gem 'capybara'
gem 'selenium-webdriver'
gem 'capybara-webkit'
```

> NOTE:
>
> If you plan to use chrome as test browser, you need to install the
> chromedriver.
>
> 1. ```$ brew install chromedriver``` if you're using brew.
> 2. download from [google](https://sites.google.com/a/chromium.org/chromedriver/)


<br><br><hr>
## Setting up test script
Unlike models or controllers test script, Capybara requires special setup in
your script. You can include it into your helper if needed. A typical example:
```
RSpec.describe "view file name", type: :view do
  Capybara.register_driver :selenium_chrome do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end
  # Capybara.javascript_driver = :webkit
  Capybara.javascript_driver = :selenium_chrome


  feature "view Index page" do
    scenario "perfect Loading page", js: true do
      ...
    end

    ...
  end

  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
end
```

> NOTE:
>
> The example above instructing Capybara to use Chrome as a test browser
> instead of default Mozilla browser. We'll leave the `:webkit` settings
> in comments for headless testing.
>
> <br>
> Next, instead of using the standard `context` and `it`, we uses
> `feature` and `scenario` since we're emulating the way human use
> the web.
>
> <br>
> Lastly, each Capybara processes should have a teardown step. This teardown
> resets the capybara to original condition and set it back to use default
> driver.


<br><br><hr>
## `scenario` and `feature` term
`feature` term indicates the context are feature related. Hence, we're
expecting a use behavior testing from it. 

<br>
Hence, `scenario` is the situation/simulation where we want to test,
instead of a normal test cases.

<br>
However, it's not a rule so you can still use `context` and `it` if
you want.


<br><br><hr>
## Enable javascript testing
Ensure you append the hash ```js: true``` inside the test case. This will
enable the javascript enabling. Example:
```
    scenario "perfect Loading page", js: true do
      ...
    end
```


<br><br><hr>
## Testing view
Testing view with Capybara is similar to mimicking you using the website itself.
You can manipulate the web by observing views, clicking link, visit url etc.


<br><br>
### visit page
We usually starts off with visit page:
```
visit('/projects')
visit(post_comments_path(post))
```


<br><br>
### validate path
To check current path, we use `have_current_path`:
```
expect(page).to have_current_path(post_comments_path(post))
expect(URI.parse(current_url).path).to eq customers_path
```


<br><br>
### find text on a page
To find text or object inside a page, we use `have_content` to manage the
expectation matching:
```
expect(page).to have_selector('table tr')
expect(page).to have_selector(:xpath, '//table/tr')

expect(page).to have_xpath('//table/tr')
expect(page).to have_css('table tr.foo')
expect(page).to have_content('foo')
```


<br><br>
### interact with a form
the following are the actions to interact with the from:
```
fill_in('First Name', :with => 'John')
fill_in('Password', :with => 'Seekrit')
fill_in('Description', :with => 'Really Long Text...')
choose('A Radio Button')
check('A Checkbox')
uncheck('A Checkbox')
attach_file('Image', '/path/to/image.jpg')
select('Option', :from => 'Select Box')
```


<br><br>
### click
the following are the actions to click on something in a page:
```
click_link('id-of-link')
click_link('Link Text')
click_button('Save')
click_on('Link Text') # clicks on either links or buttons
click_on('Button Value')
```


<br><br>
### finding
the following are instructing the driver to find something in the page:
```
find_field('First Name').value
find_link('Hello', :visible => :all).visible?
find_button('Send').click

find(:xpath, "//table/tr").click
find("#overlay").find("h1").click
all('a').each { |a| a[:href] }
```

Example:
```
find('#navigation').click_link('Home')
expect(find('#navigation')).to have_button('Sign out')
```


<br><br>
### wait / delay
Each action will have a wait action default to 2 seconds. You can alter the
waiting time using `default_max_wait_time`:
```
Capybara.default_max_wait_time = 5      # seconds
```


<br><br><hr>
## Examples
```
require 'rails_helper'

RSpec.describe "customers/new.html.erb", type: :view do

  Capybara.register_driver :selenium_chrome do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end
  # Capybara.javascript_driver = :webkit
  Capybara.javascript_driver = :selenium_chrome


  feature "Customer new page" do
    scenario "able to create new customer" do
      visit new_customer_path
      expect(page).to have_content 'Create New Customer'
      fill_in 'Name', with: "Joseph"
      click_button 'Create Customer'
      expect(page).to have_content 'Joseph'
    end

    scenario "able to go back to Customer#index page" do
      visit new_customer_path
      click_link 'Back'
      expect(URI.parse(current_url).path).to eq customers_path
    end

  end

  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
end
```


<br><br><hr>
## Going headless to speed up testing performance
To go into headless mode, we just need to set the `Capybara.javascript_driver`
to `:webkit` instead of the existing `:selenium_chrome`. Hence, it is:
```
Capybara.javascript_driver = :webkit
# Capybara.javascript_driver = :selenium_chrome
```


<br><br><hr>
## That's it!
That's all about testing views and features with Capybara. Enjoy!


<br><br><hr>
## References
1. http://www.sadafnoor.com/blog/testing-rails-app-with-rspec-factorygirl-capybara-selenium-headless/
2. https://github.com/jnicklas/capybara
3. http://tutorials.jumpstartlab.com/topics/capybara/capybara_with_selenium_and_webkit.html

