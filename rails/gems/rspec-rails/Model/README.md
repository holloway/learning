# Rspec - Model
Rspec model is quite straight forward: it's same as normal rspec testing with
ruby class. Thanks to [Shoulda-matcher](https://github.com/thoughtbot/shoulda-matchers)
gem, now, testing model becomes crazily easier. Let's see case by case.


<br><br><hr>
## Validate ```presence: true```
for any fields that has ```presence: true```:
```
validates :username, presence: true
validates :username, :presence => { :message => "cannot be blank" }
```

<br>
the test code looks as follows:
```
it { is_expected.to validate_presence_of(:username) }
it { is_expected.to validate_presence_of(:username).with_message("cannot be blank") }
```


<br><br><hr>
## Validate ```uniqueness: true```
for any field that has ```uniqueness: true```:
```
validates :api_token, uniqueness: true
validates :api_token, :uniqueness => { :message => "cannot be blank" }
validates :name, uniqueness: { case_sensitive: false }
```

<br>
the test code looks as follows:
```
it { is_expected.to validate_uniqueness_of(:api_token) }
it { is_expected.to validate_uniqueness_of(:api_token).with_message("cannot be blank") }
it { is_expected.to validate_uniqueness_of(:name).case_insensitive }
```


<br><br><hr>
## Validate ```acceptance: true```
for any field that has ```acceptance: true```:
```
validates :terms_of_service, acceptance: { accept: 'yes' }
validates :eula, acceptance: { accept: ['TRUE', 'accepted'], message: "must accept" }
```

<br>
the test code looks as follows:
```
it { is_expected.to validate_acceptance_of(:terms_of_service) }
it { is_expected.to validate_acceptance_of(:eula).with_message("must accept") }
```


<br><br><hr>
## Validate ```numericality: true```
for any field that has ```numericality: true```:
```
validates :points, numericality: true
validates :games_played, numericality: { only_integer: true }
validates :idx_token, numericality: { odd: true, even: true }
validates :quantity, numericality: { less_than_equal_to: 50 }
validates :minimum_purchase, numericality: { greater_than_equal_to: 5 }
```

<br>
the test code looks as follows:
```
it { is_expected.to validate_numericality_of(:points) }
it { is_expected.to validate_numericality_of(:games_played).only_integer }
it { is_expected.to validate_numericality_of(:idx_token).odd }
it { is_expected.to validate_numericality_of(:idx_token).even }
it { is_expected.to validate_numericality_of(:quantity).is_less_than_or_equal_to(50) }
it { is_expected.to validate_numericality_of(:minimum_purchase).is_greater_than_or_equal_to(5) }
```


<br><br><hr>
## Validate ```inclusion:```
for any field that has ```inclusion:```:
```
validates :size, inclusion: { in: %w(small medium large), message: "%{value} is not a valid size" }
validates :age, inclusion: { in: (18..35).to_a }
```

<br>
the test code looks as follows:
```
it { is_expected.to ensure_inclusion_of(:size).in_array(['small', 'medium', 'large']) }.with_message("%{value} is not a valid size")
it { is_expected.to ensure_inclusion_of(:age).in_range(18..35) }
```


<br><br><hr>
## Validate ```length:```
for any field that has ```length:```:
```
validates :name, length: { minimum: 2 }
validates :bio, length: { maximum: 500 }
validates :password, length: { in: 6..20 }
validates :registration_number, length: { is: 6 }
```

<br>
the test code looks as follows:
```
it { is_expected.to ensure_length_of(:name).is_at_least(2) }
it { is_expected.to ensure_length_of(:bio).is_at_most(500) }
it { is_expected.to ensure_length_of(:password).is_at_least(6).is_at_most(20) }
it { is_expected.to ensure_length_of(:registration_number).is_equal_to(6) }
```


<br><br><hr>
## Validate ```belongs_to```
for case:
```
belongs_to :user
```

<br>
The test code is:
```
it { is_expected.to belong_to(:user) }
```


<br><br><hr>
## Validate ```have_one```
for case:
```
has_one :user_account
```

<br>
The test code is:
```
it { is_expected.to have_one(:user_account) }
```


<br><br><hr>
## Validate ```has_many```
for case:
```
have_many :articles
```

<br>
The test code is:
```
it { is_expected.to have_many(:articles) }
```


<br><br><hr>
## Validate ```class and foreign key```
for case:
```
has_one :item, class_name: "Bag"
has_many :cars, class_name: "Car"
has_one :core, class_name: "User Heart", foreign_key: :user_heart_id
has_many :employees, class_name: "Drone", foreign_key: :worker_drone_id
```

<br>
The test code is:
```
it { is_expected.to have_one(:item).class_name("Bag") }
it { is_expected.to have_many(:cars).class_name("Car") }
it { is_expected.to have_one(:core).class_name("User Heart").with_foreign_key(:user_heart_id) }
it { is_expected.to have_many(:employees).class_name("Drone").with_foreign_key(:worker_drone_id) }
```


<br><br><hr>
## Validate ```through:```
for case:
```
have_one :ssid, through: :user_accounts
have_many :wishlist_items, through: :wishlist
```

<br>
The test code is:
```
it { is_expected.to have_one(:ssid).through(:user_accounts) }
it { is_expected.to have_many(:wishlist_items).through(:wishlist) }
```


<br><br><hr>
## Validate ```has_many_and_belong_to_many:```
for case:
```
has_and_belongs_to_many :assemblies
```

<br>
The test code is:
```
it { is_expected.to have_and_belong_to_many(:assemblies) }
```


<br><br><hr>
## Validate ```dependent: :destroy```
for case:
```
has_one :user_account, dependent: :destroy
has_many :assemblies, dependent: :destroy
```

<br>
The test code is:
```
it { is_expected.to have_one(:user_account).dependent(:destroy) }
it { is_expected.to has_many(:assemblies).dependent(:destroy) }
```


<br><br><hr>
## Validate ```polymorphic: true```
for case:
```
has_many :assemblies, polymorphic: true
```

<br>
The test code is:
```
it { is_expected.to have_many(:assemblies) } # automatically detected
```


<br><br><hr>
## Validate ```accepts_nested_attributes_for```
for case:
```
accepts_nested_attributes_for :author, :pages
```

<br>
The test code is:
```
it { is_expected.to accept_nested_attributes_for(:author) }
it { is_expected.to accept_nested_attributes_for(:pages) }
```


<br><br><hr>
## Validate ```have_readonly_attribute```
for case:
```
attr_readonly :password
```

<br>
The test code is:
```
 it { expect(asset).to have_readonly_attribute(:password) }
```


<br><br><hr>
## Validate database column
for case in migration file (containing columns):
```
create_table :politicians do |t|
  t.string :email
  t.string :name
  t.string :political_stance, default: 'undecided', null: false
end

add_index :politicians, [:email]
```

<br>
The test code is:
```
it { is_expected.to have_db_column(:name).of_type(:string) }
it { is_expected.to have_db_column(:political_stance).of_type(:string).with_options(default: 'undecided', null: false) }
it { is_expected.to have_db_index(:email).unique(:true) }
```


<br><br><hr>
## Validate callbacks / custom validations
for cases:
```
after_create :send_welcome_email
after_create :track_new_user_signup
before_validation :make_email_validation_ready, on: :create
after_save: :calculate_some_metrics
before_destroy :update_user_count
before_destroy :send_goodbye_email
```

<br>
The test code is:
```
it { is_expected.to callback(:send_welcome_email).after(:create) }
it { is_expected.to callback(:track_new_user_signup).after(:create) }
it { is_expected.to callback(:make_email_validation_ready!).before(:validation).on(:create) }
it { is_expected.to callback(:calculate_some_metrics).after(:save) }
it { is_expected.to callback(:update_user_count).before(:destroy) }
it { is_expected.to callback(:send_goodbye_email).before(:destroy) }
```


<br><br><hr>
## Validate scope
for cases:
```
class Vote
  scope :loved, -> { order('created_at DESC') }
...

end
```

<br>
The test code is:
```
it ".loved returns all votes with a score > 0" do
  # run a normal test
  product = create(:product)

  love_vote = create(:vote, score: 1, product_id: product.id)
  expect(Vote.loved.first).to eq(love_vote)
end
```


<br><br><hr>
## Validate public methods
for case:
```
class Contra
  ...

  def some_method_name
    ...
  end


  def some_other_method_name
    ...
  end

  private
    ...
end
```

<br>
The test code is:
```
it { is_expected.to respond_to(:some_method_name) }
it { is_expected.to respond_to(:some_other_method_name) }

# anything else you should test it with conventional rspec class testing methods
# e.g: run the method --> expect the return value is eq ? 
```


<br><br><hr>
## That's it
All right. That's all!


<br><br><hr>
## References
1. http://stackoverflow.com/questions/6966263/rails-custom-validation-message
2. http://guides.rubyonrails.org/active_record_validations.html
3. http://stackoverflow.com/questions/13550042/ruby-1-to-100-odd-numbers-in-array
4. https://gist.github.com/kyletcarlson/6234923
5. https://github.com/thoughtbot/shoulda-matchers
6. http://stackoverflow.com/questions/13550042/ruby-1-to-100-odd-numbers-in-array
7. http://guides.rubyonrails.org/active_record_querying.html
8. http://stackoverflow.com/questions/28706722/adding-scope-to-get-objects-in-reverse-order-of-their-created-time-in-rails