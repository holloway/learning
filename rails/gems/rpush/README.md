# rPush Mobile Push Notification
Need a push notification service? rPush gem comes to the rescue. Based on
rPush documentation, it caters services from Apple Push Notification (APNS) all
the way to Windows devices.

<br>
There are 2 stages for setting up the rpush. The note here will show how to
setup APNS server using the daemon mode as recommended.

<br>
There is an **important** known bug. Please read that section before applying
this gem.


<br><br><hr>
## Setting up rpush gem
### 1. Install the rpush and daemon gem into your gemfile, bundle install them.
These are the gems we're using.
```
gem 'daemons'
gem 'rpush'
```

<br>
and then run ```bundle install``` to install them.
```
$ bundle install
```


<br><br>
### 2. Execute the rpush installation
Follow the rpush main instruction to install rpush into Rails.
```
$ bundle exec rpush init
```

> NOTE:
>
> Please do install the database migration file since rpush is using it for
> logging the notification. At the end of the process, you should have:
>
> 1. a config/initializers/rpush.rb
> 2. a bunch of new db/migrations files


<br><br>
### 3. Migrate the database
With the new setup completes, you can migrate the database.
```
$ bundle exec rails db:migrate      # Rails 5 commands
```


<br><br><hr>
## Setting up Apple Push Notification Service (APNS)
This section is mainly for setting up Apple Push Notification Service (APNS).

<br><br>
### 1) Generate the project APNS certificates
This is an apple related step for generating APNS certificate per project.
You can follow this [guide](https://gitlab.com/hollowaykeanho/learning/tree/master/ios/push_notification#generate-and-install-apns-certificates-for-backend-server)
to generate the necessary certificate for backend server.

<br><br>
### 2) In your ```config/initializers/rpush.rb```, add in APNS initializer
The file only contains rpush initializer. In your case, you'll need to add in
the APNS initalizer. Add the following code into bottom of that file, after
any ```@end``` loop.

```
if !Rpush::Apns::App.find_by(name: ENV['APNS_NAME'])
  app = Rpush::Apns::App.new
  app.name = ENV['APNS_NAME']
  app.certificate = File.read(ENV['APNS_CERTIFICATE_PATH'])
  app.environment = ENV['APNS_ENV']
  app.password = ENV['APNS_CERTIFICATE_PASSWORD']
  app.connections = 1
  app.save!
end
```

> NOTE:
> 
> Most of the parameters are using environments variables for flexibility.

<br><br>
### 3) Add environment variables
Assuming you're using [Figaro](https://gitlab.com/hollowaykeanho/learning/tree/master/rails/gems/figaro)
gem, you'll need to add those variables into your system (production and
development).

For my cases, I'm using Figaro gem, the values should look something like this:
```
default: &default
  APNS_NAME: 'QuantoApp'
  APNS_ENV: 'sandbox'
  APNS_CERTIFICATE_PASSWORD: 'test123?core'

development:
  <<: *default
  APNS_CERTIFICATE_PATH: 'config/apns.pem'

test:
  <<: *default
  mailer_domain: 'localhost:4000'
  APNS_CERTIFICATE_PATH: 'config/apns.pem'

production:
  <<: *default
  APNS_CERTIFICATE_PATH: '~/app-root/data/apns/apns.pem'
```

> NOTE:
>
> Notice the location for my APNS certificate. Do ensure you're storing the
> cert at the right path. In the example above, I store the cert in:
>
> development => config/apns.pem # similar to Figaro's application.yml
>
> production => ~/app-root/data/apns/apns.pem
>
> <br>
> APNS_CERTIFICATE_PASSWORD is the password to unlock your APNS certificate.
>
> <br>
> APNS_NAME can be any name and ANPS_ENV is the cert environment. Refer to
> your app cert details.


<br><br><hr>
## Running rPush as Daemon
This is **not** the only way. I recommended this path mainly due to the
APNS working nature itself. If you're attaching it to an individual process
(spawn threading), you might make the main process busy.

<br>
Also, do note that rpush is using the database. Hence, remember to have
your database ready just like your ```rails server```. If you don't get
what I mean,
```
$ bundle exec rails db:create     # Rails5 command
$ bundle exec rails db:Migrate    # Rails5 command
```


<br><br>
### Run the server is development and basic mode
To run the rPush server, simply use the following command:
```
$ bundle exec rpush start
```

<br>
To stop, simply use the following command:
```
$ bundle exec rpush stop
```


<br><br>
### Run the server in production mode
To run the rPush server in the production mode, simply specify the environment
using the following command:
```
$ bundle exec rpush start --no-foreground --rails-env=$RAILS_ENV
```

<br>
To stop the rPush server in the production mode, simply use the following
command with environment specified.
```
$ bundle exec rpush stop --rails-env=$RAILS_ENV
```


<br><br>
### Run the server in OpenShift
You'll need to append a long command and use the direct command from the vendor
folder itself. Include the rpush code inside your start and stop overrides
scripts:

<br>
**.openshift/override/start**:
```
#!/bin/bash -e
source $OPENSHIFT_REPO_DIR/.openshift/action_hooks/override/global_variables
CURRENT_LOCATION=$(pwd)
cd $OPENSHIFT_REPO_DIR

# ...

echo "Starting rpush daemon"
/usr/bin/bundle exec $OPENSHIFT_REPO_DIR/vendor/bundle/ruby/bin/rpush start --no-foreground --rails-env=$RAILS_ENV |& /usr/bin/logshifter -tag ruby &
echo "[ STARTED ]"

# ...


cd $CURRENT_LOCATION
```


<br>
**.openshift/override/stop**:
```
#!/bin/bash -e
source $OPENSHIFT_REPO_DIR/.openshift/action_hooks/override/global_variables
CURRENT_LOCATION=$(pwd)
cd $OPENSHIFT_REPO_DIR


if [ -s "$PUMA_PID_FILE" ]; then
  # ...

  echo "Stopping rpush Server"
  /usr/bin/bundle exec $OPENSHIFT_REPO_DIR/vendor/bundle/ruby/bin/rpush stop --rails-env=$RAILS_ENV |& /usr/bin/logshifter -tag ruby &
  echo "[ STOPPED ]"

  # ...
fi

cd $CURRENT_LOCATION
```

> NOTE:
>
> Be sure to specify your RAILS_ENV as "production", parallel to your main rails
> app.


<br><br>
### That's it
That's all about setting up your own push notification server.

<br><br><hr>
## Send Apple Push Notification
For me, I generate a model file just to handle Apple Push Notification and
making my code clean. The model file is as follow:

<br>
**models/apns.rb**:
```
class Apns
  def self.create_notification(device_token, alert, data)
    n = Rpush::Apns::Notification.new
    n.app = Rpush::Apns::App.find_by_name(ENV['APNS_NAME'])
    n.device_token = device_token
    n.alert = alert
    n.data = data
    n.save!
  end
end
```

<br>
To use it, I simply define the alert and the data dictionary following the
[Apple Documentation](https://developer.apple.com/library/mac/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/TheNotificationPayload.html#//apple_ref/doc/uid/TP40008194-CH107-SW7).

<br>
Example:
```
        Apns.delay.create_notification(apns_token, { :title => "Delivery Status",
                                                     :body => "Insufficient credits. Your order failed",
                                                     :type => FAILED_NOTIFICATION_TYPE,
                                                     :do => DO_UPDATE_USER_CREDITS
                                                   },
                                                   {}
        )
```

> NOTE:
>
> ```:type``` and ```:do``` hashes are my own self defined values. I included
> them to show an example on how to do it.
>
> ```apns_token``` however, is the app notification token given from the mobile
> device. Your iOS app should post this token to backend server.


<br><br><hr>
## Known Bugs!
### 1) Use ```rails db:reset``` if you want to recreate your database.
There is a known [bug](https://github.com/rpush/rpush/issues/281) for screwing
up the database sequences. **DO NOT** simply drop your database! Use
```bundle exec rails db:reset``` in Rails5 or ```bundle exec rake db:reset```
in Rails4 to reset your database.

Otherwise, good luck with your development process.


<br><br><hr>
## References 
1. https://github.com/rpush/rpush
2. https://developer.apple.com/library/mac/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/TheNotificationPayload.html#//apple_ref/doc/uid/TP40008194-CH107-SW7