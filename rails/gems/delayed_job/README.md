# Delayed_job - Background work.
In the case where you need background job but you don't have enough horsepower
(e.g. gears in OpenShift, dyno in Heroku) and you want a persistent job
handling, delayed_job gem is the go.

<br>
Unlike Sidekiq, you don't need to spend 2 gears (extra horsepower) on Redis and
Sidekiq cartidges respectively. Also, unlike Sucker Punch gem, delayed_job uses
the local database to keep track on job progresses and execute them
automatically, even in unstable server crashes senarios (persistence). However,
with that said, this comes at the cost of using database I/O traffic.

<br>
Choose this implemention only if you can compromise database I/O traffic for
background processes.

<br><br><hr>
## Assumptions
1. Ruby 2.0.0
2. Know your [Rails App](https://gitlab.com/hollowaykeanho/learning/tree/master/rails/gems/rspec-rails)
3. Rails 4.2 and above


<br><br><hr>
## Steps
### 1) Follow the delayed_job instruction to install into Rails.
Add the gemfile with daemon gem
```
gem 'delayed_job_active_record'
gem 'daemon'
```


<br><br>
### 2) Perform a bundle install for the gems.
```
$ bundle install
```


<br><br>
### 3) Configure the activejob to use delayed job.
Add the following configuration into your ***config/application.rb***:
```
config.active_job.queue_adapter = :delayed_job
```


<br><br>
### 4) Create the activejob database table.
```
$ bundle exec rails generate delayed_job:active_record
$ bundle exec rake db:migrate
```


<br><br>
### 5) Open a new terminal and star your ActiveJob.
```
$ bundle exec rake .bin/delayed_job -n 2 start
```

> NOTE:
>
> The command is:
> ```
> $ bundle exec rake .bin/delayed_job -n <number_of_worker> start
> ```
>
> So remember to specify the number of worker for your app.


<br><br>
### 7) Run a short test on background work.
You're done. Now run a short test before commiting to git.


<br><br>
### 8) commit and push to your repository
Once the test is passing, you can now commit using git and push to your
repository.


<br><br>
> NOTE:
>
> For mailer, on the official delayed job README, it needs its conventional
> delay call rather than using the deliver method.
>
> Hence, instead of:
> ```
> # without delayed_job
> Notifier.signup(@user).deliver
>
> # with delayed_job
> Notifier.delay.signup(@user)
> 
> # with delayed_job running at a specific time
> Notifier.delay(run_at: 5.minutes.from_now).signup(@user)
> ```
> as of to date of writing this README.md. Please get up-to-date information
> from the official documentation.

<br><br><hr>
## References
1. https://github.com/collectiveidea/delayed_job