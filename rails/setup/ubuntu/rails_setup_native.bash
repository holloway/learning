#!/bin/bash
setup() {
	# sudo apt-get install ruby ruby-dev libicu-dev -y
	keyword="PATH=\"\$(ruby -rubygems -e 'puts Gem.user_dir')/bin:\$PATH\""
	bashrc_addition=i"\
# ruby sourcing
if which ruby >/dev/null && which gem >/dev/null; then
	$keyword
fi
export BUNDLE_PATH=\"\$HOME/.gem\"
"

	filepath=~/.bashrc
	ret="$(cat "$filepath" | grep -n "$keyword" | cut -d : -f 1)"
	if [[ "$ret" == "" ]]; then
		echo "New installation. Setting up your $filepath"
		echo "$bashrc_addition" >> $filepath
	fi

	keyword="gem: --user-install"
	filepath=~/.gemrc
	if [[ -f "$filepath" ]]; then
		ret="$(cat ~/.gemrc | grep -n "$keyword" | cut -d : -f 1)"
		if [[ "$ret" == "" ]]; then
			echo "$keyword" >> $filepath
		fi
	else
		echo "$keyword" > $filepath
	fi
}
setup
