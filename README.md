# learning
[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://img.shields.io/badge/License-MIT-blue.svg)
[![Owner](https://img.shields.io/badge/Owner-Holloway-brightgreen.svg)](https://img.shields.io/badge/Owner-Holloway-brightgreen.svg)

Hello, welcome to my programming learning archives. In here, you'll find codes,
guides and tips that I synthesized from the internet that I used for daily
production. Feel free to start from here.


<br>
<div align="center">
<a href="https://gitlab.com/hollowaykeanho/learning/tree/master">
    <img width="100" height="100" src="https://cdn.rawgit.com/hollowaykeanho/fe17a1c2332bdf7915596c4d1602c6da/raw/fb2d9f4021ba1405d57a893fc7ee357fc2fbda9d/Enter.svg" />
    <p>INTO THE REPOSITORY</p>
</a>
</div>

<br><br>
# Who Am I
I'm Holloway, the principal owner for this repository.

<br>
This repo contains all my knowledge wealth related throughout my software
careers since the year 2008, university era. I started off with Embedded C for
robotic design back in university. Then I progressed to be Linux kernel
development and Infrastructure-as-a-Service from 2012-2015 (or dev-op). Today,
I'm expanding myself towards to Platform-as-a-Service and towards architecture
design.


<br><br>
# Why Create Such Repository Instead of Using Blogging Platform
To be frank, I love programming and coding and with GIT and Linux programming
discipline over the years, blogging for coding documentation doesn't get
together. I jot my notes down in a flashy manner. Hence, an 80x80 terminal is
a quick choice to note them down. A good understanding of Markdown style
and syntax is sufficient.

<br>
Blogging platform, however, provided many tools that encourage me to
procrastinate into styling and coloring. Besides, it's hard to find and
integrate CSS styling for coding blocks. Moreover, blogging needs me to host of
my files elsewhere and then use links to connect the contents back.

<br>
To save all the hassles, managing all in one repo does all will do the trick.
It saves time, consolidates resources into a single platform and moves swiftly,
like a ninja!


<br><br>
# Why ON EARTH!! Your ENGLISH SUCKS!
My sincere apology for my English! Haha! All right, most of the notes are
jotted down in a flash, pretty much like the way we use pen and small pocket
note. I do spend minimum 4 hours a week to scrutinize the notes into blogging
standards based on my progress.

<br>
In case you found a note, and it is that horrible, and you need urgent
attention, please go ahead and file an issue here:

<div align="center">
    <a href="https://gitlab.com/hollowaykeanho/learning/issues">
        <img height="100" width="100" src="https://cdn.rawgit.com/hollowaykeanho/009effd0d4eea7adbb07e507fdad0eae/raw/bc195d9e770ebceff4310585138321b67dbb3ba6/virus.svg"></svg>
        <p>ISSUE Section</p>
    </a>
</div>

<br>
Remember to place the label as **"GRAMMAR"**. I will fix it as soon as I'm
available.


<br><br>
# I Want to Contribute
Wow, thank you. You can follow GitLab [workflow](http://docs.gitlab.com/ee/workflow/forking_workflow.html)
and contribute via merge requests. Contact me when you via messenger when you
bump into questions.

<br>
However, please take note that I still use Linux kernel development best
practices as my mantra of programming. 


<br><br>
# How can I Reference
You can use the links or mention my username (hollowaykeanho) as "author."
However, if there are other mentioned authors in the article, please do
reference them accordingly.

<br>
If references section is available, I **strongly** encourage you to go through
them and perform referencing accordingly. Just in case.

<br>
Let's respect copyrights.


<br><br>
# LICENSE
MIT License. Details can be found [here](https://gitlab.com/hollowaykeanho/learning/blob/master/LICENSE)


<br><br>
# Special Thanks to:
<div>Icons made by <a href="http://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
