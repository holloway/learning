# To Drop A Database
To drop a database, we use `dropdb`.

```
$ dropdb <database name>
```

<br><br>
## References
1. https://www.tutorialspoint.com/postgresql/postgresql_drop_database.htm