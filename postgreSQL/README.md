# PostgreSQL Database
This section is all about PostgreSQL database. In this section, you'll find
all the contents related to PostgreSQL database.


## Setup PostgreSQL
### Ubuntu Linux
This section assumes you're using Ubuntu/Debian Linux which uses `apt-get`
packaging manager by default. To install, open a terminal and run:
```
  # Install PostgreSQL
$ sudo apt-get update -y
$ sudo apt-get install -y postgresql-common postgresql-contrib
$ sudo apt-get install -y postgresql libpq-dev

  # Setup user for PostgreSQL
$ sudo -u postgres createuser $USER -s
$ sudo -u postgres psql --command "\\password $USER"

  # Run the setup
$ service postgresql start
$ sudo update-rc.d postgresql enable
```


<br><br>
### Mac OSX
This section assumes you had installed `brew` packaging manager. We'll be using
`brew` install to manage all the setup. To install, open a terminal and run:
```
$ brew install postgresql
$ ln -sfv /usr/local/opt/postgresql/*plist ~/Library/LaunchAgents
$ launchctl load ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist
```

> NOTE:
>
> The first command is the actual installation. The second command is for
> PostgreSQL to run at login automatically while the third command is to launch
> the server now.

<br>
### Additional Setup for OSX
#### Creating 'postgres' database user
This is for Linux-Mac environment. Linux uses "postgres" account by default and
it is different
```
$ createuser --pwprompt postgres
$ psql -c "ALTER USER postgres WITH SUPERUSER;" -d <database name>
$ psql -c "ALTER USER postgres WITH CREATEDB;" -d <database name>
$ psql -c "ALTER USER postgres WITH CREATEROLE;" -d <database name>
$ psql -c "ALTER USER postgres WITH REPLICATION;" -d <database name>
$ psql -c "ALTER USER postgres WITH BYPASSRLS;" -d <database name>
```


<br><br>
## References
1. http://stackoverflow.com/questions/10757431/postgres-upgrade-a-user-to-be-a-superuser
2. https://www.tunnelsup.com/setting-up-postgres-on-mac-osx/