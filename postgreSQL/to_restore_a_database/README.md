# To Restore a Database
to restore a database, we use the following command:

```
 DB=<DATABASE_NAME> && createdb $DB && psql -d $DB < <SQL_DUMP_FILE>
```


<br><br>
## References
1. https://www.odoo.com/forum/help-1/question/could-not-restore-the-database-openerp-58025
