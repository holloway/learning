# Get User List for A Database
To get a list of users for a particular database, you can use the following
command:

```
$ psql -c "\du"
                             List of roles
 Role name |                   Attributes                   | Member of 
-----------+------------------------------------------------+-----------
 chpert    |                                                | {}
 postgres  | Superuser, Create role, Create DB, Replication | {}
 test      |                                                | {}
```

<br><br>
## References
1. http://unix.stackexchange.com/questions/201666/command-to-list-postgresql-user-accounts