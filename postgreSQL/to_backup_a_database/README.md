# To Backup a Database
to backup a database, we use the following command:

```
$ pg_dump --cluster 9.1/main --format=c <database_name> > <dumpfile name>
```


<br><br>
## References
1. https://www.odoo.com/forum/help-1/question/could-not-restore-the-database-openerp-58025
